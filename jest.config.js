module.exports = {
  moduleNameMapper: {
    '^.+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      'babel-jest',
  },
  preset: 'react-native',
  testMatch: ['<rootDir>/Tests/**/*.js', '**/?(*.)(spec|test).js?(x)'],
  testPathIgnorePatterns: ['/node_modules/', '<rootDir>/Tests/Setup.js'],
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
  },
  setupFiles: [
    '<rootDir>/node_modules/react-native-gesture-handler/jestSetup.js',
    '<rootDir>/Tests/Setup',
  ],
};
