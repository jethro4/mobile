import React from 'react';

const DropdownContext = React.createContext({
  openDropdownScreen: () => {},
});

export default DropdownContext;
