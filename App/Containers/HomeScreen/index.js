import React, {useState, useRef} from 'react';
import {Animated} from 'react-native';

import {Metrics} from '../../Themes';

import TabContainer from '../Shared/TabContainer';
import {moderateScale} from 'react-native-size-matters/extend';
import {chunk} from 'lodash';

import {setupDetailsVar, userDetailsVar} from '../../Cache';
import {useQuery} from '../../Hooks';
import HomeTabNavigation from '../../Navigation/HomeTabNavigation';

const ROW_WIDGET_COUNT = Metrics.isTablet ? 3 : 2;
const CIRCLE_SIZE = Metrics.wp(Metrics.isTablet ? 18 : 24);
const logoSize = moderateScale(32, 0.4);

const HomeScreen = (props) => {
  const {data: setupDetails} = useQuery(HomeScreen.name, {
    cacheFn: setupDetailsVar,
  });
  const {data: userDetails} = useQuery(HomeScreen.name, {
    cacheFn: userDetailsVar,
  });
  console.tron.log('HomeScreen userDetails', userDetails);
  const animatedSearchOpened = useRef(new Animated.Value(0)).current;
  const [widgets, setWidgets] = useState(
    chunk(
      [
        {
          name: 'Calculator',
        },
        {
          name: 'Calendar',
        },
        {
          name: 'Search',
        },
        {
          name: 'Email',
        },
      ],
      ROW_WIDGET_COUNT,
    ),
  );
  const [searchText, setSearchText] = useState('');

  return (
    <>
      <TabContainer headerTitle="Home">
        <HomeTabNavigation />
      </TabContainer>
    </>
  );
};

export default HomeScreen;
