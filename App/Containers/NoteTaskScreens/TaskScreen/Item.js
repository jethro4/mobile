import React, {useState, useRef} from 'react';
import {TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import {View, Content, Modal} from '../../../Components/Layout';
import SvgIcon from '../../../Components/SvgIcon';
import {Switch} from '../../../Components/Forms';
import {Text} from '../../../Components/Text';
import {IconButton} from '../../../Components/Buttons';
import {Metrics, Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {COMPLETE_TASK} from '../../../GraphQL/NotesTasks';
import {useNavigation} from '@react-navigation/native';
import moment from 'moment';
import Alert from '../../../Containers/Shared/Alert';
import {ViewMoreText} from '../../../Components/Text';
import {useMutation} from '@apollo/client';

const CONTAINER_VERTICAL_PADDING = moderateScale(16, 0.1);
const INNER_VERTICAL_PADDING = moderateScale(12, 0.1);
export const TASK_ITEM_HEIGHT = moderateScale(242, 0.2);

const Item = (props) => {
  const {item} = props;
  const taskSwitchRef = useRef();
  const navigation = useNavigation();
  const modalRef = useRef();
  const completeTaskModalRef = useRef();
  const [isComplete, setIsComplete] = useState(!!item.completionDate);
  const [hasShowMore, setHasShowMore] = useState(false);
  const [isPromptTaskComplete, setIsPromptTaskComplete] = useState(false);
  const [callMutation] = useMutation(COMPLETE_TASK, {
    onError() {},
  });

  const handleShow = (shouldShow) => () => {
    if (shouldShow) {
      modalRef.current.show();
    } else {
      modalRef.current.hide();
    }
  };

  const onTaskComplete = () => {
    setIsComplete(true);
    callMutation({
      variables: {
        completeTaskInput: {id: item.id},
        __mutationkey: 'completeTaskInput',
      },
    });
  };
  const setIsPromptTaskCompleteFn = (isVisible) => {
    setIsPromptTaskComplete(isVisible);
  };

  return (
    <View
      style={{
        backgroundColor: Colors.white,
        paddingVertical: CONTAINER_VERTICAL_PADDING,
        borderBottomWidth: 1,
        borderBottomColor: Colors.border,
        marginBottom: moderateScale(8, 0.3),
        height: TASK_ITEM_HEIGHT,
      }}>
      <Content>
        <View alignCenter justifyBetween row>
          <View
            isFlex
            style={{
              paddingRight: moderateScale(12, 0.1),
            }}>
            <Text color="primary" weight="medium" xsmall>
              {moment(item.creationDate).format('ddd DD MMM YYYY hh:mmA')}
            </Text>
            <Text
              color="darkerGray"
              numberOfLines={1}
              style={{marginTop: moderateScale(4, 0.1)}}
              xxxsmall>
              Assigned By:{' '}
              <Text color="darkerGray" weight="semibold" xxxsmall>
                {item.assignedBy}
              </Text>
            </Text>
          </View>

          <View alignCenter row>
            <Text color="darkerGray" xsmall>
              Done:{' '}
            </Text>
            <View
              style={{
                transform: [
                  {scaleX: moderateScale(0.8, 0.2)},
                  {scaleY: moderateScale(0.8, 0.2)},
                ],
              }}>
              <Switch
                defaultValue={isComplete}
                disabled={isComplete}
                onValueChange={(value) => {
                  if (value) {
                    completeTaskModalRef.current.show();
                  }
                }}
                ref={taskSwitchRef}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: Colors.border,
            marginVertical: INNER_VERTICAL_PADDING,
          }}
        />

        <View isFlex>
          <Text
            color="primary"
            ellipsizeMode="tail"
            numberOfLines={1}
            small
            style={{
              marginTop: moderateScale(4, 0.3),
            }}
            weight="medium">
            {item.title}
          </Text>
          <ViewMoreText handleShow={handleShow(true)} slarge>
            {item.note}
          </ViewMoreText>
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: Colors.border,
            marginVertical: INNER_VERTICAL_PADDING,
          }}
        />

        <View
          alignCenter
          justifyBetween
          row
          style={{
            marginBottom: INNER_VERTICAL_PADDING,
          }}>
          <Text color="small" xxxsmall>
            Action Date:{' '}
            <Text color="small" weight="semibold" xxxsmall>
              {moment(item.actionDate).format('YYYY-MM-DD')}
            </Text>
          </Text>

          <Text color="small" style={{textAlign: 'right'}} xxxsmall>
            Assigned To:{' '}
            <Text color="small" numberOfLines={1} weight="semibold" xxxsmall>
              {item.assignedTo}
            </Text>
          </Text>
        </View>
      </Content>

      <View justifyEnd>
        {item.tags && item.tags.length > 0 ? (
          <View>
            <ScrollView
              bounces={false}
              contentContainerStyle={{
                paddingTop: CONTAINER_VERTICAL_PADDING,
                paddingHorizontal: Metrics.spaceHorizontal,
              }}
              horizontal={true}>
              <View alignCenter row>
                {item.tags.map((tag, index) => {
                  return (
                    <View
                      key={index}
                      style={{
                        paddingHorizontal: moderateScale(4, 0.3),
                        paddingVertical: moderateScale(4, 0.3),
                        borderRadius: moderateScale(4, 0.3),
                        borderWidth: 0.1,
                        borderColor: Colors.darkerGray,
                        backgroundColor: Colors.lighterGray,
                        marginRight: moderateScale(8, 0.3),
                        marginBottom: moderateScale(4, 0.3),
                      }}>
                      <Text color="darkerGray" xsmall>
                        #{tag}
                      </Text>
                    </View>
                  );
                })}
              </View>
            </ScrollView>
          </View>
        ) : (
          <Text
            color="lightGray"
            xsmall
            style={{
              marginLeft: Metrics.spaceHorizontal,
            }}>
            No Tags
          </Text>
        )}
      </View>

      <Modal alignCenter justifyCenter ref={modalRef}>
        <View
          style={{
            backgroundColor: Colors.superLightGray,
            borderRadius: 14,
            width: moderateScale(320),
            paddingTop: moderateScale(8, 0.3),
          }}>
          <View alignCenter justifyBetween row>
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                alignItems: 'center',
              }}>
              <Text center weight="semibold" xxxlarge>
                Notes
              </Text>
            </View>

            <IconButton
              containerStyle={{
                alignSelf: 'flex-end',
                alignItems: 'flex-end',
              }}
              icon="Close"
              iconFill={Colors.darkerGray}
              iconSize={moderateScale(16, 0.1)}
              isAnimated
              onPress={handleShow(false)}
            />
          </View>

          <View
            style={{
              paddingHorizontal: Metrics.spaceHorizontal,
              // paddingTop: CONTAINER_VERTICAL_PADDING,
              maxHeight: Metrics.hp(74),
            }}>
            <ScrollView
              contentContainerStyle={{
                paddingVertical: moderateScale(10, 0.1),
              }}>
              <Text slarge>{item.note}</Text>
            </ScrollView>
          </View>
        </View>
      </Modal>

      <Alert
        ref={completeTaskModalRef}
        style={{
          height: moderateScale(178),
          marginBottom: moderateScale(220),
        }}>
        <>
          <View
            isFlex
            justifyCenter
            style={{
              paddingHorizontal: moderateScale(20),
              paddingVertical: moderateScale(20),
            }}>
            <Text
              center
              style={{
                marginBottom: moderateScale(16),
              }}
              weight="medium"
              xlarge>
              Complete Task?
            </Text>
            <Text center small>
              You won't be able to undo
            </Text>
            <Text center small>
              this action
            </Text>
          </View>

          <View row>
            <TouchableOpacity
              onPress={() => {
                taskSwitchRef.current.toggleSwitch();
                completeTaskModalRef.current.hide();
              }}
              style={styles.button}>
              <Text
                center
                style={{
                  color: '#AAAAAA',
                }}
                xlarge>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                completeTaskModalRef.current.hide();
                onTaskComplete();
              }}
              style={styles.button}>
              <Text
                center
                style={{
                  color: '#007AFF',
                }}
                xlarge>
                Yes
              </Text>
            </TouchableOpacity>
          </View>
        </>
      </Alert>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    height: moderateScale(44),
    justifyContent: 'center',
  },
});

Item.defaultProps = {};

export default Item;
