import React, {useEffect, useRef} from 'react';
import {Content, View} from '../../../Components/Layout';
import {ErrorMessage} from '../../../Components/Text';
import FormArea from '../../../Components/Forms/FormArea';
import FormInput from '../../Shared/Forms/FormInput';
import FormSelect from '../../Shared/Forms/FormSelect';
import FormTagsInput from '../../Shared/Forms/FormTagsInput';
import SubmitButton from '../../Shared/Forms/SubmitButton';

import {createNoteValidationSchema} from '../../../Utils/validations';
import {Formik} from 'formik';
import {CREATE_NOTE, UPDATE_NOTE} from '../../../GraphQL/NotesTasks';
import {CREATE_DO_NOTE} from '../../../GraphQL/DataObjects';
import {Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {useNavigation} from '@react-navigation/native';
import Const from '../../../Constants';
import {filterNoteTags} from '.';
import {useMutation} from '@apollo/client';
import R from 'ramda';

const NoteForms = (props) => {
  const {initValues, isEdit, onSuccess, doItemId, handleSubmit} = props;
  const navigation = useNavigation();
  const tagsRef = useRef();
  const noteRef = useRef();
  const noteValuesRef = useRef();

  const [
    callCreateMutation,
    {loading: createLoading, error: createError, data: createData = {}},
  ] = useMutation(CREATE_NOTE, {
    onError() {},
  });

  const [
    callUpdateMutation,
    {loading: updateLoading, error: updateError, data: updateData = {}},
  ] = useMutation(UPDATE_NOTE, {
    onError() {},
  });

  const loading = createLoading || updateLoading;
  const error = createError || updateError;

  useEffect(() => {
    if (
      (!createLoading && !createError && createData.createNote) ||
      (!updateLoading && !updateError && updateData.updateNote)
    ) {
      console.tron.log('SUCCESS SAVE Note!!');
      onSuccess && onSuccess();

      if (!doItemId) {
        navigation.goBack();
      } else {
        navigation.navigate('DataObjectTabs', {
          doNoteValues: noteValuesRef.current,
        });
      }
    }
  }, [createLoading, updateLoading]);

  return (
    <Formik
      initialValues={initValues}
      onSubmit={async (values) => {
        const doNoteValues = {
          doItemId,
          ...R.dissoc('contactId', values),
        };

        noteValuesRef.current = doNoteValues;

        if (handleSubmit) {
          await handleSubmit(doNoteValues);

          navigation.navigate('DataObjectTabs', {
            doNoteValues,
          });
        } else {
          if (!isEdit) {
            callCreateMutation({
              variables: {
                createNoteInput: values,
                __mutationkey: 'createNoteInput',
              },
            });
          } else {
            callUpdateMutation({
              variables: {
                updateNoteInput: values,
                __mutationkey: 'updateNoteInput',
              },
            });
          }
        }
      }}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={createNoteValidationSchema}>
      {({handleSubmit, handleChange, values, errors, setFieldValue}) => (
        <>
          <Content
            isFlex={false}
            style={{
              backgroundColor: Colors.white,
            }}>
            <FormSelect
              defaultValue={values.type}
              items={Const.ACTION_TYPES}
              label="Action Type"
              onSelectPickerItem={(value) => setFieldValue('type', value)}
            />

            <FormInput
              // autoFocus
              autoCapitalize="words"
              defaultValue={values.title}
              error={errors.title}
              label="Title"
              onChangeText={handleChange('title')}
              onSubmitEditing={() => {
                console.tron.log('tagsRef', tagsRef);
                tagsRef.current.focus();
              }}
              returnKeyType="next"
            />

            <FormTagsInput
              autoCapitalize="words"
              defaultTags={filterNoteTags(values.tags)}
              error={errors.tags}
              label="Tags"
              onSubmitEditing={() => {
                noteRef.current.focus();
              }}
              onTagsUpdate={(value) =>
                setFieldValue('tags', filterNoteTags(value))
              }
              ref={tagsRef}
              returnKeyType="next"
            />

            <FormArea
              autoCapitalize="sentences"
              defaultValue={values.note}
              label="Type your notes here..."
              onChangeText={handleChange('note')}
              ref={noteRef}
              returnKeyType="next"
            />
          </Content>

          <SubmitButton
            isLoading={loading}
            onPress={handleSubmit}
            title="SAVE"
          />

          <View
            isFlex
            justifyEnd
            style={{
              paddingVertical: moderateScale(20, 0.3),
              paddingHorizontal: Metrics.spaceHorizontal,
              // backgroundColor: Colors.background
            }}>
            <ErrorMessage center>{error?.message}</ErrorMessage>
          </View>
        </>
      )}
    </Formik>
  );
};

export default NoteForms;
