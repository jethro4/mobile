import React, {useEffect, useRef} from 'react';
import {Content, View} from '../../../Components/Layout';
import {ErrorMessage} from '../../../Components/Text';
import FormArea from '../../../Components/Forms/FormArea';
import FormInput from '../../Shared/Forms/FormInput';
import FormSelect from '../../Shared/Forms/FormSelect';
import FormTagsInput from '../../Shared/Forms/FormTagsInput';
import FormDatePicker from '../../Shared/Forms/FormDatePicker';
import SubmitButton from '../../Shared/Forms/SubmitButton';

import {createTaskValidationSchema} from '../../../Utils/validations';
import {Formik} from 'formik';
import {CREATE_TASK} from '../../../GraphQL/NotesTasks';
import {Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {useNavigation} from '@react-navigation/native';
import FormAssignToSelect from './FormAssignToSelect';
import {filterTaskTags} from '.';
import {useMutation} from '@apollo/client';

const PRIORITY_LEVELS = ['1. Critical', '2. Essential', '3. Non-Essential'];

const TaskForms = (props) => {
  const {initValues, onSuccess} = props;
  const navigation = useNavigation();
  const tagsRef = useRef();
  const noteRef = useRef();

  const [callCreateMutation, {loading, error, data = {}}] = useMutation(
    CREATE_TASK,
    {
      onError() {},
    },
  );

  useEffect(() => {
    console.tron.log('Task!!', data);
    if (!loading && !error && data.createTask) {
      console.tron.log('SUCCESS SAVE Task!!');
      onSuccess();
      navigation.goBack();
    }
  }, [loading]);

  return (
    <Formik
      initialValues={initValues}
      onSubmit={(values) => {
        console.tron.log('onSubmit Task', values);
        callCreateMutation({
          variables: {
            createTaskInput: values,
            __mutationkey: 'createTaskInput',
          },
        });
        // alert("In Progress")
      }}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={createTaskValidationSchema}>
      {({handleSubmit, handleChange, values, errors, setFieldValue}) => (
        <>
          <Content
            isFlex={false}
            style={{
              backgroundColor: Colors.white,
            }}>
            <FormAssignToSelect
              error={errors.userEmail}
              label="Assign To (Macanta User)"
              onSelectPickerItem={(value) => setFieldValue('userEmail', value)}
              zIndex={1001}
            />

            <FormSelect
              defaultValue={PRIORITY_LEVELS[values.priority]}
              error={errors.priority}
              items={PRIORITY_LEVELS}
              label="Priority"
              onSelectPickerItem={(value) =>
                setFieldValue('priority', PRIORITY_LEVELS.indexOf(value) + 1)
              }
            />

            <FormInput
              // autoFocus
              autoCapitalize="words"
              defaultValue={values.title}
              error={errors.title}
              label="Action Description"
              onChangeText={handleChange('title')}
              onSubmitEditing={() => {
                console.tron.log('tagsRef', tagsRef);
                tagsRef.current.focus();
              }}
              returnKeyType="next"
            />

            <FormDatePicker
              error={errors.actionDate}
              iconSize={moderateScale(14, 0.3)}
              label="Action Date"
              onDateSelected={handleChange('actionDate')}
            />

            <FormTagsInput
              autoCapitalize="words"
              defaultTags={filterTaskTags(values.tags)}
              error={errors.tags}
              label="Tags"
              onSubmitEditing={() => {
                noteRef.current.focus();
              }}
              onTagsUpdate={(value) =>
                setFieldValue('tags', filterTaskTags(value))
              }
              ref={tagsRef}
              returnKeyType="next"
            />

            <FormArea
              autoCapitalize="sentences"
              defaultValue={values.note}
              label="Type your notes here..."
              onChangeText={handleChange('note')}
              ref={noteRef}
              returnKeyType="next"
            />
          </Content>

          <SubmitButton
            isLoading={loading}
            onPress={handleSubmit}
            title="SAVE"
          />

          <View
            isFlex
            justifyEnd
            style={{
              paddingVertical: moderateScale(20, 0.3),
              paddingHorizontal: Metrics.spaceHorizontal,
              backgroundColor: Colors.background,
            }}>
            <ErrorMessage center>{error?.message}</ErrorMessage>
          </View>
        </>
      )}
    </Formik>
  );
};

export default TaskForms;
