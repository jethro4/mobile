import React, {useState, useRef, useImperativeHandle, forwardRef} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import SvgIcon from '../../Components/SvgIcon';
import {View, Content} from '../../Components/Layout';
import {Button} from '../../Components/Buttons';
import {ApplicationStyles, Colors} from '../../Themes';
import {ErrorMessage, Text} from '../../Components/Text';
import {moderateScale} from 'react-native-size-matters/extend';
import {FormInput, PasswordInput} from '../../Components/Forms';
import {loginValidationSchema} from '../../Utils/validations';
import {Formik} from 'formik';
import LoginAlerts from './LoginAlerts';
import {setupDetailsVar, userDetailsVar, sessionVar} from '../../Cache';
import {GET_SETUP_DETAILS, GET_USER_DETAILS} from '../../GraphQL/Cache';
import {useQuery} from '@apollo/client';

const {height} = ApplicationStyles.formRow;
export const BUTTON_HEIGHT = moderateScale(height, 0.3);

const questionIconSize = moderateScale(16);
const backIconSize = moderateScale(18, 0.1);

const Login = (props, ref) => {
  const modalRef = useRef();
  // const { loading: loadingInitValues, data } = useQuery(GET_USER_DETAILS);
  // const initialValues = data.userDetails || {}
  const {data: {setupDetails = {}} = {}} = useQuery(GET_SETUP_DETAILS);
  const {data: {userDetails = {}} = {}} = useQuery(GET_USER_DETAILS);

  console.tron.log('initial User details', userDetails, setupDetails);

  const [alertInfo, setAlertInfo] = useState({type: '', payload: {}});
  const [setupError, setSetupError] = useState('');
  // const dispatch = useDispatch()
  const emailRef = useRef();
  const passwordRef = useRef();

  const showAlertInfo = (type = '', payload = {}) => {
    setAlertInfo({type, payload});
    modalRef.current.show();
  };

  useImperativeHandle(
    ref,
    () => {
      return {
        focusEmail: () => {
          emailRef.current.focus();
        },
      };
    },
    [],
  );

  const onLogin = async (data) => {
    const {sessionId, email, password, firstName, lastName} = data;
    console.tron.log('LOG IN SUCCESS: ' + sessionId);
    sessionVar({sessionId});
    console.tron.log('onLogin userDetails', {
      email,
      password,
      firstName,
      lastName,
    });
    userDetailsVar({email, password, firstName, lastName});
    props.navigation.replace('Home');
  };

  const onError = (error) => {
    setSetupError(error?.message);
  };

  const onBack = () => {
    props.navigation.goBack();
  };

  return (
    <View isFlex>
      <Formik
        initialValues={userDetails}
        key={JSON.stringify(userDetails)}
        onSubmit={(values) => {
          const {email, password} = values;

          setSetupError('');

          showAlertInfo('login', {email, password});
        }}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={loginValidationSchema}>
        {({handleChange, values, handleSubmit, errors}) => (
          <Content largeSpace>
            <FormInput
              autoCompleteType="email"
              defaultValue={values.email}
              error={errors.email}
              icon="Email"
              keyboardType="email-address"
              onChangeText={handleChange('email')}
              onSubmitEditing={() => passwordRef.current.focus()}
              placeholder="Email"
              ref={emailRef}
              returnKeyType="next"
              textContentType="emailAddress"
            />

            <PasswordInput
              defaultValue={values.password}
              error={errors.password}
              icon="Lock"
              onChangeText={handleChange('password')}
              onSubmitEditing={handleSubmit}
              placeholder="Password"
              ref={passwordRef}
              returnKeyType="done"
            />

            <Button
              icon={
                <View
                  style={{
                    marginLeft: 10,
                  }}>
                  <SvgIcon
                    fill={Colors.white}
                    height={moderateScale(10)}
                    name="RightChevron"
                    width={moderateScale(10)}
                  />
                </View>
              }
              label="Log me in"
              // isLoading={isLoading}
              onPress={handleSubmit}
            />
            <ErrorMessage center>{setupError}</ErrorMessage>

            <View
              // isFlex
              alignCenter
              justifyBetween
              row
              style={{
                marginTop: moderateScale(36, 0.3),
              }}>
              <View isFlex>
                <TouchableOpacity onPress={onBack} style={styles.backButton}>
                  <SvgIcon
                    fill={Colors.black}
                    height={backIconSize}
                    name="LeftChevron"
                    width={backIconSize}
                  />
                  <Text
                    medium
                    style={{
                      color: Colors.black,
                      marginLeft: moderateScale(8),
                    }}>
                    Back
                  </Text>
                </TouchableOpacity>
              </View>

              {/* <View
                isFlex
              >
                <TouchableOpacity
                  style={styles.options}
                  // onPress={() => showAlertInfo()}
                >
                  <SvgIcon fill={Colors.secondary} name='Help' width={questionIconSize} height={questionIconSize}/>
                  <Label style={{
                    color: Colors.primary,
                    marginLeft: moderateScale(8)
                  }}>Forgot Password?</Label>
                </TouchableOpacity>
              </View> */}
            </View>
          </Content>
        )}
      </Formik>

      {
        <LoginAlerts
          alertInfo={alertInfo}
          onError={onError}
          onSuccess={onLogin}
          ref={modalRef}
        />
      }
    </View>
  );
};

const styles = StyleSheet.create({
  backButton: {
    alignItems: 'center',
    flexDirection: 'row',
    height: BUTTON_HEIGHT,
  },
  options: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

export default forwardRef(Login);
