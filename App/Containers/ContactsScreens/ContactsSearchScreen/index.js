import React, {useEffect, useState, useRef, useCallback} from 'react';
import {Keyboard} from 'react-native';
import TabContainer from '../../Shared/TabContainer';
import {InfiniteScrollList} from '../../../Components/Layout';
import {Colors} from '../../../Themes';
import {IconButton} from '../../../Components/Buttons';
import Search from '../../Shared/Search';
import ContactItem, {ITEM_CONTACT_HEIGHT} from './ContactItem';
import {moderateScale} from 'react-native-size-matters/extend';
import {debounce} from 'lodash';
import {LIST_CONTACTS} from '../../../GraphQL/Contacts';
import {sessionVar} from '../../../Cache';
import Const from '../../../Constants';
import {useLazyQuery} from '@apollo/client';

const CONTACT_ITEMS_LIMIT = 20;

const ContactsSearchScreen = (props) => {
  const searchRef = useRef();
  const listRef = useRef();
  const [searchText, setSearchText] = useState('');
  const [searchType, setSearchType] = useState('Name');
  const [
    callQuery,
    {loading: loadingContacts, error, data = {}},
  ] = useLazyQuery(LIST_CONTACTS);

  const filterData = useCallback(
    debounce(
      (search = '', type, page = 0, limit = CONTACT_ITEMS_LIMIT, noReset) => {
        if (!noReset && page === 0) {
          listRef.current.resetPage();
        }

        const {sessionId} = sessionVar();

        console.tron.log(
          `filterData search: ${search}, type: ${type}, page: ${page}, limit: ${limit}, sessionId: ${sessionId}`,
        );
        const payload = {page, limit, sessionId};
        switch (type) {
          case 'Name': {
            callQuery({variables: {...payload, q: search}});
            break;
          }
          case 'Email': {
            callQuery({variables: {...payload, q: `email:${search}`}});
            break;
          }
          case 'Company': {
            callQuery({variables: {...payload, q: `company:${search}`}});
            break;
          }
          case 'Phone': {
            callQuery({variables: {...payload, q: search}});
            break;
          }
          case 'Address': {
            callQuery({variables: {...payload, q: `address:${search}`}});
            break;
          }
        }
      },
      400,
    ),
    [],
  );

  // useEffect(() => {
  //   filterData(searchText)
  // }, [contacts, searchText])
  const handleSearchText = (text) => {
    setSearchText(text);
    filterData(text, searchType);
  };

  const handleSearchType = (type) => {
    setSearchType(type);
    if (searchText) {
      filterData(searchText, type);
    }
  };

  const reloadData = useCallback(() => {
    console.tron.log('onRefresh Contacts reloadData');
    filterData(searchText, searchType, 0, CONTACT_ITEMS_LIMIT);
  }, [filterData, searchText, searchType]);

  const handleSort = (value) => {
    if (value) {
      alert(
        'Request Contacts GET API update for Geover.\n\nSort by oldest first',
      );
      // filterData(searchText, searchType, "asc")
    } else {
      alert(
        'Request Contacts GET API update for Geover.\n\nSort by with latest first',
      );
      // filterData(searchText, searchType, "desc")
    }
  };

  const handleOnScrollBeginDrag = () => {
    searchRef.current.blur();
  };

  useEffect(() => {
    if (props.route.params?.hasContactSaved) {
      reloadData();
    }
  }, [props.route.params, reloadData]);

  return (
    <TabContainer
      headerRight={
        <IconButton
          icon="AddContact"
          iconFill={Colors.white}
          iconSize={moderateScale(26, 0.4)}
          onPress={() => {
            Keyboard.dismiss();
            props.navigation.navigate('ContactAdd');
          }}
        />
      }
      headerTitle="Contacts">
      <Search
        categories={Const.CONTACT_CATEGORIES}
        defaultCategory={searchType}
        onChangeText={(text) => handleSearchText(text)}
        onSelectPickerItem={(item) => handleSearchType(item)}
        onSort={(value) => handleSort(value)}
        ref={searchRef}
      />
      <InfiniteScrollList
        contentContainerStyle={{
          paddingVertical: moderateScale(8, 0.3),
        }}
        data={data.listContacts && data.listContacts.items}
        getData={(page, limit) => {
          filterData(searchText, searchType, page - 1, limit, true);
        }}
        getItemLayout={(data, index) => ({
          length: ITEM_CONTACT_HEIGHT,
          offset: ITEM_CONTACT_HEIGHT * index,
          index,
        })}
        keyboardShouldPersistTaps="always"
        keyExtractor={(item, index) => item.id || String(index)}
        // keyExtractor={(item, index) => String(index)}
        loading={loadingContacts}
        onScrollBeginDrag={handleOnScrollBeginDrag}
        pageItemsLength={CONTACT_ITEMS_LIMIT}
        ref={listRef}
        renderItem={({item}) => (
          <ContactItem
            contact={item}
            onPress={() => {
              Keyboard.dismiss();
              props.navigation.navigate('ContactDetails', {
                contact: item,
              });
            }}
          />
        )}
      />
    </TabContainer>
  );
};

export default ContactsSearchScreen;
