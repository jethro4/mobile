import React, {useRef, useState} from 'react';
import {ScrollView, TouchableOpacity} from 'react-native';
import {Content, View} from '../../../Components/Layout';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import {Label, Text} from '../../../Components/Text';
import Loader from '../../../Components/Loader';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {sortArrayByObjectKey} from '../../../Utils/array';
import {moderateScale} from 'react-native-size-matters/extend';
import {IconButton} from '../../../Components/Buttons';
import DocumentPicker from 'react-native-document-picker';
import {setupDetailsVar} from '../../../Cache';
import FastImage from 'react-native-fast-image';
import {pick} from '../../../Utils/attachmentPicker';
import {downloadFile} from '../../../Utils/file';
import Alert from '../../../Containers/Shared/Alert';

const ITEM_ATTACHMENT_HEIGHT = moderateScale(122, 0.3);
const THUMBNAIL_SIZE = moderateScale(64, 0.3);

const viewFile = (localFile) => {
  return FileViewer.open(localFile, {showOpenWithDialog: true});
};

const selectFilesDocumentPicker = async () => {
  try {
    const res = await pick();
    console.tron.log(
      'file',
      res.uri,
      res.type, // mime type
      res.name,
    );

    return {
      uri: res.uri,
      type: res.type,
      name: res.name,
    };
  } catch (err) {
    if (DocumentPicker.isCancel(err)) {
      // User cancelled the picker, exit any dialogs or menus and move on
    } else {
      // throw err;
    }
  }
};

const File = ({attachment}) => {
  const {fileName, thumbnail, downloadUrl} = attachment;
  const [loadingFilePress, setLoadingFilePress] = useState(false);

  const handlePressFile = async () => {
    setLoadingFilePress(true);
    const localFile = `${RNFS.TemporaryDirectoryPath}/${fileName}`;

    try {
      console.tron.log('localFile: ' + localFile);
      if (downloadUrl) {
        await downloadFile(downloadUrl, localFile);
        await viewFile(localFile);

        console.tron.log('SUCCESS DOWNLOAD AND OPEN FILE: ' + downloadUrl);
      } else {
        await viewFile(thumbnail);

        console.tron.log('Only thumbnail url: ' + thumbnail);
      }
    } catch (err) {
      console.tron.log('ERROR on viewing attachments');
      console.tron.log(err);
    }

    setLoadingFilePress(false);
  };

  return (
    <TouchableOpacity
      disabled={loadingFilePress}
      onPress={handlePressFile}
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.lightestGray,
        borderRadius: moderateScale(4),
        marginTop: moderateScale(8, 0.3),
        height: ITEM_ATTACHMENT_HEIGHT,
        paddingHorizontal: Metrics.spaceHorizontal,

        elevation: 1,
        shadowColor: Colors.black,
        shadowRadius: 2,
        shadowOpacity: 0.16,
        shadowOffset: {
          width: 2,
          height: 2,
        },
      }}>
      <>
        <FastImage
          resizeMode={FastImage.resizeMode.contain}
          source={{
            uri: `data:image/png;base64, ${thumbnail}`,
          }}
          style={{width: THUMBNAIL_SIZE, height: THUMBNAIL_SIZE}}
        />
        <Text
          center
          color="primary"
          style={{
            marginTop: moderateScale(12, 0.3),
          }}
          weight="semibold"
          xsmall>
          {fileName}
        </Text>

        {loadingFilePress && (
          <View
            alignCenter
            justifyCenter
            style={{
              ...Metrics.absoluteFill,
              borderRadius: moderateScale(4),
              backgroundColor: 'rgba(0, 0, 0, 0.4)',
            }}>
            <Loader color={Colors.white} size="small" />
          </View>
        )}
      </>
    </TouchableOpacity>
  );
};
const Attachments = ({attachments, handleData}) => {
  const [doAttachments, setDOAttachments] = useState(attachments);

  const scrollRef = useRef(null);
  const alertRef = useRef(null);

  const setShowAlert = (shouldShow) => {
    if (shouldShow) {
      alertRef.current.show();
    } else {
      alertRef.current.hide();
    }
  };

  const handleUploadFiles = async () => {
    const file = await selectFilesDocumentPicker();
    console.tron.log('FILE', file);
    if (file) {
      setShowAlert(true);
      const dataFiles = await handleData(file);

      const fileWithThumbnail =
        dataFiles?.find((f) => f.filename === file.name) || {};

      const newFile = {
        id: fileWithThumbnail.id,
        thumbnail: fileWithThumbnail.thumbnail,
        fileName: fileWithThumbnail.filename,
        downloadUrl: fileWithThumbnail.download_url,
      };

      console.tron.log(
        'responseFile',
        {...fileWithThumbnail, thumbnail: !!fileWithThumbnail.thumbnail},
        {...newFile, thumbnail: !!newFile.thumbnail},
      );

      const arrWithNewAttachment = doAttachments.concat(newFile);
      const sortedArr = sortArrayByObjectKey(arrWithNewAttachment, 'fileName');

      console.tron.log('Before setting new sorted doAttachments', sortedArr);

      setDOAttachments(sortedArr);

      scrollRef.current.scrollTo({
        y:
          sortedArr.findIndex((a) => a.id === newFile.id) *
          ITEM_ATTACHMENT_HEIGHT,
        animated: true,
      });

      setShowAlert(false);
    }
  };

  return (
    <View isFlex>
      <ScrollView
        bounces={false}
        contentContainerStyle={{
          paddingTop: moderateScale(8, 0.1),
          paddingBottom:
            moderateScale(10, 0.3) +
            ApplicationStyles.floatingButtonLength +
            moderateScale(16, 0.3),
        }}
        ref={scrollRef}>
        <Content>
          {doAttachments.length === 0 && (
            <Label center darkerGray style={{marginTop: moderateScale(8)}}>
              No files attached
            </Label>
          )}
          {doAttachments.length > 0 &&
            doAttachments.map(({id, ...attachment}, index) => {
              return <File attachment={attachment} key={`${id}-${index}`} />;
            })}
        </Content>
      </ScrollView>
      <View
        alignCenter
        pointerEvents="box-none"
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          paddingVertical: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
          // backgroundColor: 'red'
        }}>
        <IconButton
          containerStyle={{
            height: 'auto',
            marginTop: Metrics.spaceHorizontal,
            // paddingHorizontal: 0
          }}
          icon="Plus"
          iconFill={Colors.white}
          iconSize={moderateScale(16, 0.1)}
          isAnimated
          onPress={handleUploadFiles}
          style={{
            width: ApplicationStyles.floatingButtonLength,
            height: ApplicationStyles.floatingButtonLength,
            borderRadius: ApplicationStyles.floatingButtonLength,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Colors.black,

            elevation: 3,
            shadowColor: Colors.black,
            shadowRadius: 5,
            shadowOpacity: 0.16,
            shadowOffset: {
              width: 0,
              height: 4,
            },
          }}
        />
      </View>

      <Alert
        disableBackdropPress={true}
        ref={alertRef}
        style={
          {
            // backgroundColor: Colors.white,
          }
        }>
        <View
          isFlex
          style={
            {
              // paddingHorizontal: moderateScale(20),
              // paddingVertical: moderateScale(20),
            }
          }>
          <View
            alignCenter
            justifyCenter
            row
            style={{
              backgroundColor: Colors.white,
              borderRadius: 14,
              height: moderateScale(72),
            }}>
            <View
              style={{
                position: 'absolute',
                left: moderateScale(24),
              }}>
              <Loader />
            </View>
            <Text color="darkerGray" large>
              Uploading file...
            </Text>
          </View>
        </View>
      </Alert>
    </View>
  );
};

export default Attachments;
