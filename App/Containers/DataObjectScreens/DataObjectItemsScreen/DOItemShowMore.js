import React, {useCallback, useRef, useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {View, Modal} from '../../../Components/Layout';
import SvgIcon from '../../../Components/SvgIcon';
import {Text} from '../../../Components/Text';
import {IconButton} from '../../../Components/Buttons';
import {Metrics, Colors} from '../../../Themes';
import DataObjectTabNavigation from '../../../Navigation/DataObjectTabNavigation';
import {moderateScale} from 'react-native-size-matters/extend';
import ContainerContext from '../../../Containers/Shared/ContainerContext';
import {useNavigation} from '@react-navigation/native';

export const modalWidth = Metrics.isTablet
  ? moderateScale(320)
  : Metrics.wp(100) - Metrics.spaceHorizontal * 2;

const DOItemShowMore = (props) => {
  const {contactId, doId, fields, item} = props;
  const navigation = useNavigation();

  const handleShow = async () => {
    navigation.navigate('DataObjectTabs', {
      contactId,
      doId,
      doItemId: item.id,
      fields,
      items: item.data,
      connectedContacts: item.connectedContacts,
      attachments: item.attachments,
      notes: item.notes,
    });
  };

  return (
    <TouchableOpacity
      onPress={() => handleShow(true)}
      style={{
        position: 'absolute',
        bottom: 0,
        right: 0,
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: 'red',
        paddingTop: moderateScale(20, 0.1),
        paddingHorizontal: Metrics.spaceHorizontal,
      }}>
      <Text
        color="yellow"
        style={{
          marginRight: moderateScale(4, 0.3),
        }}
        xxsmall>
        Show more
      </Text>
      <SvgIcon
        fill={Colors.yellow}
        height={moderateScale(10, 0.3)}
        name="RightChevron"
        width={moderateScale(10, 0.3)}
      />
    </TouchableOpacity>
  );
};

export default DOItemShowMore;
