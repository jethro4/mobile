import React, {Fragment, useCallback, useEffect, useRef, useState} from 'react';
import {Platform, ScrollView, TouchableOpacity} from 'react-native';
import {Content, View, Modal} from '../../../Components/Layout';
import {sessionVar} from '../../../Cache';
import {
  MultiSelect,
  PickerWrapper,
  PickerContextContainer,
} from '../../../Components/Forms';
import {PICKER_ITEM_HEIGHT} from '../../../Components/Forms/Pickers/MultiSelect';
import PanelHeader from '../../../Components/Forms/Panel/PanelHeader';
import {Label, Text} from '../../../Components/Text';
import Loader from '../../../Components/Loader';
import {Button} from '../../../Components/Buttons';
import {BUTTON_HEIGHT} from '../../../Components/Buttons/Button';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {sortArrayByKeyCondition} from '../../../Utils/array';
import {moderateScale} from 'react-native-size-matters/extend';
import {useQuery, useLazyQuery} from '@apollo/client';
import {
  LIST_RELATIONSHIPS,
  CONNECT_OR_DISCONNECT_RELATIONSHIP,
} from '../../../GraphQL/DataObjects';
import {LIST_CONTACTS} from '../../../GraphQL/Contacts';
import R from 'ramda';
import {useMutation} from '@apollo/client';
import {debounce} from 'lodash';
import SvgIcon from '../../../Components/SvgIcon';
import {IconButton} from '../../../Components/Buttons';
import {FormInput} from '../../../Components/Forms';
import {InfiniteScrollList} from '../../../Components/Layout';

const ITEM_CONTACT_SEARCH_HEIGHT = moderateScale(
  Metrics.isAndroid ? 74 : 60,
  0.4,
);
const ITEM_RELATIONSHIP_HEIGHT = moderateScale(100, 0.3);
const CONTACT_ITEMS_LIMIT = 20;
const SEARCH_ICON_SIZE = moderateScale(18, 0.3);
const CONNECT_FILTER_HEIGHT = moderateScale(38, 0.3);
const SAVE_CHANGES_BUTTON_HEIGHT = BUTTON_HEIGHT;
const {height} = ApplicationStyles.formRow;

export const getContactRelationships = (contacts) => {
  const allContactRelationships = [];

  contacts.forEach((contact) => {
    const contactRelationships = contact.relationships.map((r) => r.role);
    contactRelationships.forEach((r) => {
      if (!allContactRelationships.includes(r)) {
        allContactRelationships.push(r);
      }
    });
  });

  return allContactRelationships;
};

export const getFilteredRelationshipsByLimit = (
  contactRelationships,
  allRelationships,
) => {
  const filteredRelationships = allRelationships.filter((r) => {
    if (!r.hasLimit) {
      return true;
    }

    return !contactRelationships.includes(r.role);
  });

  return filteredRelationships.map((r) => r.role);
};

const ConnectedContact = ({
  allRelationships,
  name,
  email,
  filteredRelationships,
  relationships,
  navigation,
  onSelectRelationships,
}) => {
  const [
    callQuery,
    {loading: loadingContacts, error, data = {}},
  ] = useLazyQuery(LIST_CONTACTS);

  const handleViewContact = () => {
    callQuery({variables: {page: 0, limit: 1, q: `email:${email}`}});
  };

  useEffect(() => {
    if (data.listContacts) {
      const contact = data.listContacts.items[0];
      if (contact) {
        navigation.navigate({
          name: 'ContactDetails',
          key: 'ContactDetails-2',
          params: {contact, __isFromDO: true},
        });
      }
    }
  }, [data.listContacts]);

  return (
    <View
      justifyCenter
      style={{
        height: ITEM_RELATIONSHIP_HEIGHT,
      }}>
      <TouchableOpacity
        disabled={loadingContacts}
        onPress={handleViewContact}
        style={{alignSelf: 'flex-start'}}>
        {!loadingContacts ? (
          <>
            <Text ellipsizeMode="tail" numberOfLines={1} weight="medium">
              {name}
            </Text>
            <Text
              color="darkerGray"
              ellipsizeMode="tail"
              numberOfLines={1}
              small
              // style={{
              //   marginTop: moderateScale(4, 0.3),
              // }}
              weight="medium">
              {email}
            </Text>
          </>
        ) : (
          <Loader />
        )}
      </TouchableOpacity>
      <PickerWrapper>
        <MultiSelect
          allItems={allRelationships}
          containerStyle={{
            marginTop: moderateScale(4, 0.3),
          }}
          defaultValue={relationships}
          iconFill={Colors.black}
          items={filteredRelationships}
          labelStyle={{
            color: Colors.darkerGray,
            flex: 1,
          }}
          onSelectPickerItems={onSelectRelationships}
          style={{
            height: PICKER_ITEM_HEIGHT,
            justifyContent: 'space-between',
          }}
        />
      </PickerWrapper>
    </View>
  );
};

const ContactSearch = ({
  handleConnectContact,
  handleShowConnect,
  selectedConnectedContacts,
}) => {
  const searchRef = useRef();
  const listRef = useRef();
  const [searchText, setSearchText] = useState('');
  const [selectedContactIds, setSelectedContactIds] = useState(
    Object.keys(selectedConnectedContacts),
  );
  const [
    callQuery,
    {loading: loadingContacts, error, data: dataContacts = {}},
  ] = useLazyQuery(LIST_CONTACTS);

  const filterData = useCallback(
    debounce((search = '', page = 0, limit = CONTACT_ITEMS_LIMIT, noReset) => {
      if (!noReset && page === 0) {
        listRef.current.resetPage();
      }

      const {sessionId} = sessionVar();

      console.tron.log(
        `filterData search: ${search}, page: ${page}, limit: ${limit}, sessionId: ${sessionId}`,
      );

      const payload = {page, limit, sessionId};

      callQuery({variables: {...payload, q: search}});
    }, 400),
    [],
  );

  const handleSearchText = (text) => {
    setSearchText(text);
    filterData(text);
  };

  const handleOnScrollBeginDrag = () => {
    searchRef.current.blur();
  };

  useEffect(() => {
    setSelectedContactIds(Object.keys(selectedConnectedContacts));
  }, [selectedConnectedContacts]);

  return (
    <View
      style={{
        backgroundColor: Colors.white,
        borderRadius: 14,
        overflow: 'hidden',
        width: Metrics.isTablet
          ? moderateScale(320)
          : Metrics.wp(100) - Metrics.spaceHorizontal * 2,
        paddingTop: moderateScale(8, 0.1),
      }}>
      <View alignCenter justifyBetween row>
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            alignItems: 'center',
          }}>
          <Text center color="primary" weight="semibold" xlarge>
            Connect Other Contact
          </Text>
        </View>

        <IconButton
          containerStyle={{
            alignSelf: 'flex-end',
            alignItems: 'flex-end',
          }}
          icon="Close"
          iconFill={Colors.darkerGray}
          iconSize={moderateScale(16, 0.3)}
          isAnimated
          onPress={() => handleShowConnect(false)}
        />
      </View>

      <View
        style={{
          height: Metrics.hp(74),
        }}>
        <View justifyCenter>
          <FormInput
            // containerStyle={{
            //   backgroundColor: 'red',
            // }}
            autoFocus
            noBorder
            noMargin
            onChangeText={(text) => handleSearchText(text)}
            placeholder="Type contact name..."
            placeholderTextColor={Colors.darkerGray}
            ref={searchRef}
            style={{
              backgroundColor: 'transparent',
              borderBottomColor: Colors.lightestGray,
              borderBottomWidth: 1,
              color: Colors.text,
              fontSize: moderateScale(16, 0.3),
              height: moderateScale(50, 0.3),
              // paddingLeft:
              //   SEARCH_ICON_SIZE +
              //   Metrics.spaceHorizontal +
              //   moderateScale(12),
            }}
          />

          {/* <View
          pointerEvents="none"
          style={{
            position: 'absolute',
            left: Metrics.spaceHorizontal,
          }}>
          <SvgIcon
            fill={Colors.lighterGray}
            height={SEARCH_ICON_SIZE}
            name="Search"
            width={SEARCH_ICON_SIZE}
          />
        </View> */}
        </View>
        <View
          isFlex
          style={{
            backgroundColor: Colors.lightestGray,
          }}>
          <InfiniteScrollList
            contentContainerStyle={{
              paddingVertical: moderateScale(8, 0.3),
            }}
            data={dataContacts.listContacts && dataContacts.listContacts.items}
            getData={(page, limit) => {
              filterData(searchText, page - 1, limit, true);
            }}
            getItemLayout={(data, index) => ({
              length: ITEM_CONTACT_SEARCH_HEIGHT,
              offset: ITEM_CONTACT_SEARCH_HEIGHT * index,
              index,
            })}
            keyboardShouldPersistTaps="always"
            keyExtractor={(item, index) => item.id || String(index)}
            // keyExtractor={(item, index) => String(index)}
            loading={loadingContacts}
            onScrollBeginDrag={handleOnScrollBeginDrag}
            pageItemsLength={CONTACT_ITEMS_LIMIT}
            ref={listRef}
            renderItem={({item}) => {
              const isAlreadySelected = selectedContactIds.includes(item.id);

              return (
                <TouchableOpacity
                  disabled={isAlreadySelected}
                  onPress={handleConnectContact(item)}
                  style={{
                    backgroundColor: isAlreadySelected
                      ? Colors.lighterGray
                      : Colors.white,
                    borderBottomColor: Colors.border,
                    borderBottomWidth: 1,
                    height: ITEM_CONTACT_SEARCH_HEIGHT,
                    justifyContent: 'center',
                    paddingHorizontal: moderateScale(12, 0.3),
                  }}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    weight="medium">{`${item.firstName} ${item.lastName}`}</Text>
                  <Text
                    color="darkerGray"
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    small
                    style={{
                      marginTop: moderateScale(4, 0.3),
                    }}
                    weight="medium">
                    {item.email}
                  </Text>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </View>
    </View>
  );
};

const Relationships = ({
  connectedContacts: relationshipConnectedContacts,
  contactId,
  handleData,
  navigation,
  relationships: data,
}) => {
  const [connectedContacts, setConnectedContacts] = useState(
    relationshipConnectedContacts,
  );
  const [sortedContacts, setSortedContacts] = useState(null);
  const [filteredRelationships, setFilteredRelationships] = useState(null);
  const [customFilters, setCustomFilters] = useState({});
  const [updatedFilters, setUpdatedFilters] = useState(null);
  const [loadingSubmit, setLoadingSubmit] = useState(null);

  const scrollRef = useRef(null);
  const modalRef = useRef();
  const pickersRef = useRef(null);

  const handleConnectContact = ({id, firstName, lastName, email}) => () => {
    setConnectedContacts((state) =>
      state.concat({
        contactId: id,
        firstName,
        lastName,
        email,
        relationships: [],
      }),
    );
    handleShowConnect(false);

    scrollRef.current.scrollTo({
      y: (connectedContacts.length - 1) * ITEM_RELATIONSHIP_HEIGHT,
      animated: true,
    });
  };

  const handleShowConnect = (shouldShow) => {
    if (shouldShow) {
      modalRef.current.show();
    } else {
      modalRef.current.hide();
    }
  };

  const handleSave = useCallback(async () => {
    pickersRef.current.closeAll();
    setLoadingSubmit(true);
    await handleData(updatedFilters);
    setLoadingSubmit(false);
    setUpdatedFilters(null);
  }, [updatedFilters, handleData]);

  useEffect(() => {
    setSortedContacts(
      sortArrayByKeyCondition(
        connectedContacts,
        'contactId',
        (value) => value === contactId,
      ),
    );
  }, [connectedContacts, contactId]);

  useEffect(() => {
    if (sortedContacts) {
      let initCustomFilters = {};
      sortedContacts.forEach(
        ({contactId: connectedContactId, relationships}) => {
          initCustomFilters[connectedContactId] = relationships.map(
            (r) => r.role,
          );
        },
      );

      setCustomFilters(initCustomFilters);
    }
  }, [sortedContacts]);

  useEffect(() => {
    if (sortedContacts && data.length) {
      const contactRelationships = getContactRelationships(sortedContacts);
      const filteredRelationshipsByLimit = getFilteredRelationshipsByLimit(
        contactRelationships,
        data,
      );

      setFilteredRelationships(filteredRelationshipsByLimit);
    }
  }, [sortedContacts, data]);

  const handleSelectRelationships = (connectedContactId) => (
    contactRelationships,
    relationship,
  ) => {
    const isAdded = contactRelationships.includes(relationship);
    const customFilter = customFilters[connectedContactId] || [];

    const updatedCustomFilters = {
      ...customFilters,
      [connectedContactId]: isAdded
        ? customFilter.concat(relationship)
        : customFilter.filter((r) => r !== relationship),
    };

    const allCustomRelationships = R.flatten(
      Object.values(updatedCustomFilters),
    );
    const filteredRelationshipsByLimit = getFilteredRelationshipsByLimit(
      R.uniq([...allCustomRelationships, ...contactRelationships]),
      data,
    );

    // console.log(
    //   'updatedCustomFilters',
    //   updatedCustomFilters,
    //   contactRelationships,
    //   relationship,
    // );

    setCustomFilters(updatedCustomFilters);
    setFilteredRelationships(filteredRelationshipsByLimit);

    // handleSave(connectedContactId, contactRelationships);
    setUpdatedFilters((state) => ({
      ...state,
      [connectedContactId]: contactRelationships,
    }));
  };

  if (!filteredRelationships) {
    return <Loader style={{marginTop: moderateScale(20, 0.3)}} />;
  }

  const allRelationships = data.map((r) => r.role);
  const shouldDisableSave =
    !updatedFilters ||
    Object.values(customFilters).some(
      (relationships) => relationships.length === 0,
    );

  return (
    <>
      <View
        isFlex
        style={{
          marginTop: moderateScale(8, 0.1),
        }}>
        <TouchableOpacity
          onPress={() => handleShowConnect(true)}
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            height: '100%',
            justifyContent: 'center',
            paddingLeft: moderateScale(12, 0.3),
            paddingRight: Metrics.spaceHorizontal,
            height: CONNECT_FILTER_HEIGHT,
            justifyContent: 'flex-end',
          }}>
          <>
            <Label
              color="primary"
              style={{
                marginRight: moderateScale(4, 0.3),
              }}>
              Connect Other Contact
            </Label>
            <SvgIcon
              fill={Colors.primary}
              height={moderateScale(12, 0.3)}
              name="Plus"
              width={moderateScale(12, 0.3)}
            />
          </>
        </TouchableOpacity>
        <PickerContextContainer ref={pickersRef}>
          <ScrollView
            bounces={false}
            contentContainerStyle={{
              // paddingTop: moderateScale(8, 0.1),
              paddingBottom:
                SAVE_CHANGES_BUTTON_HEIGHT +
                moderateScale(20, 0.3) +
                moderateScale(10, 0.1) +
                PICKER_ITEM_HEIGHT * 5.5,
            }}
            ref={scrollRef}>
            <View>
              {sortedContacts.map(
                (
                  {
                    contactId: connectedContactId,
                    email,
                    firstName,
                    lastName,
                    relationships,
                  },
                  index,
                ) => {
                  const isSelectedContact = connectedContactId === contactId;
                  const name = firstName + ' ' + lastName;
                  const contactFilterSelection = filteredRelationships.concat(
                    customFilters[connectedContactId],
                  );
                  const sortedContactFilterSelection = allRelationships.filter(
                    (r) => contactFilterSelection.includes(r),
                  );
                  const transformedRelationships = relationships.map(
                    (r) => r.role,
                  );
                  const sortedRelationhips = allRelationships.filter((r) =>
                    transformedRelationships.includes(r),
                  );

                  return (
                    <Fragment key={connectedContactId}>
                      <Content
                        style={{
                          ...Platform.select({
                            ios: {
                              zIndex: sortedContacts.length + 1 - index,
                            },
                          }),
                        }}>
                        <ConnectedContact
                          allRelationships={allRelationships}
                          email={email}
                          filteredRelationships={sortedContactFilterSelection}
                          key={connectedContactId}
                          name={name}
                          navigation={navigation}
                          onSelectRelationships={handleSelectRelationships(
                            connectedContactId,
                          )}
                          relationships={sortedRelationhips}
                        />
                      </Content>

                      {isSelectedContact && connectedContacts.length > 1 && (
                        <PanelHeader
                          label="Other Related Contacts"
                          labelProps={{
                            weight: 'normal',
                          }}
                          // labelStyle={{
                          //   textTransform: 'uppercase',
                          //   color: Colors.mutedText,
                          //   marginTop: moderateScale(15, 0.1),
                          //   fontWeight: 'bold',
                          //   fontSize: moderateScale(
                          //     ApplicationStyles.fontSizes.xsmall,
                          //     0.5,
                          //   ),
                          // }}
                          style={{
                            marginTop: moderateScale(12, 0.3),
                            // backgroundColor: Colors.white,
                            // borderBottomWidth: 0,
                            borderBottomColor: Colors.lightestGray,
                            // height: ApplicationStyles.panelHeader.height * 0.8,
                            // paddingHorizontal: 0,
                          }}
                        />
                      )}
                    </Fragment>
                  );
                },
              )}
            </View>
          </ScrollView>
        </PickerContextContainer>
        <View
          style={{
            position: 'absolute',
            bottom: moderateScale(20, 0.3),
            width: '100%',
            alignSelf: 'center',
            paddingHorizontal: Metrics.spaceHorizontal,
          }}>
          <Button
            disabled={shouldDisableSave}
            isLoading={loadingSubmit}
            label="Save Changes"
            onPress={handleSave}
            // icon={
            //   <View style={{
            //     marginLeft: 10
            //   }}>
            //     <SvgIcon fill={Colors.white} name='RightChevron' width={moderateScale(10)} height={moderateScale(10)}/>
            //   </View>
            // }
          />
        </View>
      </View>
      <Modal alignCenter justifyCenter ref={modalRef}>
        <ContactSearch
          handleConnectContact={handleConnectContact}
          handleShowConnect={handleShowConnect}
          selectedConnectedContacts={customFilters}
        />
      </Modal>
    </>
  );
};

export default Relationships;
