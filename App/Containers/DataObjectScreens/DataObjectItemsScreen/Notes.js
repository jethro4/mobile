import React, {Fragment, useEffect, useState} from 'react';
import {ScrollView} from 'react-native';
import {Content, View} from '../../../Components/Layout';
import {IconButton} from '../../../Components/Buttons';
import {useReactiveVar} from '../../../Hooks';
import PanelHeader from '../../../Components/Forms/Panel/PanelHeader';
import {Label, Text} from '../../../Components/Text';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {sortArrayByKeyCondition} from '../../../Utils/array';
import {moderateScale} from 'react-native-size-matters/extend';
import Item from '../../NoteTaskScreens/NoteScreen/Item';

const Notes = ({contactId, notes, navigation, handleData, doNoteValues}) => {
  const [doNotes, setDONotes] = useState(notes);
  useEffect(() => {
    if (doNoteValues) {
      const {type, tags, ...values} = doNoteValues;
      setDONotes((state) =>
        [
          {
            id: `local-note-${state ? state.length : 0}`,
            type,
            tags: [type.toLowerCase()].concat(tags),
            ...values,
          },
        ].concat(state),
      );
    }
  }, [doNoteValues]);

  return (
    <View isFlex>
      <ScrollView
        bounces={false}
        contentContainerStyle={{
          paddingTop: moderateScale(16, 0.1),
          paddingBottom:
            moderateScale(10, 0.1) +
            ApplicationStyles.floatingButtonLength +
            moderateScale(16, 0.3),
        }}
        style={{
          backgroundColor:
            doNotes.length === 0 ? Colors.white : Colors.superLightGray,
        }}>
        {doNotes.length === 0 && (
          <Label center darkerGray style={{marginTop: moderateScale(8)}}>
            No notes found
          </Label>
        )}
        {doNotes.length > 0 &&
          doNotes.map((item) => <Item key={item.id} item={item} />)}
      </ScrollView>

      <View
        alignCenter
        pointerEvents="box-none"
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          paddingVertical: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
          // backgroundColor: 'red'
        }}>
        <IconButton
          containerStyle={{
            height: 'auto',
            marginTop: Metrics.spaceHorizontal,
            // paddingHorizontal: 0
          }}
          icon="Plus"
          iconFill={Colors.white}
          iconSize={moderateScale(16, 0.1)}
          isAnimated
          onPress={() => {
            navigation.navigate('NoteTaskAdd', {
              contactId,
              isTask: false,
              handleSubmit: handleData,
              // onSuccess: reloadData,
            });
          }}
          style={{
            width: ApplicationStyles.floatingButtonLength,
            height: ApplicationStyles.floatingButtonLength,
            borderRadius: ApplicationStyles.floatingButtonLength,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Colors.black,

            elevation: 3,
            shadowColor: Colors.black,
            shadowRadius: 5,
            shadowOpacity: 0.16,
            shadowOffset: {
              width: 0,
              height: 4,
            },
          }}
        />
      </View>
    </View>
  );
};

export default Notes;
