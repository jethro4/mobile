import React, {useCallback, useEffect} from 'react';
import {Alert, View, FlatList} from '../../../Components/Layout';
import {LIST_DATA_OBJECT_ITEMS} from '../../../GraphQL/DataObjects';
import ScreenContainer from '../../Shared/ScreenContainer';
import DataObjectItem from './Item';
import {moderateScale} from 'react-native-size-matters/extend';
import {useQuery, useLazyQuery} from '@apollo/client';
import {GET_USER_DETAILS} from '../../../GraphQL/Cache';
import {IconButton} from '../../../Components/Buttons';
import {useNavigation} from '@react-navigation/native';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {userDetailsVar} from '../../../Cache';

const DataObjectItemsScreen = (props) => {
  const {doId, doTitle, contact} = props.route.params;
  console.tron.log('contact', contact);
  const navigation = useNavigation();

  const [callQueryDOItems, {loading, error, data = {}}] = useLazyQuery(
    LIST_DATA_OBJECT_ITEMS,
  );

  console.tron.log('dataObject items', loading, error, data);

  const reloadData = useCallback(() => {
    const {email} = userDetailsVar();
    console.tron.log('onRefresh Data Object Items reloadData');
    callQueryDOItems({
      variables: {
        groupId: doId,
        contactId: contact.id,
        email,
      },
    });
  }, [callQueryDOItems, doId, contact.id]);

  useEffect(() => {
    const {email} = userDetailsVar();
    if (email) {
      callQueryDOItems({
        variables: {
          groupId: doId,
          contactId: contact.id,
          email,
        },
      });
    }
  }, [callQueryDOItems]);

  useEffect(() => {
    if (props.route.params?.hasItemSaved) {
      reloadData();
    }
  }, [props.route.params, reloadData]);

  return (
    <ScreenContainer
      headerTitle={doTitle}
      // headerRight={
      //   <IconButton
      //     onPress={() => {}}
      //     icon='Plus'
      //     iconFill={Colors.white}
      //   />
      // }
    >
      <FlatList
        contentContainerStyle={{
          paddingTop: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
          paddingBottom:
            moderateScale(10, 0.3) +
            ApplicationStyles.floatingButtonLength +
            moderateScale(16, 0.3),
        }}
        data={data.listDataObjectItems && data.listDataObjectItems.items}
        keyExtractor={(item, index) => item.id || String(index)}
        loading={loading}
        onRefresh={() => reloadData()}
        renderItem={({item}) => (
          <DataObjectItem
            contactId={contact.id}
            doId={doId}
            fields={data.listDataObjectItems && data.listDataObjectItems.fields}
            item={item}
          />
        )}
      />

      <View
        alignCenter
        pointerEvents="box-none"
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          paddingVertical: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
          // backgroundColor: 'red'
        }}>
        <IconButton
          containerStyle={{
            height: 'auto',
            marginTop: Metrics.spaceHorizontal,
            // paddingHorizontal: 0
          }}
          disabled={!data.listDataObjectItems}
          icon="Plus"
          iconFill={Colors.white}
          iconSize={moderateScale(16, 0.1)}
          isAnimated
          onPress={() => {
            navigation.navigate('DataObjectTabs', {
              contactId: contact.id,
              doId,
              fields: data.listDataObjectItems.fields,
              items: [],
              connectedContacts: [
                {...contact, contactId: contact.id, relationships: []},
              ],
              attachments: [],
              notes: [],
              isCreate: true,
              // onSuccess: reloadData,
            });
          }}
          style={{
            width: ApplicationStyles.floatingButtonLength,
            height: ApplicationStyles.floatingButtonLength,
            borderRadius: ApplicationStyles.floatingButtonLength,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Colors.black,

            elevation: 3,
            shadowColor: Colors.black,
            shadowRadius: 5,
            shadowOpacity: 0.16,
            shadowOffset: {
              width: 0,
              height: 4,
            },
          }}
        />
      </View>
    </ScreenContainer>
  );
};

DataObjectItemsScreen.defaultProps = {};

export default DataObjectItemsScreen;
