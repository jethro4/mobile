import React, {useEffect, useRef, useState} from 'react';
import {Platform, Alert, ScrollView, TouchableOpacity} from 'react-native';
import {View, Content} from '../../../Components/Layout';

import Panel from '../../../Components/Forms/Panel';
import PanelHeader from '../../../Components/Forms/Panel/PanelHeader';
import {Text} from '../../../Components/Text';

import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {
  sortArrayByPriority,
  sortArrayByKeyCondition,
} from '../../../Utils/array';

import {moderateScale} from 'react-native-size-matters/extend';
import {
  FormInput,
  FormArea,
  Select,
  PickerWrapper,
  PickerContextContainer,
  RadioForm,
  CheckboxForm,
} from '../../../Components/Forms';
import {PICKER_ITEM_HEIGHT as SINGLE_SELECT_PICKER} from '../../../Components/Forms/Pickers/Select';
import copyToClipboard from '../../../Utils/copyToClipboard';
import {Button} from '../../../Components/Buttons';
import {BUTTON_HEIGHT} from '../../../Components/Buttons/Button';
import {PICKER_ITEM_HEIGHT} from '../../../Components/Forms/Pickers/MultiSelect';
import FormDatePicker from '../../Shared/Forms/FormDatePicker';
import {CREATE_OR_UPDATE_DO_ITEMS} from '../../../GraphQL/DataObjects';
import {useMutation} from '@apollo/client';

const SECTION_FILTER_HEIGHT = moderateScale(38, 0.3);
const SAVE_CHANGES_BUTTON_HEIGHT = BUTTON_HEIGHT;

export const convertWithSections = (itemData) => {
  return itemData.reduce((acc, d) => {
    const sections = {...acc};
    const sectionName = d.sectionTag || 'General';
    if (!sections[sectionName]) {
      sections[sectionName] = {subGroups: {}};
    }

    const section = sections[sectionName];
    const subGroups = section.subGroups;
    const subGroupName = d.subGroup || 'subGroupWithoutName';
    if (!subGroups[subGroupName]) {
      subGroups[subGroupName] = [];
    }

    const subGroup = subGroups[subGroupName];
    subGroup.push(d);

    return sections;
  }, {});
};

export const transformSectionsData = (sectionsObj) => {
  const transformedSectionsData = Object.entries(sectionsObj).map(
    ([sectionName, sectionData]) => {
      const transformedSubGroups = Object.entries(sectionData.subGroups).map(
        ([subGroupName, data]) => {
          return {
            subGroupName,
            data: data.map((d) => ({
              fieldId: d.fieldId,
              fieldName: d.name,
              value: d.value || d.default || '',
            })),
          };
        },
      );
      const sortedSubGroups = sortArrayByPriority(
        transformedSubGroups,
        'subGroupName',
        ['subGroupWithoutName'],
      );

      return {
        sectionName,
        subGroups: sortedSubGroups,
      };
    },
  );

  const sortedSectionsData = sortArrayByPriority(
    transformedSectionsData,
    'sectionName',
    ['sectionWithoutName'],
  );

  const sortedSectionsGeneral = sortArrayByKeyCondition(
    sortedSectionsData,
    'sectionName',
    (value) => value === 'General',
  );

  return sortedSectionsGeneral;
};

const getSectionsData = (itemData) => {
  const sectionsObj = convertWithSections(itemData);

  const transformedSectionsData = transformSectionsData(sectionsObj);

  return transformedSectionsData;
};

const Details = (props) => {
  const {isCreate, fields, items, handleData} = props;
  const scrollRef = useRef(null);
  const pickersRef = useRef(null);
  const requiredFields = useRef(
    fields.filter((field) => !!field.required).map((field) => field.name),
  ).current;
  const [sectionsData, setSectionsData] = useState([]);
  const [sectionNames, setSectionNames] = useState([]);
  const [selectedFilter, setSelectedFilter] = useState('All');
  const [filteredSectionsData, setFilteredSectionsData] = useState([]);
  const [updatedDetails, setUpdatedDetails] = useState(null);
  const [loadingSaveDetails, setLoadingSaveDetails] = useState(false);

  const handleFilterBySection = (value) => {
    setSelectedFilter(value);
    scrollRef.current.scrollTo({
      y: 0,
      animated: false,
    });
  };

  const copyToClipboardFn = (text) => () => {
    copyToClipboard(text);
  };

  const handleChange = (type) => (value) => {
    console.tron.log('handleChange', type, value);

    setUpdatedDetails((state) => ({
      ...state,
      [type]: value,
    }));
  };

  const handleSave = async () => {
    let requiredFieldsErr = [];

    pickersRef.current.closeAll();

    requiredFields.forEach((field) => {
      const hasValue =
        !!updatedDetails[field] ||
        (!updatedDetails.hasOwnProperty(field) &&
          items.some((item) => item.name === field && !!item.value));

      if (!hasValue) {
        requiredFieldsErr.push(field);
      }
    });

    if (requiredFieldsErr.length) {
      Alert.alert(
        `Required fields:${requiredFieldsErr.map((err) => `\n${err}`)}`,
      );
    } else {
      setLoadingSaveDetails(true);
      setUpdatedDetails(null);
      await handleData(updatedDetails);
      setLoadingSaveDetails(false);
    }
  };

  useEffect(() => {
    const allSections = getSectionsData(isCreate ? fields : items);
    const allSectionNames = allSections.map((section) => section.sectionName);

    setSectionsData(allSections);
    setSectionNames(['All', ...allSectionNames]);
  }, [items]);

  useEffect(() => {
    if (selectedFilter !== 'All') {
      const filteredData = sectionsData.filter(
        (section) => section.sectionName === selectedFilter,
      );

      setFilteredSectionsData(filteredData);
    } else {
      setFilteredSectionsData(sectionsData);
    }
  }, [selectedFilter, sectionsData, setFilteredSectionsData]);

  if (!sectionsData.length) {
    return null;
  }

  const shouldDisableSave = !updatedDetails;

  return (
    <View
      isFlex
      style={{
        marginTop: moderateScale(8, 0.1),
      }}>
      <Select
        defaultValue="All"
        items={sectionNames}
        labelPrefix="Section: "
        listTop={SECTION_FILTER_HEIGHT}
        onSelectPickerItem={handleFilterBySection}
        style={{
          paddingLeft: moderateScale(12, 0.3),
          paddingRight: Metrics.spaceHorizontal,
          height: SECTION_FILTER_HEIGHT,
          justifyContent: 'flex-end',
        }}
      />

      <PickerContextContainer ref={pickersRef}>
        <ScrollView bounces={false} ref={scrollRef}>
          {filteredSectionsData.map((section, sectionIndex) => {
            return (
              <Panel
                isAccordion={true}
                key={section.sectionName}
                label={section.sectionName}
                panelStyle={{
                  paddingBottom:
                    filteredSectionsData.length - 1 === sectionIndex
                      ? SAVE_CHANGES_BUTTON_HEIGHT +
                        moderateScale(20, 0.3) +
                        moderateScale(10, 0.1) +
                        PICKER_ITEM_HEIGHT * 5.5
                      : 0,
                  zIndex: filteredSectionsData.length + 1 - sectionIndex,
                }}>
                {section.subGroups.map((subGroup, subGroupIndex) => {
                  return (
                    <Content
                      key={subGroup.subGroupName}
                      style={{
                        zIndex: section.subGroups.length + 1 - subGroupIndex,
                      }}>
                      {subGroup.subGroupName !== 'subGroupWithoutName' && (
                        <PanelHeader
                          label={subGroup.subGroupName}
                          labelProps={{
                            weight: 'normal',
                          }}
                          labelStyle={{
                            textTransform: 'uppercase',
                            color: Colors.mutedText,
                            marginTop: moderateScale(15, 0.1),
                            fontWeight: 'bold',
                            fontSize: moderateScale(
                              ApplicationStyles.fontSizes.xsmall,
                              0.5,
                            ),
                          }}
                          style={{
                            backgroundColor: Colors.white,
                            borderBottomColor: Colors.border,
                            height: ApplicationStyles.panelHeader.height * 0.8,
                            paddingHorizontal: 0,
                          }}
                        />
                      )}
                      {subGroup.data.map((itemData, subGroupDataIndex) => {
                        const field = fields.find(
                          (f) => f.name === itemData.fieldName,
                        );
                        let renderFormComponent = () => (
                          <FormInput
                            // autoFocus
                            defaultValue={itemData.value}
                            // error={errors.address2Street1}
                            noBorder={false}
                            noMargin
                            onChangeText={handleChange(field.name)}
                            placeholder={
                              field.placeholder || itemData.fieldName
                            }
                            // onSubmitEditing={() => lastNameRef.current.focus()}
                            returnKeyType="next"
                            style={{
                              paddingLeft: 0,
                            }}
                          />
                        );

                        switch (field?.type) {
                          case 'TextArea': {
                            renderFormComponent = () => (
                              <FormArea
                                // autoFocus
                                defaultValue={itemData.value}
                                // error={errors.address2Street1}
                                noBorder={false}
                                noMargin
                                onChangeText={handleChange(field.name)}
                                placeholder={
                                  field.placeholder || itemData.fieldName
                                }
                                // onSubmitEditing={() => lastNameRef.current.focus()}
                                returnKeyType="next"
                                style={{
                                  paddingLeft: 0,
                                }}
                              />
                            );

                            break;
                          }
                          case 'Select': {
                            renderFormComponent = () => (
                              <PickerWrapper>
                                <Select
                                  defaultValue={itemData.value}
                                  iconFill={Colors.black}
                                  items={field.choices}
                                  labelStyle={{
                                    color: Colors.black,
                                    flex: 1,
                                    fontSize: moderateScale(
                                      ApplicationStyles.fontSizes.slarge,
                                      0.3,
                                    ),
                                  }}
                                  onSelectPickerItem={handleChange(field.name)}
                                  style={{
                                    height: SINGLE_SELECT_PICKER,
                                    justifyContent: 'space-between',
                                  }}
                                />
                              </PickerWrapper>
                            );

                            break;
                          }
                          case 'Date': {
                            renderFormComponent = () => (
                              <FormDatePicker
                                defaultDate={itemData.value}
                                format="YYYY-MM-DD"
                                iconSize={moderateScale(14, 0.3)}
                                label="Select Date"
                                mode="date"
                                onDateSelected={handleChange(field.name)}
                              />
                            );

                            break;
                          }
                          case 'DateTime': {
                            renderFormComponent = () => (
                              <FormDatePicker
                                defaultDate={itemData.value}
                                format="YYYY-MM-DD h:mmA"
                                iconSize={moderateScale(14, 0.3)}
                                label="Select Date & Time"
                                mode="datetime"
                                onDateSelected={handleChange(field.name)}
                              />
                            );

                            break;
                          }
                          case 'Radio': {
                            const showTypes = field.choices.map((choice) => ({
                              label: choice,
                              value: choice,
                            }));

                            renderFormComponent = () => (
                              <RadioForm
                                initial={showTypes.findIndex(
                                  (s) => s.value === itemData.value,
                                )}
                                onPress={(value) => {
                                  handleChange(field.name)(value);
                                }}
                                radio_props={showTypes}
                                formHorizontal={false}
                                labelHorizontal={true}
                                animation={true}
                                noBorder={false}
                              />
                            );

                            break;
                          }
                          case 'Checkbox': {
                            const values = field.choices.map((choice) => ({
                              label: choice,
                              value: choice,
                            }));

                            const initialValues = itemData.value
                              ?.split('|')
                              .filter((v) => !!v);

                            renderFormComponent = () => (
                              <CheckboxForm
                                initial={initialValues}
                                onPress={(cBoxValues) => {
                                  handleChange(field.name)(
                                    cBoxValues.map((v) => v.value).join('|'),
                                  );
                                }}
                                // style={{
                                //   justifyContent: 'space-between',
                                // }}
                                values={values}
                                formHorizontal={false}
                                labelHorizontal={true}
                                animation={true}
                                noBorder={false}
                              />
                            );

                            break;
                          }
                        }

                        return (
                          <View
                            key={itemData.fieldId}
                            style={{
                              paddingVertical: moderateScale(10, 0.1),
                              ...Platform.select({
                                ios: {
                                  zIndex:
                                    subGroup.data.length +
                                    1 -
                                    subGroupDataIndex,
                                },
                              }),
                              // borderBottomWidth: 1,
                              // borderBottomColor: Colors.border,
                            }}>
                            <TouchableOpacity
                              disabled={!itemData.value}
                              onPress={copyToClipboardFn(itemData.value)}
                              style={
                                {
                                  // flex: 0.4,
                                  // backgroundColor: 'pink'
                                }
                              }>
                              <Text small weight="semibold">
                                {itemData.fieldName}
                                {field.required && (
                                  <Text
                                    color="required"
                                    small
                                    weight="semibold">
                                    {' '}
                                    *
                                  </Text>
                                )}
                              </Text>
                            </TouchableOpacity>
                            {renderFormComponent()}
                          </View>
                        );
                      })}
                    </Content>
                  );
                })}
              </Panel>
            );
          })}
        </ScrollView>
      </PickerContextContainer>

      <View
        style={{
          position: 'absolute',
          bottom: moderateScale(20, 0.3),
          width: '100%',
          alignSelf: 'center',
          paddingHorizontal: Metrics.spaceHorizontal,
        }}>
        <Button
          disabled={shouldDisableSave}
          isLoading={loadingSaveDetails}
          label="Save Changes"
          onPress={handleSave}
        />
      </View>
    </View>
  );
};

export default Details;
