import React, {useEffect, useContext} from 'react';

import {View, FlatList} from '../../../Components/Layout';

import {LIST_DATA_OBJECTS} from '../../../GraphQL/DataObjects';
import ContactContext from '../../../Containers/ContactsScreens/ContactDetailsScreen/ContactContext';
import DataObjectType from './Item';
import {moderateScale} from 'react-native-size-matters/extend';
import {Metrics} from '../../../Themes';
import {useQuery} from '@apollo/client';

const DataObjectTypesScreen = (props) => {
  const {navigation} = props;
  const {contact, setInitialDataLoaded} = useContext(ContactContext);

  const {loading, error, data = {}, refetch} = useQuery(LIST_DATA_OBJECTS, {});

  console.tron.log('dataObject types', contact, loading, error, data);

  const reloadData = () => {
    console.tron.log('onRefresh Data Object Types reloadData');
    refetch({});
  };

  useEffect(() => {
    navigation.setOptions({
      title: 'Data Objects',
    });
  }, [navigation]);

  useEffect(() => {
    if (data.listDataObjects) {
      setInitialDataLoaded('dataObject');
    }
  }, [data.listDataObjects]);

  return (
    <View isFlex>
      <FlatList
        contentContainerStyle={{
          paddingTop: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
        }}
        data={data.listDataObjects}
        keyExtractor={(item, index) => item.id || String(index)}
        loading={loading}
        onRefresh={() => reloadData()}
        renderItem={({item}) => (
          <DataObjectType contact={contact} item={item} />
        )}
      />
    </View>
  );
};

DataObjectTypesScreen.defaultProps = {};

export default DataObjectTypesScreen;
