import React from 'react';
import {GET_ACCESS_PERMISSIONS} from '../../../GraphQL/Admin';
import {userDetailsVar} from '../../../Cache';
import {readGraphqlCache} from '../../../GraphQL';
import {useQuery} from '../../../Hooks';
import {hasAccessPermissions} from './helpers';

const Access = (props) => {
  const {type, name, isMutation, email, children} = props;

  if (email) {
    return <AccessByEmail {...props} />;
  } else if (hasAccess(type, name, isMutation)) {
    return children;
  }

  return null;
};

const AccessByEmail = (props) => {
  const {type, name, isMutation, email, children} = props;
  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {email});

  if (!data.getAccessPermissions) {
    return null;
  }

  if (hasAccessPermissions(data.getAccessPermissions, type, name, isMutation)) {
    return children;
  }

  return null;
};

export const getLoggedUserAccessPermissions = () => {
  const {email} = userDetailsVar();

  const dataAccessPermissions = readGraphqlCache(GET_ACCESS_PERMISSIONS, {
    email,
  });

  return dataAccessPermissions.getAccessPermissions;
};

export const hasAccess = (type, name, isMutation) => {
  const accessPermissions = getLoggedUserAccessPermissions();

  console.tron.log('accessPermissions', accessPermissions);

  return hasAccessPermissions(accessPermissions, type, name, isMutation);
};

export default Access;
