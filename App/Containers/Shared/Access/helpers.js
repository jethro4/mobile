import Const from '../../../Constants';

export const hasAccessPermissions = (
  accessPermissions,
  type,
  name,
  isMutation,
) => {
  if (accessPermissions.level === 'admin') {
    return true;
  }

  let permission;

  switch (type) {
    case 'section': {
      const {sections} = accessPermissions;
      const dataPermission = sections.reduce((value, section) => {
        const access = section.access.find((item) => name === item.name);

        if (access) {
          return access.permission;
        }
      }, '');

      permission = dataPermission;
      break;
    }
  }

  if (
    (permission === Const.ACCESS_LEVELS.READ_WRITE && isMutation) ||
    ([Const.ACCESS_LEVELS.READ_ONLY, Const.ACCESS_LEVELS.READ_WRITE].includes(
      permission,
    ) &&
      !isMutation)
  ) {
    return true;
  }

  return false;
};
