import React, {forwardRef, useState} from 'react';
import {StyleSheet} from 'react-native';
import {FormInput, Select} from '../../Components/Forms';
import SvgIcon from '../../Components/SvgIcon';

import {View} from '../../Components/Layout';
import {Colors, ApplicationStyles, Metrics} from '../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const {height} = ApplicationStyles.formRow;
const SEARCH_HEIGHT = moderateScale(54, 0.3);

const SEARCH_ICON_SIZE = moderateScale(18, 0.3);
const FILTER_ICON_SIZE = moderateScale(20, 0.3);

let Search = (props, ref) => {
  const [categoryBtnWidth, setCategoryBtnWidth] = useState(0);
  const {
    categories,
    onSort,
    onSelectPickerItem,
    defaultCategory,
    containerStyle,
    style,
    ...allProps
  } = props;

  const onSortFn = (value) => {
    onSort(value);
  };

  return (
    <>
      <View
        alignCenter
        row
        style={{
          backgroundColor: Colors.white,
          zIndex: 1000,
          height: SEARCH_HEIGHT,
          borderBottomWidth: 1,
          borderBottomColor: Colors.border,
          paddingRight: categoryBtnWidth,
          // paddingHorizontal: Metrics.spaceHorizontal,
          ...containerStyle,
        }}>
        <FormInput
          containerStyle={{
            flex: 1,
          }}
          noBorder
          noMargin
          placeholder="Search"
          placeholderTextColor={Colors.darkerGray}
          ref={ref}
          style={[styles.searchContainer, style]}
          {...allProps}
        />
        <View
          style={{
            width: 1,
            height: '56%',
            backgroundColor: Colors.border,
            // marginRight: moderateScale(12, 0.3),
          }}
        />
        <View
          pointerEvents="none"
          style={{
            position: 'absolute',
            left: Metrics.spaceHorizontal,
          }}>
          <SvgIcon
            fill={Colors.lightGray}
            height={SEARCH_ICON_SIZE}
            name="Search"
            width={SEARCH_ICON_SIZE}
          />
        </View>

        {/* <View
          style={{
            width: 1,
            backgroundColor: Colors.lightGray,
            height: '40%'
          }}
        /> */}
        {/* <ToggleButton
          onPress={onSortFn}
          iconUntoggled='SortDesc'
          iconToggled='SortAsc'
          iconSize={FILTER_ICON_SIZE}
          iconFill={Colors.primary}
        /> */}
      </View>
      <Select
        defaultValue={defaultCategory}
        items={categories}
        noBorder={true}
        onLayout={({nativeEvent}) => {
          const {layout} = nativeEvent;
          const {width} = layout;

          console.tron.log('search input layout', layout);
          setCategoryBtnWidth(width);
        }}
        onSelectPickerItem={onSelectPickerItem}
        style={{
          position: 'absolute',
          top: -SEARCH_HEIGHT,
          paddingLeft: moderateScale(12, 0.3),
          paddingRight: Metrics.spaceHorizontal,
          height: SEARCH_HEIGHT,
          zIndex: 1001,
          right: 0,
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: Colors.white,
    color: Colors.text,
    fontSize: moderateScale(16, 0.4),
    height: '100%',
    paddingLeft: SEARCH_ICON_SIZE + Metrics.spaceHorizontal + moderateScale(12),
  },
});

Search = forwardRef(Search);

Search.defaultProps = {
  onSort: () => {},
};

export default Search;
