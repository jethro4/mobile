import React, {useCallback, useState, useEffect} from 'react';
import {Container as ContainerView, View} from '../../Components/Layout';
import Header from './Header';
import ContainerContext from './ContainerContext';

const Container = (props) => {
  const {
    children,
    hasBack,
    noHeaderInset,
    headerStyle,
    headerTitle,
    headerLeft,
    headerCenter,
    headerRight,
    barStyle,
    onBack,
    ...allProps
  } = props;
  const [headerRightComp, setHeaderRightComp] = useState(headerRight);

  const setHeaderRight = useCallback((headerRight) => {
    setHeaderRightComp(headerRight);
  }, []);

  useEffect(() => {
    if (headerRight) {
      setHeaderRightComp(headerRight);
    }
  }, [headerRight]);

  return (
    <ContainerContext.Provider
      value={{
        setHeaderRight,
      }}>
      <ContainerView {...allProps}>
        <Header
          barStyle={barStyle}
          hasBack={hasBack}
          headerCenter={headerCenter}
          headerLeft={headerLeft}
          headerRight={headerRightComp}
          headerStyle={headerStyle}
          headerTitle={headerTitle}
          noInset={noHeaderInset}
          onBack={onBack}
        />
        <View isFlex>{children}</View>
      </ContainerView>
    </ContainerContext.Provider>
  );
};

export default Container;
