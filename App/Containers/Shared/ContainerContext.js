import React from 'react';

const ContainerContext = React.createContext({
  setHeaderRight: () => null,
});

export default ContainerContext;
