import React from 'react';
import Container from './Container';

const TabContainer = (props) => {
  return <Container {...props} />;
};

export default TabContainer;
