import React from 'react';
import Container from './Container';

const ScreenContainer = (props) => {
  return <Container hasBack hasBottomInset {...props} />;
};

export default ScreenContainer;
