import React, {
  forwardRef,
  useState,
  useEffect,
  useRef,
  useImperativeHandle,
} from 'react';
import {Animated, TouchableOpacity, TextInput, StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters/extend';
import {Metrics, Colors, ApplicationStyles} from '../../../Themes';
import SvgIcon from '../../../Components/SvgIcon';
import {Text, Label, ErrorMessage} from '../../../Components/Text';
import {View} from '../../../Components/Layout';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height, 0.3);
const CONTAINER_PADDING_TOP = 0;
const PADDING_TOP = CONTAINER_PADDING_TOP + moderateScale(22, 0.3);
const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);

let FormTagsInput = (props, ref) => {
  const {
    children,
    inputPaddingTop,
    inputPaddingLeft,
    onTagsUpdate,
    onSubmitEditing,
    noBorder,
    error,
    onFocus,
    onBlur,
    label,
    defaultTags,
    containerStyle,
    style,
    placeholderTextColor,
    ...allProps
  } = props;

  const containerPaddingTop = inputPaddingTop + (Metrics.isAndroid ? 4 : 0);
  const formPaddingTop = PADDING_TOP; // + inputPaddingTop

  const [tags, setTags] = useState(defaultTags);
  const [value, setValue] = useState('');
  const formRef = useRef();

  const [isFocused, setIsFocused] = useState(false);
  const animatedIsFocused = useRef(
    new Animated.Value(defaultTags.length ? 1 : 0),
  ).current;

  useImperativeHandle(ref, () => formRef.current, []);

  useEffect(() => {
    Animated.timing(animatedIsFocused, {
      useNativeDriver: false,
      toValue: isFocused || tags.length ? 1 : 0,
      duration: 200,
    }).start();
  }, [isFocused, tags]);

  handleFocus = () => {
    setIsFocused(true);
    onFocus();
  };
  handleBlur = () => {
    addTag(value);
    setValue('');

    setIsFocused(false);
    onBlur();
  };

  const onChangeTextFn = (text) => {
    const replacedText = text.replace(/\s+/g, '_');
    setValue(replacedText);
  };

  const onClear = () => {
    setValue('');
  };

  const setTagsFn = (tags) => {
    setTags(tags);
    onTagsUpdate(tags);
    console.tron.log('tags update', tags);
  };

  const addTag = (tag) => {
    if (tag && !tags.includes(tag)) {
      setTagsFn(tags.concat(tag));
    }
  };

  const removeTag = (tag) => {
    setTagsFn(tags.filter((oldTag) => oldTag !== tag));
  };

  const onAddTag = () => handleBlur();

  const showButton = isFocused || !!value;

  return (
    <View
      style={[
        styles.inputView,
        {
          paddingTop: containerPaddingTop,
        },
        containerStyle,
      ]}>
      <View>
        {tags.length > 0 && (
          <View
            style={[
              styles.tagsContainer,
              {
                paddingTop: inputPaddingTop * 2,
                paddingLeft: inputPaddingLeft,
              },
            ]}>
            {tags.map((tag) => {
              return (
                <TouchableOpacity
                  key={tag}
                  onPress={() => removeTag(tag)}
                  style={styles.tagContainer}>
                  <View
                    style={{marginRight: moderateScale(8, 0.3)}}
                    weight="medium">
                    <Text small>#{tag}</Text>
                  </View>
                  <SvgIcon
                    fill={Colors.darkerGray}
                    height={moderateScale(6, 0.3)}
                    name="Close"
                    width={moderateScale(6, 0.3)}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        )}

        <TextInput
          onBlur={handleBlur}
          onFocus={handleFocus}
          ref={formRef}
          {...allProps}
          blurOnSubmit
          onChangeText={onChangeTextFn}
          onSubmitEditing={({nativeEvent: {text}}) => {
            addTag(text);
            formRef.current.clear();
            onSubmitEditing();
            // setTimeout(() => formRef.current.focus(), 2000)
          }}
          placeholder={tags.length ? 'Add tags' : ''}
          placeholderTextColor={placeholderTextColor}
          style={[
            styles.inputContainer,
            {
              paddingTop: inputPaddingTop,
              paddingLeft: inputPaddingLeft,
            },
            !noBorder && {
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            },
            style,
          ]}
          underlineColorAndroid="transparent"
          value={value}
        />
      </View>

      <Animated.Text
        pointerEvents="none"
        style={{
          position: 'absolute',
          left: inputPaddingLeft,
          top: animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [formPaddingTop, inputPaddingTop],
          }),
          fontSize: animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [FONT_SIZE, moderateScale(10, 0.3)],
          }),
          color: animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [placeholderTextColor, Colors.darkerGray],
          }),
        }}>
        {label}
      </Animated.Text>

      {
        // tags.length > 0 &&
        // <TouchableOpacity
        // onPress={onClear}
        // style={{
        //   position: 'absolute',
        //   top: 0,
        //   bottom: 0,
        //   right: 0,
        //   width: moderateScale(32),
        //   justifyContent: 'center',
        //   alignItems: 'flex-end'
        // }}>
        //   <Text color="primary" weight="semibold" large>ADD</Text>
        // </TouchableOpacity>
      }
      <ErrorMessage
        style={{
          marginLeft: inputPaddingLeft,
        }}>
        {error}
      </ErrorMessage>
      {children}
      {showButton && (
        <TouchableOpacity onPress={onAddTag} style={styles.addBtnContainer}>
          <Label style={styles.addLabel}>Add</Label>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  addBtnContainer: {
    alignItems: 'center',
    bottom: 0,
    height: INPUT_HEIGHT,
    justifyContent: 'center',
    paddingHorizontal: moderateScale(18, 0.3),
    position: 'absolute',
    right: 0,
  },
  addLabel: {
    color: Colors.yellow,
    fontSize: moderateScale(14, 0.3),
  },
  inputContainer: {
    color: Colors.text,
    fontSize: FONT_SIZE,
    height: INPUT_HEIGHT,
    width: '100%',
  },
  inputView: {
    backgroundColor: Colors.white,
  },
  tagContainer: {
    alignItems: 'center',
    backgroundColor: Colors.background,
    borderRadius: moderateScale(11, 0.3),
    flexDirection: 'row',
    marginBottom: moderateScale(4, 0.3),
    marginRight: moderateScale(8, 0.3),
    paddingHorizontal: moderateScale(15, 0.3),
    paddingVertical: moderateScale(4, 0.3),
  },
  tagsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

FormTagsInput = forwardRef(FormTagsInput);

FormTagsInput.defaultProps = {
  defaultTags: [],
  inputPaddingTop: moderateScale(8, 0.3),
  inputPaddingLeft: 0, //FORM_PADDING,
  autoCapitalize: 'none',
  autoCompleteType: 'off',
  autoCorrect: false,
  returnKeyType: 'done',
  noBorder: false,
  placeholderTextColor: Colors.lightGray,
  onTagsUpdate: () => {},
  onFocus: () => {},
  onBlur: () => {},
  onSubmitEditing: () => {},
};

export default FormTagsInput;
