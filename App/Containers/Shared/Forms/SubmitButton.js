import React, {useContext} from 'react';
import {TouchableOpacity} from 'react-native';
import {Text} from '../../../Components/Text';
import {Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import ContainerContext from '../ContainerContext';
import Loader from '../../../Components/Loader';

const SubmitButton = (props) => {
  const {setHeaderRight} = useContext(ContainerContext);
  const {onPress, isLoading, title, textStyle} = props;

  React.useLayoutEffect(() => {
    setHeaderRight(
      <TouchableOpacity
        disabled={isLoading}
        onPress={onPress}
        style={{
          paddingHorizontal: Metrics.spaceHorizontal,
          height: moderateScale(44, 0.4),
          justifyContent: 'center',
        }}
        {...props}>
        {!isLoading ? (
          <Text
            color="white"
            large
            style={[
              {
                letterSpacing: 1,
              },
              textStyle,
            ]}
            weight="semibold">
            {title}
          </Text>
        ) : (
          <Loader color={Colors.purple} />
        )}
      </TouchableOpacity>,
    );
  }, [isLoading]);

  return null;
};

export default SubmitButton;
