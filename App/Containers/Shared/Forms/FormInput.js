import React, {forwardRef} from 'react';
import {FloatingLabelInput} from '../../../Components/Forms';
import {moderateScale} from 'react-native-size-matters/extend';

const FormInput = (props, ref) => {
  return (
    <FloatingLabelInput
      inputPaddingTop={moderateScale(8, 0.3)}
      ref={ref}
      {...props}
    />
  );
};

export default forwardRef(FormInput);
