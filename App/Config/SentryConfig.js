import DebugConfig from './DebugConfig';
import * as Sentry from '@sentry/react-native';

if (DebugConfig.enableSentry) {
  Sentry.init({
    dsn:
      'https://eccb95dae9124a7d9dfa051f0b7cc8a2@o420639.ingest.sentry.io/5339209',
  });
}

const captureException = (err) => {
  console.tron.log(`Sentry error message: ${err?.message}`);

  if (DebugConfig.enableSentry) {
    Sentry.captureException(err);
  }
};

export default {
  captureException,
};
