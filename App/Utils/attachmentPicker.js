import {Platform, ActionSheetIOS} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import {getFileNameFromURIPath} from './string';
import {downloadFile} from './file';
import RNFS from 'react-native-fs';

const captions = {
  image: 'Image',
  document: 'Document',
  cancel: 'Cancel',
  title: 'Pick type of media',
};

const imagePickerOptions = {
  title: 'Select Photo',
  storageOptions: {
    skipBackup: true, //set as true to prevent auto backup in iCloud
    path: 'images',
  },
};

export const pickIOS = () => {
  return new Promise((resolve, reject) => {
    const {image, document, cancel, title} = captions;
    const options = [image, document, cancel];
    const handlers = [pickImage, pickDocument, pickClosed];
    const cancelButtonIndex = options.indexOf(cancel);

    ActionSheetIOS.showActionSheetWithOptions(
      {options, cancelButtonIndex, title},
      (buttonIndex) => {
        handlers[buttonIndex](resolve, reject);
      },
    );
  });
};

export const pickImage = (resolve, reject) => {
  ImagePicker.launchImageLibrary(imagePickerOptions, (response) => {
    if (response.didCancel) {
      reject(new Error('Action cancelled!'));
    } else {
      const {uri, type, fileName} = response;

      const name = fileName || getFileNameFromURIPath(uri) || 'photo.jpg';

      resolve({uri, type, name});
    }
  });
};

export const pickDocument = async (resolve, reject) => {
  try {
    const result = await DocumentPicker.pick();

    const {uri: tempUri, type, name: fileName} = result;

    console.tron.log('tempUri', tempUri, type);
    let uri = tempUri;

    if (Platform.OS === 'android') {
      console.tron.log('before destPath', uri);
      const destPath = `${RNFS.TemporaryDirectoryPath}/${fileName}`;
      try {
        await RNFS.copyFile(uri, destPath);
        const statResult = await RNFS.stat(destPath);

        uri = `file://${statResult.originalFilepath}`;
      } catch (err) {
        console.tron.log('FILE ERR', err);
      }
    }

    const name = fileName || getFileNameFromURIPath(uri) || 'file.jpg';

    console.tron.log('name', name);

    const realURI = Platform.select({
      android: decodeURI(uri),
      ios: decodeURI(uri),
    });

    resolve({uri: realURI, type, name});
  } catch {
    reject(new Error('Action cancelled!'));
  }
};

export const pickClosed = (_, reject) => {
  reject(new Error('Action cancelled!'));
};

export const pick = () => {
  if (Platform.OS === 'ios') {
    return pickIOS();
  }

  return new Promise((resolve, reject) => {
    return pickDocument(resolve, reject);
  });
};

export default {
  pick,
};
