import RNFS from 'react-native-fs';

export const downloadFile = (url, localFile) => {
  const options = {
    fromUrl: url,
    toFile: localFile,
  };
  return RNFS.downloadFile(options).promise;
};
