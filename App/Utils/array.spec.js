import {
  convertPropertiesToArray,
  getPropertiesWithValues,
  convertArrayToObject,
  sortArrayByObjectKey,
  sortArrayByPriority,
  sortArrayByKeyCondition,
} from './array';

describe('Array Utils', () => {
  const initialValues = {
    type: 'All',
    title: '',
    note: '',
    tags: [],
    show: 'Notes Only',
  };

  describe('convertPropertiesToArray', () => {
    let obj;

    beforeEach(() => {
      obj = initialValues;
    });

    it('happy - convert object property key pair to array items', () => {
      const actualResult = convertPropertiesToArray(obj);
      const expectedResult = [
        {
          key: 'type',
          value: 'All',
        },
        {
          key: 'title',
          value: '',
        },
        {
          key: 'note',
          value: '',
        },
        {
          key: 'tags',
          value: [],
        },
        {
          key: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - return empty array when object is empty', () => {
      obj = {};
      const actualResult = convertPropertiesToArray(obj);
      const expectedResult = [];

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('getPropertiesWithValues', () => {
    let obj;

    beforeEach(() => {
      obj = initialValues;
    });

    it('happy - convert object property key pair to array items with truthy values', () => {
      const actualResult = getPropertiesWithValues(obj);
      const expectedResult = [
        {
          key: 'type',
          id: 'type',
          value: 'All',
        },
        {
          key: 'show',
          id: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - array properties should be flattened in object by appending index in key', () => {
      obj = {
        type: 'All',
        title: '',
        note: '',
        tags: ['Tag_filter', 'Tag_filter_2', 'Tag_filter_3'],
        show: 'Notes Only',
      };
      const actualResult = getPropertiesWithValues(obj, ['All']);
      const expectedResult = [
        {
          key: 'tags',
          id: 'tags0',
          value: 'Tag_filter',
        },
        {
          key: 'tags',
          id: 'tags1',
          value: 'Tag_filter_2',
        },
        {
          key: 'tags',
          id: 'tags2',
          value: 'Tag_filter_3',
        },
        {
          key: 'show',
          id: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - should not include an item with excluded value', () => {
      const actualResult = getPropertiesWithValues(obj, ['All']);
      const expectedResult = [
        {
          key: 'show',
          id: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('convertArrayToObject', () => {
    let arr, obj;

    beforeEach(() => {
      arr = [
        {
          key: 'title',
          value: 'Test Title 2',
          id: 'title',
        },
        {
          key: 'show',
          value: 'Notes Only',
          id: 'show',
        },
        {
          key: 'tags',
          value: 'Test_tag_1',
          id: 'tags0',
        },
        {
          key: 'tags',
          value: 'Test_tag_2',
          id: 'tags1',
        },
      ];

      obj = initialValues;
    });

    it('happy - convert array to object key value pair with array items', () => {
      const actualResult = convertArrayToObject(arr, obj);
      const expectedResult = {
        type: 'All',
        title: 'Test Title 2',
        note: '',
        tags: ['Test_tag_1', 'Test_tag_2'],
        show: 'Notes Only',
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - with empty array items', () => {
      arr = [
        {
          key: 'title',
          value: 'Test Title 2',
          id: 'title',
        },
        {
          key: 'show',
          value: 'Notes Only',
          id: 'show',
        },
      ];

      const actualResult = convertArrayToObject(arr, obj);
      const expectedResult = {
        type: 'All',
        title: 'Test Title 2',
        note: '',
        tags: [],
        show: 'Notes Only',
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('sorting arrays', () => {
    let arr;

    beforeEach(() => {
      arr = [{value: 'b'}, {value: 'c'}, {value: 'a'}];
    });

    it('happy - sort array by key', () => {
      arr = [
        {
          id: '87',
          fileName: '3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
        },
        {
          id: '70',
          fileName: 'flowers.jpeg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/flowers.jpeg',
        },
        {
          id: '61',
          fileName: 'vali_2_manual.pdf',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/vali_2_manual.pdf',
        },
        {
          id: '90',
          fileName: 'FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
        },
      ];

      const actualResult = sortArrayByObjectKey(arr, 'fileName');

      const expectedResult = [
        {
          id: '87',
          fileName: '3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
        },
        {
          id: '90',
          fileName: 'FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
        },
        {
          id: '70',
          fileName: 'flowers.jpeg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/flowers.jpeg',
        },
        {
          id: '61',
          fileName: 'vali_2_manual.pdf',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/vali_2_manual.pdf',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by key with priority', () => {
      const actualResult = sortArrayByPriority(arr, 'value', ['c']);
      const expectedResult = [{value: 'c'}, {value: 'b'}, {value: 'a'}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array with multiple items with priority', () => {
      arr = arr.concat({value: 'c'});
      const actualResult = sortArrayByPriority(arr, 'value', ['c']);
      const expectedResult = [
        {value: 'c'},
        {value: 'c'},
        {value: 'b'},
        {value: 'a'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort arrayByKeyCondition', () => {
      arr = arr.concat({value: 'c'});
      const actualResult1 = sortArrayByKeyCondition(
        arr,
        'value',
        (value) => value === 'c',
      );
      const actualResult2 = sortArrayByKeyCondition(
        arr,
        'value',
        (value) => value === 'a',
      );

      const expectedResult1 = [
        {value: 'c'},
        {value: 'c'},
        {value: 'b'},
        {value: 'a'},
      ];
      const expectedResult2 = [
        {value: 'a'},
        {value: 'b'},
        {value: 'c'},
        {value: 'c'},
      ];

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });
  });
});
