import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import SendSMS from 'react-native-sms';
import {openComposer} from 'react-native-email-link';

export const call = (phoneNumber) => {
  RNImmediatePhoneCall.immediatePhoneCall(phoneNumber);
};

export const sendSMS = (
  recipients = [],
  successCallback = () => {},
  errCallback = () => {},
) => {
  console.tron.log('recipients', recipients);
  SendSMS.send(
    {
      body: '',
      recipients,
      successTypes: ['sent', 'queued'],
      allowAndroidSendWithoutReadPermission: true,
    },
    (completed, cancelled, error) => {
      console.tron.log(
        'SMS Callback: completed: ' +
          completed +
          ' cancelled: ' +
          cancelled +
          ' error: ' +
          error,
      );

      if (completed) {
        successCallback();
      } else if (error) {
        errCallback();
      }
    },
  );
};

export const sendEmail = (email) => {
  openComposer({
    to: email,
    subject: '',
    body: '',
  });
};
