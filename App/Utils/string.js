export const getFileNameFromURIPath = (str) => {
  return str.replace(/^.*[\\\/]/, '');
};
