import {Alert} from 'react-native';
import NetInfo from '@react-native-community/netinfo';

export const hasConnection = async () => {
  const state = await NetInfo.fetch();
  console.tron.log('Is connected?', state.isConnected);
  return state.isConnected;
};

export const showNoConnectionAlert = (() => {
  let isShowing = false;

  return () => {
    if (!isShowing) {
      isShowing = true;

      Alert.alert(
        'No Connection',
        'This action needs network connection in order to proceed',
        [{text: 'OK', onPress: () => (isShowing = false)}],
      );
    }
  };
})();
