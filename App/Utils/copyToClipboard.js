import {Alert} from 'react-native';
import Clipboard from '@react-native-community/clipboard';

export default function (text) {
  if (text) {
    Alert.alert('Copied to clipboard:\n' + text);
    Clipboard.setString(text);
  }
}
