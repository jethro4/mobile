import {setItem, getItem} from '../Utils/storage';
import {mergeValues} from '../Utils/transformers';

export default (value, storageKey) => {
  let stateSetters = {};
  let cache = value;
  const loadingPromise = new Promise((resolve) => {
    getItem(storageKey).then((storedValue) => {
      if (!storedValue) {
        setItem(storageKey, value);
      } else {
        cache = mergeValues(cache, storedValue);
      }

      console.tron.log(
        'makeVar get initialValue storageKey: ' + storageKey + ' cache: ',
        cache,
      );

      resolve();
    });
  });

  const triggerSetters = (newValue) => {
    Object.values(stateSetters).forEach((setState) => setState(newValue));
  };

  const subscribe = (setStateFn, key) => {
    stateSetters[key] = setStateFn;
  };

  return (setStateOrValue, key, options = {}) => {
    if (options.directCache) {
      return cache;
    }

    return loadingPromise.then(() => {
      if (typeof setStateOrValue === 'function') {
        const setStateFn = setStateOrValue;
        subscribe(setStateFn, key);
        triggerSetters(cache);
      } else {
        const newValue = setStateOrValue;

        if (newValue !== undefined) {
          if (options.merge) {
            cache = mergeValues(cache, newValue);
          } else {
            cache = newValue;
          }

          triggerSetters(newValue);
          setItem(storageKey, newValue);
        } else {
          triggerSetters(cache);
        }
      }

      return cache;
    });
  };
};
