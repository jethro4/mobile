import {makeVar as apolloMakeVar, InMemoryCache} from '@apollo/client';
import {setItem, getItem} from '../Utils/storage';

const makeVar = (key, initialValue) => {
  let value = initialValue;

  return (newValue) => {
    if (newValue !== undefined && newValue !== value) {
      console.tron.log('setItem in storage: ' + key, value, newValue);
      value = newValue;
      setItem(key, newValue);
      apolloMakeVar(newValue);
    }

    return value;
  };
};

export const setupDetailsVar = makeVar('setupDetails', {
  appName: '',
  apiKey: '',
});
export const userDetailsVar = makeVar('userDetails', {
  email: '',
  password: '',
  firstName: '',
  lastName: '',
});
export const sessionVar = makeVar('session', {sessionId: ''});
export const noteFiltersVar = makeVar('noteFilters', {
  filterBy: 'terms',
  terms: '',
  tags: [],
});
export const taskFiltersVar = makeVar('taskFilters', {
  filterBy: 'terms',
  terms: '',
  tags: [],
});

export {makeVar};

const rehydrateVarFromStorage = (key, itemVar) => {
  return getItem(key).then((value) => {
    console.tron.log(`rehydrateVarFromStorage 'key-value': ${key}`, value);
    itemVar(value || {}); //default empty object value if value=null
  });
};

export const rehydrateVars = async () => {
  await rehydrateVarFromStorage('setupDetails', setupDetailsVar);
  await rehydrateVarFromStorage('userDetails', userDetailsVar);
  await rehydrateVarFromStorage('noteFilters', noteFiltersVar);
  await rehydrateVarFromStorage('taskFilters', taskFiltersVar);
  await rehydrateVarFromStorage('session', sessionVar);
};

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        setupDetails: {
          read() {
            return setupDetailsVar();
          },
        },
        userDetails: {
          read() {
            return userDetailsVar();
          },
        },
        session: {
          read() {
            return sessionVar();
          },
        },
        noteFilters: {
          read() {
            return noteFiltersVar();
          },
        },
        taskFilters: {
          read() {
            return taskFiltersVar();
          },
        },
      },
    },
  },
});

export default cache;
