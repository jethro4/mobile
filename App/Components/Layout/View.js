import React from 'react';
import {View as NativeView} from 'react-native';

const View = (props) => {
  const {style, ...allProps} = props;
  return (
    <NativeView
      style={[
        props.absoluteFill && {
          position: absolute,
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
        },
        props.isFlex && {flex: 1},
        props.row && {flexDirection: 'row'},
        props.justifyEnd && {justifyContent: 'flex-end'},
        props.justifyCenter && {justifyContent: 'center'},
        props.justifyBetween && {justifyContent: 'space-between'},
        props.justifyAround && {justifyContent: 'space-around'},
        props.alignCenter && {alignItems: 'center'},
        props.alignEnd && {alignItems: 'flex-end'},
        props.selfCenter && {alignSelf: 'center'},
        style,
      ]}
      {...allProps}
    />
  );
};

export default View;
