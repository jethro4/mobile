import React, {useState} from 'react';
import {TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import {View} from '../../Components/Layout';
import {Colors, ApplicationStyles, Metrics} from '../../Themes';
import {ErrorMessage} from '../../Components/Text';
import SvgIcon from '../SvgIcon';
import {moderateScale} from 'react-native-size-matters/extend';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);
const CLEAR_BTN_WIDTH = moderateScale(32, 0.3);

const FormInput = React.forwardRef((props, ref) => {
  const {
    children,
    containerStyle,
    style,
    defaultValue,
    onChangeText,
    clearStyle,
    noClear,
    noMargin,
    noBorder,
    error,
    icon,
    iconSize,
    ...allProps
  } = props;

  const [value, setValue] = useState(defaultValue);

  const onChangeTextFn = (text) => {
    setValue(text);
    onChangeText(text);
  };

  const onClear = () => {
    setValue('');
    onChangeText('');
  };

  return (
    <>
      <View
        style={[
          styles.inputView,
          !props.noMargin && {marginBottom: moderateScale(20, 0.3)},
          containerStyle,
        ]}>
        <View>
          <TextInput
            onChangeText={onChangeTextFn}
            ref={ref}
            style={[
              styles.inputContainer,
              {
                paddingLeft: icon ? iconSize + FORM_PADDING * 2 : FORM_PADDING,
                paddingRight: CLEAR_BTN_WIDTH,
              },
              !noBorder && {
                borderBottomWidth: 1,
                borderBottomColor: Colors.border,
              },
              style,
            ]}
            value={value}
            {...allProps}
          />

          {!noClear && !!value && (
            <TouchableOpacity
              onPress={onClear}
              style={[
                {
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  right: 0,
                  width: CLEAR_BTN_WIDTH,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
                clearStyle,
              ]}>
              <View
                style={{
                  width: moderateScale(16, 0.3),
                  height: moderateScale(16, 0.3),
                  borderRadius: moderateScale(16, 0.3),
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.lightGray,
                }}>
                <SvgIcon
                  fill={Colors.white}
                  height={moderateScale(8, 0.3)}
                  name="Close"
                  width={moderateScale(8, 0.3)}
                />
              </View>
            </TouchableOpacity>
          )}
        </View>
        <ErrorMessage>{error}</ErrorMessage>
        {children}
        {!!icon && (
          <View
            pointerEvents="none"
            style={{
              position: 'absolute',
              top: 0,
              height: INPUT_HEIGHT,
              left: FORM_PADDING,
              justifyContent: 'center',
            }}>
            <SvgIcon
              fill={Colors.secondary}
              height={iconSize}
              name={icon}
              width={iconSize}
            />
          </View>
        )}
      </View>
    </>
  );
});

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: Colors.white,
    color: Colors.text,
    fontSize: moderateScale(15, 0.3),
    height: INPUT_HEIGHT,
    width: '100%',
  },
  inputView: {
    justifyContent: 'center',
  },
});

FormInput.defaultProps = {
  defaultValue: '',
  containerStyle: {},
  placeholderTextColor: Colors.lightGray,
  autoCapitalize: 'none',
  autoCompleteType: 'off',
  autoCorrect: false,
  keyboardType: Metrics.isIos ? 'ascii-capable' : 'default',
  returnKeyType: 'done',
  noBorder: true,
  noMargin: false,
  iconSize: moderateScale(18),
  clearStyle: {},
  onChangeText: () => {},
};

export default FormInput;
