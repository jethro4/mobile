import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {View} from '../Layout';
import {Text} from '../Text';
import {ApplicationStyles, Colors} from '../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import SvgIcon from '../SvgIcon';

const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);

const Style = StyleSheet.create({
  formHorizontal: {
    flexDirection: 'row',
  },

  labelVertical: {
    paddingLeft: 0,
  },
  labelVerticalWrap: {
    flexDirection: 'column',
    paddingLeft: moderateScale(10, 0.3),
  },

  labelWrapStyle: {
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
  },

  checkbox: {
    alignItems: 'center',
    alignSelf: 'center',

    borderColor: Colors.lightGray,
    borderRadius: 30,

    height: 30,

    justifyContent: 'center',
    width: 30,
  },

  checkboxActive: {
    backgroundColor: Colors.lightGray,
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },

  checkboxForm: {},

  checkboxLabel: {
    lineHeight: moderateScale(14, 0.3),
    paddingLeft: moderateScale(10, 0.3),
  },

  checkboxNormal: {
    borderRadius: moderateScale(2, 0.3),
  },

  checkboxWrap: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: moderateScale(5, 0.3),
  },
});

export class Checkbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  static defaultProps = {
    isSelected: false,
    buttonColor: Colors.lightGray,
    selectedButtonColor: Colors.lightGray,
    labelHorizontal: true,
    disabled: false,
    idSeparator: '|',
  };
  render() {
    var c = this.props.children;

    var idSeparator = this.props.idSeparator ? this.props.idSeparator : '|';
    var idSeparatorAccessibilityLabelIndex = this.props.accessibilityLabel
      ? this.props.accessibilityLabel.indexOf(idSeparator)
      : -1;
    var idSeparatorTestIdIndex = this.props.testID
      ? this.props.testID.indexOf(idSeparator)
      : -1;

    var accessibilityLabel = this.props.accessibilityLabel
      ? idSeparatorAccessibilityLabelIndex !== -1
        ? this.props.accessibilityLabel.substring(
            0,
            idSeparatorAccessibilityLabelIndex,
          )
        : this.props.accessibilityLabel
      : 'checkboxButton';
    var testID = this.props.testID
      ? idSeparatorTestIdIndex !== -1
        ? this.props.testID.substring(0, idSeparatorTestIdIndex)
        : this.props.testID
      : 'checkboxButton';

    var accessibilityLabelIndex =
      this.props.accessibilityLabel && idSeparatorAccessibilityLabelIndex !== -1
        ? this.props.accessibilityLabel.substring(
            idSeparatorAccessibilityLabelIndex + 1,
          )
        : '';
    var testIDIndex =
      this.props.testID && testIDIndex !== -1
        ? this.props.testID.split(testIDIndex + 1)
        : '';

    var renderContent = false;
    renderContent = c ? (
      <View
        style={[
          Style.checkboxWrap,
          this.props.style,
          !this.props.labelHorizontal && Style.labelVerticalWrap,
        ]}>
        {c}
      </View>
    ) : (
      <TouchableOpacity
        accessibilityLabel={
          accessibilityLabel + 'Input' + accessibilityLabelIndex
        }
        accessible={this.props.accessible}
        activeOpacity={0.4}
        onPress={() => {
          this.props.onPress(this.props.obj.value, this.props.index);
        }}
        style={[
          Style.checkboxWrap,
          this.props.style,
          !this.props.labelHorizontal && Style.labelVerticalWrap,
        ]}
        testID={testID + 'Input' + testIDIndex}>
        <CheckboxInput {...this.props} />
        <Text
          style={[
            Style.checkboxLabel,
            !this.props.labelHorizontal && Style.labelVertical,
            {color: this.props.labelColor},
            this.props.labelStyle,
          ]}>
          {this.props.obj.label}
        </Text>
      </TouchableOpacity>
    );
    return <View style={this.props.wrapStyle}>{renderContent}</View>;
  }
}

export class CheckboxInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelected: false,
      buttonColor: props.buttonColor || Colors.lightGray,
    };
  }
  render() {
    var innerSize = {
      width: moderateScale(14, 0.3),
      height: moderateScale(14, 0.3),
      borderRadius: moderateScale(14, 0.3) / 2,
    };
    var outerSize = {
      width: moderateScale(14, 0.3) + moderateScale(10, 0.3),
      height: moderateScale(14, 0.3) + moderateScale(10, 0.3),
      borderRadius: (moderateScale(14, 0.3) + moderateScale(10, 0.3)) / 2,
    };
    if (this.props.buttonSize) {
      innerSize.width = this.props.buttonSize;
      innerSize.height = this.props.buttonSize;
      innerSize.borderRadius = this.props.buttonSize / 2;
      outerSize.width = this.props.buttonSize + moderateScale(10, 0.3);
      outerSize.height = this.props.buttonSize + moderateScale(10, 0.3);
      outerSize.borderRadius =
        (this.props.buttonSize + moderateScale(10, 0.3)) / 2;
    }
    if (this.props.buttonOuterSize) {
      outerSize.width = this.props.buttonOuterSize;
      outerSize.height = this.props.buttonOuterSize;
      outerSize.borderRadius = this.props.buttonOuterSize / 2;
    }
    var outerColor = this.props.buttonOuterColor || this.props.buttonColor;
    var borderWidth = this.props.borderWidth || 2;
    var innerColor = this.props.buttonInnerColor || this.props.buttonColor;

    var c = this.props.isSelected && (
      <View style={[Style.checkboxActive, {backgroundColor: innerColor}]}>
        <SvgIcon
          fill={Colors.white}
          height={moderateScale(14, 0.3)}
          name="Check2"
          width={moderateScale(14, 0.3)}
        />
      </View>
    );
    // var checkboxStyle = [
    //   Style.checkbox,
    //   {
    //     borderColor: outerColor,
    //     borderWidth: borderWidth,
    //   },
    //   this.props.buttonStyle,
    //   outerSize,
    // ];

    // if (this.props.disabled) {
    //   return (
    //     <View style={this.props.buttonWrapStyle}>
    //       <View style={checkboxStyle}>{c}</View>
    //     </View>
    //   );
    // }

    return (
      <View
        style={[
          Style.checkboxNormal,
          {
            width: moderateScale(22, 0.3),
            height: moderateScale(22, 0.3),
            borderWidth: 2,
            borderColor: Colors.border,
            justifyContent: 'center',
            alignItems: 'center',
          },
        ]}>
        {c}
      </View>
    );
  }
}

CheckboxInput.defaultProps = {
  buttonInnerColor: Colors.formBlue,
  buttonOuterColor: Colors.lightGray,
  disabled: false,
};

export default class CheckboxForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndexes:
        props.initial.map((initialValue) =>
          props.values.findIndex((v) => v.value === initialValue),
        ) || [],
    };
    this._renderButton = this._renderButton.bind(this);
  }
  static defaultProps = {
    values: [],
    buttonColor: Colors.lightGray,
    selectedButtonColor: Colors.lightGray,
    formHorizontal: false,
    labelHorizontal: true,
    animation: false,
    labelColor: Colors.lightGray,
    selectedLabelColor: Colors.text,
    wrapStyle: {},
    disabled: false,
    placeholderTextColor: Colors.lightGray,
    noBorder: false,
  };

  //This function is for clear the selection when we are using the library in multiple choice questions
  clearSelection() {
    this.setState({activeIndexes: []});
  }

  _renderButton(obj, i) {
    return (
      <Checkbox
        accessibilityLabel={
          this.props.accessibilityLabel
            ? this.props.accessibilityLabel + '|' + i
            : 'checkboxButton' + '|' + i
        }
        accessible={this.props.accessible}
        animation={this.props.animation}
        buttonColor={
          this.state.activeIndexes.includes(i)
            ? this.props.selectedButtonColor
            : this.props.buttonColor
        }
        buttonOuterSize={this.props.buttonOuterSize}
        buttonSize={this.props.buttonSize}
        disabled={this.props.disabled}
        index={i}
        isSelected={this.state.activeIndexes.includes(i)}
        key={i}
        labelColor={
          this.state.activeIndexes.includes(i)
            ? this.props.selectedLabelColor
            : this.props.labelColor
        }
        labelHorizontal={this.props.labelHorizontal}
        labelStyle={this.props.labelStyle}
        obj={obj}
        onPress={(value, index) => {
          const updatedActiveIndexes = !this.state.activeIndexes.includes(index)
            ? this.state.activeIndexes.concat(index)
            : this.state.activeIndexes.filter((v) => v !== index);

          this.setState({
            activeIndexes: updatedActiveIndexes,
          });

          this.props.onPress(
            updatedActiveIndexes.map((v) => this.props.values[v]),
          );
        }}
        style={this.props.checkboxStyle}
        testID={
          this.props.testID
            ? this.props.testID + '|' + i
            : 'checkboxButton' + '|' + i
        }
      />
    );
  }

  render() {
    const {noBorder} = this.props;
    var render_content = false;
    if (this.props.values.length) {
      render_content = this.props.values.map(this._renderButton);
    } else {
      render_content = this.props.children;
    }
    return (
      <View
        style={[
          this.props.label && {
            paddingTop: moderateScale(16, 0.3),
          },
          !noBorder && {borderBottomWidth: 1, borderBottomColor: Colors.border},
        ]}>
        {!!this.props.label && (
          <Text
            pointerEvents="none"
            style={{
              fontSize: FONT_SIZE,
              color: this.props.placeholderTextColor,
            }}>
            {this.props.label}
          </Text>
        )}

        <View
          style={[
            {
              marginVertical: moderateScale(16, 0.3),
            },
            Style.checkboxFrom,
            this.props.style,
            this.props.formHorizontal && Style.formHorizontal,
          ]}>
          {render_content}
        </View>
      </View>
    );
  }
}
