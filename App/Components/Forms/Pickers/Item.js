import React from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import {Text} from '../../../Components/Text';
import {ApplicationStyles, Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const {height} = ApplicationStyles.formRow;
export const PICKER_ITEM_HEIGHT = moderateScale(44, 0.3);

const Item = (props) => {
  const {item, onPressItem, isSelected, style, ...allProps} = props;
  const onPress = () => {
    onPressItem(item);
  };

  return (
    <TouchableHighlight
      isSelected={isSelected}
      onPress={onPress}
      style={[
        styles.itemContainer,
        isSelected && {backgroundColor: Colors.silver},
        style,
      ]}
      {...allProps}>
      <Text large style={[styles.label, isSelected && {color: Colors.primary}]}>
        {item}
      </Text>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    height: PICKER_ITEM_HEIGHT,
    justifyContent: 'center',
    paddingLeft: Metrics.spaceHorizontal,
    width: '100%',
  },
  label: {
    fontSize: moderateScale(14, 0.3),
  },
});

Item.defaultProps = {
  underlayColor: Colors.silver,
};

export default Item;
