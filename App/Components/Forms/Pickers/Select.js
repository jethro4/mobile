import React, {
  forwardRef,
  useState,
  useCallback,
  useRef,
  useEffect,
  useImperativeHandle,
} from 'react';
import {ScrollView, Platform, StyleSheet, TouchableOpacity} from 'react-native';
import {View} from '../../Layout';
import {Colors, ApplicationStyles} from '../../../Themes';
import {Label} from '../../Text';
import {moderateScale} from 'react-native-size-matters/extend';
import SvgIcon from '../../../Components/SvgIcon';
import Item from './Item';

const {height} = ApplicationStyles.formRow;
export const PICKER_ITEM_HEIGHT = moderateScale(height, 0.3);

let Select = (props, ref) => {
  const dropdownRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const {
    listTop,
    zIndex,
    children,
    onPressPicker,
    defaultValue,
    items,
    onSelectPickerItem,
    containerStyle,
    placeholder,
    placeholderTextColor,
    labelPrefix,
    labelStyle,
    iconFill,
    noBorder,
    style,
    ...allProps
  } = props;
  const [selectedItem, setSelectedItem] = useState(defaultValue);
  const onPressPickerFn = useCallback(() => {
    setIsVisible(!isVisible);

    onPressPicker();
  }, [isVisible, onPressPicker]);

  const onSelectPickerItemFn = useCallback(
    (item) => {
      setIsVisible(false);

      if (item !== selectedItem) {
        setSelectedItem(item);
        onSelectPickerItem(item);
      }
    },
    [selectedItem, onSelectPickerItem],
  );
  useEffect(() => {
    if (dropdownRef.current) {
      const selectedItemIndex = items.indexOf(selectedItem);
      dropdownRef.current.scrollTo({
        y: selectedItemIndex * PICKER_ITEM_HEIGHT,
        animated: false,
      });
    }
  }, [isVisible]);

  useImperativeHandle(
    ref,
    () => {
      return {
        open: () => {
          setIsVisible(true);
        },
        close: () => {
          setIsVisible(false);
        },
      };
    },
    [],
  );

  return (
    <View
      style={[
        {
          ...Platform.select({
            ios: {
              zIndex, // zIndex for iOS only
            },
          }),
        },
        containerStyle,
      ]}>
      <TouchableOpacity
        onPress={onPressPickerFn}
        style={[
          styles.selectContainer,
          !noBorder && {
            borderBottomWidth: 1,
            borderBottomColor: Colors.border,
          },
          style,
        ]}
        {...allProps}>
        <>
          {children}
          <Label
            style={[
              {
                marginRight: moderateScale(4, 0.3),
                color: Colors.primary,
              },
              labelStyle,
              !selectedItem && {
                color: placeholderTextColor,
              },
            ]}>
            {selectedItem
              ? `${labelPrefix}${selectedItem}`
              : 'Please select one'}
          </Label>
          <SvgIcon
            fill={iconFill || Colors.primary}
            height={moderateScale(12, 0.3)}
            name="DownChevron"
            width={moderateScale(12, 0.3)}
          />
        </>
      </TouchableOpacity>

      <View
        style={[
          {
            backgroundColor: 'white',
            position: 'absolute',
            borderRadius: 4,
            top: listTop,
            left: 0,
            width: '100%',
            maxHeight: PICKER_ITEM_HEIGHT * 5 + PICKER_ITEM_HEIGHT * 0.5,
            zIndex, // zIndex for Android only

            elevation: 1,
            shadowColor: Colors.black,
            shadowRadius: 6,
            shadowOpacity: 0.2,
            shadowOffset: {
              width: 0,
              height: 5,
            },
          },
          !isVisible && {
            position: 'relative',
            display: 'none',
          },
        ]}>
        <ScrollView keyboardShouldPersistTaps="always" ref={dropdownRef}>
          {items.map((item) => {
            const isSelected = item === selectedItem;
            return (
              <Item
                isSelected={isSelected}
                item={item}
                // disabled={isSelected}
                key={item}
                onPressItem={onSelectPickerItemFn}
              />
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  selectContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: '100%',
    justifyContent: 'center',
  },
});

Select = forwardRef(Select);

Select.defaultProps = {
  items: [],
  onPressPicker: () => {},
  onSelectPickerItem: () => {},
  zIndex: 1000,
  selectedItem: '',
  defaultValue: '',
  labelPrefix: '',
  listTop: '100%',
  noBorder: false,
  placeholderTextColor: Colors.lightGray,
};

export default Select;
