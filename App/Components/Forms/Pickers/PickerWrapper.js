import React, {forwardRef, useCallback, useRef, useContext} from 'react';
import {ApplicationStyles} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import PickerContext from './PickerContext';

const {height} = ApplicationStyles.formRow;
export const PICKER_ITEM_HEIGHT = moderateScale(height, 0.3);

let PickerWrapper = (props, ref) => {
  const {children} = props;
  const selectRef = useRef();

  const {closeAll, setCurrentRef} = useContext(PickerContext);

  const onPressPicker = useCallback(() => {
    closeAll();
    setCurrentRef(selectRef.current);
  }, [closeAll, setCurrentRef]);

  return <>{React.cloneElement(children, {onPressPicker, ref: selectRef})}</>;
};

PickerWrapper = forwardRef(PickerWrapper);

export default PickerWrapper;
