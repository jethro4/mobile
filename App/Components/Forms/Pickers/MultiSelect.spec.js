import {getSelectedItems} from './MultiSelect';

describe('DO Relationships Data Transformer', () => {
  let selectedItems;
  let allItems;

  const initialSelectedItems = ['Macanta User', 'Passenger', 'Organiser'];

  const initialAllItems = [
    'Macanta User',
    'App Owner',
    'Developer Agent',
    'Passenger',
    'Organiser',
    'Macanta Users',
    'Macanta Userasdqwe',
  ];

  beforeEach(() => {
    selectedItems = initialSelectedItems;
    allItems = initialAllItems;
  });

  it('getSelectedItems - Add relationship', () => {
    selectedItems = ['Passenger', 'Organiser'];

    const actualResult = getSelectedItems(
      'Macanta User',
      selectedItems,
      allItems,
    );
    const expectedResult = ['Macanta User', 'Passenger', 'Organiser'];

    expect(actualResult).toEqual(expectedResult);
  });

  it('getSelectedItems - Remove relationship', () => {
    const actualResult = getSelectedItems(
      'Macanta User',
      selectedItems,
      allItems,
    );
    const expectedResult = ['Passenger', 'Organiser'];

    expect(actualResult).toEqual(expectedResult);
  });

  it('getSelectedItems - Remove then add relationship', () => {
    selectedItems = getSelectedItems('Macanta User', selectedItems, allItems);

    const actualResult = getSelectedItems(
      'Macanta User',
      selectedItems,
      allItems,
    );
    const expectedResult = ['Macanta User', 'Passenger', 'Organiser'];

    expect(actualResult).toEqual(expectedResult);
  });
});
