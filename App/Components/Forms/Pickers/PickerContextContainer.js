import React, {
  forwardRef,
  useState,
  useCallback,
  useImperativeHandle,
  useRef,
} from 'react';
import {ApplicationStyles} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import PickerContext from './PickerContext';

const {height} = ApplicationStyles.formRow;
export const PICKER_ITEM_HEIGHT = moderateScale(height, 0.3);

let PickerContextContainer = (props, ref) => {
  const {children} = props;
  const currentRef = useRef(null);

  const closeAll = useCallback(() => {
    if (currentRef.current) {
      currentRef.current.close();
    }
  }, [currentRef]);

  const setCurrentRef = useCallback((cRef) => {
    if (currentRef.current !== cRef) {
      currentRef.current = cRef;
    } else {
      currentRef.current = null;
    }
  }, []);

  useImperativeHandle(
    ref,
    () => {
      return {
        closeAll,
      };
    },
    [closeAll],
  );

  return (
    <PickerContext.Provider
      value={{
        setCurrentRef,
        closeAll,
      }}>
      {children}
    </PickerContext.Provider>
  );
};

PickerContextContainer = forwardRef(PickerContextContainer);

export default PickerContextContainer;
