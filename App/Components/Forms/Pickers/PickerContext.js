import React from 'react';

const PickerContext = React.createContext({
  setCurrentRef: () => null,
  closeAll: () => null,
});

export default PickerContext;
