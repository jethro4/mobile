import React, {
  forwardRef,
  useState,
  useCallback,
  useRef,
  useEffect,
  useImperativeHandle,
} from 'react';
import {ScrollView, Platform, StyleSheet, TouchableOpacity} from 'react-native';
import {View} from '../../Layout';
import {Colors, ApplicationStyles} from '../../../Themes';
import {Label} from '../../Text';
import {moderateScale} from 'react-native-size-matters/extend';
import SvgIcon from '../../../Components/SvgIcon';
import Item from './Item';

const {height} = ApplicationStyles.formRow;
export const PICKER_ITEM_HEIGHT = moderateScale(height, 0.3);

export const getSelectedItems = (item, selectedItems, allItems) => {
  let updatedItems;

  if (!selectedItems.includes(item)) {
    updatedItems = allItems.filter((it) =>
      selectedItems.concat(item).includes(it),
    );
  } else {
    updatedItems = selectedItems.filter((it) => item !== it);
  }

  return updatedItems;
};

let MultiSelect = (props, ref) => {
  const dropdownRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const {
    listTop,
    zIndex,
    children,
    onPressPicker,
    defaultValue,
    items,
    allItems,
    onSelectPickerItems,
    containerStyle,
    placeholder,
    placeholderTextColor,
    labelPrefix,
    labelStyle,
    iconFill,
    noBorder,
    style,
    ...allProps
  } = props;
  const [selectedItems, setSelectedItems] = useState(defaultValue);

  const onPressPickerFn = useCallback(() => {
    setIsVisible(!isVisible);

    onPressPicker(selectedItems);
  }, [isVisible]);

  const onSelectPickerItemsFn = (item) => {
    const updatedItems = getSelectedItems(item, selectedItems, allItems);

    setSelectedItems(updatedItems);
    onSelectPickerItems(updatedItems, item);
  };

  useEffect(() => {
    if (dropdownRef.current) {
      dropdownRef.current.scrollTo({
        y: 0,
        animated: false,
      });
    }
  }, [isVisible]);

  useImperativeHandle(
    ref,
    () => {
      return {
        open: () => {
          setIsVisible(true);
        },
        close: () => {
          setIsVisible(false);
        },
      };
    },
    [],
  );

  return (
    <View
      style={[
        {
          ...Platform.select({
            ios: {
              zIndex, // zIndex for iOS only
            },
          }),
        },
        containerStyle,
      ]}>
      <TouchableOpacity
        onPress={onPressPickerFn}
        style={[
          styles.selectContainer,
          !noBorder && {
            borderBottomWidth: 1,
            borderBottomColor: Colors.border,
          },
          style,
        ]}
        {...allProps}>
        <>
          {children}
          <Label
            style={[
              {
                marginRight: moderateScale(4, 0.3),
                color: Colors.primary,
              },
              labelStyle,
            ]}>
            {labelPrefix}
            {selectedItems.length > 0
              ? selectedItems.join(', ')
              : 'None selected'}
          </Label>
          <SvgIcon
            fill={iconFill}
            height={moderateScale(12, 0.3)}
            name="DownChevron"
            width={moderateScale(12, 0.3)}
          />
        </>
      </TouchableOpacity>

      <View
        style={[
          {
            backgroundColor: 'white',
            position: 'absolute',
            borderRadius: 4,
            top: listTop,
            left: 0,
            width: '100%',
            maxHeight: PICKER_ITEM_HEIGHT * 5 + PICKER_ITEM_HEIGHT * 0.5,
            zIndex, // zIndex for Android only

            elevation: 1,
            shadowColor: Colors.black,
            shadowRadius: 6,
            shadowOpacity: 0.2,
            shadowOffset: {
              width: 0,
              height: 5,
            },
          },
          !isVisible && {
            position: 'relative',
            display: 'none',
          },
        ]}>
        <ScrollView keyboardShouldPersistTaps="always" ref={dropdownRef}>
          {items.map((item) => {
            const isSelected = selectedItems.includes(item);
            return (
              <Item
                isSelected={isSelected}
                item={item}
                // disabled={isSelected}
                key={item}
                onPressItem={onSelectPickerItemsFn}
              />
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  selectContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: '100%',
    justifyContent: 'center',
  },
});

MultiSelect = forwardRef(MultiSelect);

MultiSelect.defaultProps = {
  items: [],
  onPressPicker: () => {},
  onSelectPickerItems: () => {},
  zIndex: 1000,
  defaultValue: [],
  labelPrefix: '',
  listTop: '100%',
  noBorder: false,
  iconFill: Colors.primary,
};

export default MultiSelect;
