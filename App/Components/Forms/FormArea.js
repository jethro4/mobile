import React, {useState} from 'react';
import {TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import {View} from '../../Components/Layout';
import {Colors, ApplicationStyles, Metrics} from '../../Themes';
import {ErrorMessage, Text} from '../../Components/Text';
import SvgIcon from '../SvgIcon';
import {moderateScale} from 'react-native-size-matters/extend';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height * 2, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);
const CLEAR_BTN_WIDTH = moderateScale(32, 0.3);
const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);

const FormArea = React.forwardRef((props, ref) => {
  const {
    label,
    placeholderTextColor,
    children,
    containerStyle,
    style,
    defaultValue,
    onChangeText,
    noBorder,
    error,
    icon,
    iconSize,
    ...allProps
  } = props;

  const [value, setValue] = useState(defaultValue);

  const onChangeTextFn = (text) => {
    setValue(text);
    onChangeText(text);
  };

  return (
    <>
      <View
        style={[
          styles.inputView,
          !props.noMargin && {marginBottom: moderateScale(20, 0.3)},
          containerStyle,
        ]}>
        <View>
          <TextInput
            multiline
            onChangeText={onChangeTextFn}
            ref={ref}
            style={[
              styles.inputContainer,
              {
                paddingTop: moderateScale(12, 0.3),
              },
              !noBorder && {
                borderBottomWidth: 1,
                borderBottomColor: Colors.border,
              },
              style,
            ]}
            value={value}
            {...allProps}
          />

          {!value && (
            <Text
              pointerEvents="none"
              style={{
                position: 'absolute',
                left: 0,
                top: moderateScale(12, 0.3),
                fontSize: FONT_SIZE,
                color: placeholderTextColor,
              }}>
              {label}
            </Text>
          )}
        </View>
        <ErrorMessage>{error}</ErrorMessage>
        {children}
      </View>
    </>
  );
});

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: Colors.white,
    color: Colors.text,
    fontSize: moderateScale(15, 0.3),
    height: INPUT_HEIGHT,
    width: '100%',
  },
  inputView: {
    justifyContent: 'center',
  },
});

FormArea.defaultProps = {
  defaultValue: '',
  containerStyle: {},
  placeholderTextColor: Colors.lightGray,
  autoCapitalize: 'none',
  autoCompleteType: 'off',
  autoCorrect: false,
  keyboardType: Metrics.isIos ? 'ascii-capable' : 'default',
  returnKeyType: 'done',
  noBorder: true,
  noMargin: false,
  iconSize: moderateScale(18),
  clearStyle: {},
  onChangeText: () => {},
};

export default FormArea;
