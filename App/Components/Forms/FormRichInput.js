import React, {
  forwardRef,
  useRef,
  useState,
  useEffect,
  useImperativeHandle,
} from 'react';
import {ScrollView} from 'react-native';
import {actions, RichEditor, RichToolbar} from 'react-native-pell-rich-editor';
import {View} from '../../Components/Layout';
import {Text} from '../../Components/Text';
import {ApplicationStyles, Colors, Metrics} from '../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import Loader from '../../Components/Loader';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height * 2, 0.3);
const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);

let FormRichInput = (props, ref) => {
  const {
    label,
    placeholderTextColor,
    inputPaddingTop,
    inputPaddingLeft,
    containerStyle,
    onChange,
    defaultContentHTML,
    ...allProps
  } = props;

  const [isInitialized, setIsInitialized] = useState(false);
  const [editorHgt, setEditorHgt] = useState(INPUT_HEIGHT);
  const [isFocused, setIsFocused] = useState(false);
  const [content, setContent] = useState(defaultContentHTML);
  const editorRef = useRef();

  const containerPaddingTop = inputPaddingTop;

  const onEditorInitialized = () => {
    setIsInitialized(true);
  };

  const handleHeightChange = (height) => {
    setEditorHgt(height);
  };

  const onChangeFn = (content) => {
    const isEmpty = content === '<br>' || content === '<div><br></div>';
    const newContent = !isEmpty ? content : '';

    setContent(newContent);
    onChange(newContent);
  };

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  useEffect(() => {
    if (editorRef.current) {
      editorRef.current.setContentFocusHandler(handleFocus);
    }
  }, []);

  useImperativeHandle(
    ref,
    () => {
      return {
        blur: () => {
          editorRef.current.blurContentEditor();
          handleBlur();
        },
        focus: () => {
          editorRef.current.focusContentEditor();
          handleFocus();
        },
      };
    },
    [],
  );

  return (
    <>
      <View
        style={[
          {
            // paddingTop: containerPaddingTop,
            backgroundColor: Colors.white,
          },
          containerStyle,
        ]}>
        <ScrollView
          bounces={false}
          style={{
            height: INPUT_HEIGHT,
          }}>
          <RichEditor
            containerStyle={{
              flex: 0,
              // backgroundColor: Colors.white,
              height: editorHgt,
              // paddingLeft: inputPaddingLeft,
              // paddingTop: containerPaddingTop
            }}
            contentInset={{
              left: -10 + inputPaddingLeft,
              top: -10 + containerPaddingTop,
              right: -10,
              bottom: -10,
            }}
            editorInitializedCallback={onEditorInitialized}
            editorStyle={{
              contentCSSText: `color: ${Colors.text}; font-size: ${FONT_SIZE}px;`,
            }}
            onChange={onChangeFn}
            onHeightChange={handleHeightChange}
            ref={editorRef}
            scrollEnabled={true}
            {...allProps}
            initialContentHTML={defaultContentHTML}
          />
        </ScrollView>

        {!isInitialized && !!content && (
          <View
            style={{
              ...Metrics.absoluteFill,
              top: containerPaddingTop,
              alignItems: 'flex-start',
            }}>
            <Loader />
          </View>
        )}

        {!content && (
          <Text
            pointerEvents="none"
            style={{
              position: 'absolute',
              left: inputPaddingLeft,
              top: containerPaddingTop,
              fontSize: FONT_SIZE,
              color: placeholderTextColor,
            }}>
            {label}
          </Text>
        )}
      </View>

      <RichToolbar
        actions={[
          actions.setBold,
          actions.setItalic,
          actions.insertBulletsList,
          actions.insertOrderedList,
          actions.insertLink,
        ]}
        disabledIconTint={Colors.lightGray}
        editor={editorRef}
        iconSize={moderateScale(40, 0.3)}
        iconTint={Colors.black}
        selectedIconTint={Colors.primary} // default 50
        style={{
          height: moderateScale(50, 0.3),
          backgroundColor: Colors.white,
        }}
      />
    </>
  );
};

FormRichInput = forwardRef(FormRichInput);

FormRichInput.defaultProps = {
  useContainer: false,
  disabled: false,
  defaultContentHTML: '',
  // editorStyle: {},
  // containerStyle: {},
  // style: {},
  label: '',
  inputPaddingTop: moderateScale(16, 0.3),
  inputPaddingLeft: 0, //FORM_PADDING,
  onChange: () => {},
  // initialContentHTML: `<br/>
  //   <center><b>Rich Editor</b></center>
  //   <center>
  //   <a href="https://github.com/wxik/react-native-rich-editor">React Native</a>
  //   <span>And</span>
  //   <a href="https://github.com/wxik/flutter-rich-editor">Flutter</a>
  //   </center>
  //   <br/>
  //   <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/120px-React-icon.svg.png" />
  //   <br/><br/><br/><br/>
  // `
  // editorInitializedCallback
  // onChange
  // onHeightChange

  placeholderTextColor: Colors.lightGray,
  // autoCapitalize: 'none',
  // autoCompleteType: 'off',
  // autoCorrect: false,
  // returnKeyType: 'done',
};

export default FormRichInput;
