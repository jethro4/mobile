import React from 'react';
import {Animated, StyleSheet, TouchableHighlight} from 'react-native';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {View} from '../../../Components/Layout';
import {Text} from '../../../Components/Text';
import SvgIcon from '../../../Components/SvgIcon';
import {moderateScale} from 'react-native-size-matters/extend';

const PANEL_HEADER_HEIGHT = ApplicationStyles.panelHeader.height;

const PanelHeader = (props) => {
  const {
    isAccordion,
    onPress,
    animatedIsVisible,
    label,
    labelProps,
    labelStyle,
    children,
    style,
    ...allProps
  } = props;

  const accordionProps = !isAccordion
    ? {
        disabled: true,
      }
    : {
        underlayColor: Colors.lighterGray,
        onPress,
      };

  return (
    <TouchableHighlight
      style={[styles.panelHeaderView, style]}
      {...accordionProps}
      {...allProps}>
      <>
        <Text
          small
          style={[
            {
              textTransform: 'uppercase',
            },
            labelStyle,
          ]}
          weight="medium"
          {...labelProps}>
          {label}
        </Text>
        {children}
        {isAccordion && (
          <View
            style={{
              position: 'absolute',
              right: Metrics.spaceHorizontal,
              width: moderateScale(32, 0.3),
              alignItems: 'center',
            }}>
            <Animated.View
              style={{
                transform: [
                  {
                    rotate: animatedIsVisible.interpolate({
                      inputRange: [0, 1],
                      outputRange: ['0deg', '180deg'],
                      extrapolate: 'clamp',
                    }),
                  },
                ],
              }}>
              <SvgIcon
                fill={Colors.lightGray}
                height={moderateScale(12, 0.3)}
                name="DownChevron"
                width={moderateScale(12, 0.3)}
              />
            </Animated.View>
          </View>
        )}
      </>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  panelHeaderView: {
    alignItems: 'center',
    backgroundColor: Colors.lightestGray,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: PANEL_HEADER_HEIGHT,
    justifyContent: 'space-between',
    paddingHorizontal: Metrics.spaceHorizontal,
  },
});

PanelHeader.defaultProps = {
  onPress: () => {},
};

export default PanelHeader;
