import React from 'react';
import {ActivityIndicator} from 'react-native';
import {Colors} from '../Themes';

const Loader = (props) => {
  return (
    <ActivityIndicator
      animating
      color={Colors.darkerGray}
      size="small"
      {...props}
    />
  );
};

export default Loader;
