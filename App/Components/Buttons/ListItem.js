import React from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import {Metrics, Colors} from '../../Themes';

const ListItem = (props) => {
  const {style, ...allProps} = props;

  return <TouchableHighlight {...allProps} style={[styles.rowItem, style]} />;
};

ListItem.defaultProps = {
  underlayColor: Colors.silver,
};

const styles = StyleSheet.create({
  rowItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: Colors.border,
    paddingHorizontal: Metrics.spaceHorizontal,
    backgroundColor: Colors.white,
  },
});

export default ListItem;
