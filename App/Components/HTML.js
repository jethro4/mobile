import React from 'react';
import NativeHTML from 'react-native-render-html';
import {ApplicationStyles, Colors} from '../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const HTML = (props) => {
  const {html} = props;
  return (
    <NativeHTML
      html={`<div style="color: ${Colors.text}; font-size: ${moderateScale(
        ApplicationStyles.fontSizes.small,
        0.3,
      )};">${html}</div>`}
    />
  );
};

export default HTML;
