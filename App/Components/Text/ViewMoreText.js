import React, {useEffect, useState, useRef} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {moderateScale} from 'react-native-size-matters/extend';
import SvgIcon from '../../Components/SvgIcon';
import {Colors} from '../../Themes';
import Text from './Normal';

const styles = StyleSheet.create({
  fullTextWrapper: {
    opacity: 0,
    position: 'absolute',
    left: 0,
    top: 0,
  },
  viewMoreText: {
    color: 'blue',
  },
  transparent: {
    opacity: 0,
  },
});

const ViewMoreText = (props) => {
  const {handleShow, containerStyle, children, ...allProps} = props;

  const [isFulltextShown, setIsFulltextShown] = useState(true);
  const [totalLines, setTotalLines] = useState(1);
  const [lessText, setLessText] = useState('');

  const renderFooter = () => {
    if (totalLines > 1) {
      return (
        <TouchableOpacity
          onPress={props.handleShow}
          style={[
            {
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
              // paddingTop: moderateScale(12, 0.3),
            },
            props.footerStyle,
          ]}>
          <Text
            color="yellow"
            style={{
              marginRight: moderateScale(4, 0.3),
            }}
            xxsmall>
            Show more
          </Text>
          <SvgIcon
            fill={Colors.yellow}
            height={moderateScale(10, 0.3)}
            name="DownChevron"
            width={moderateScale(10, 0.3)}
          />
        </TouchableOpacity>
      );
    }
    return null;
  };

  const handleTextLayout = (e) => {
    const {lines = []} = e.nativeEvent;

    if (lines.length) {
      const text = lines[0].text?.replace(/\n/g, '') || '';
      setLessText(text);
      setTotalLines(lines.length);
    }

    setIsFulltextShown(false);
  };

  const renderFullText = () => {
    if (isFulltextShown) {
      return (
        <View style={styles.fullTextWrapper}>
          <Text onTextLayout={handleTextLayout} style={props.textStyle}>
            {props.children}
          </Text>
        </View>
      );
    }
    return null;
  };

  return (
    <View style={containerStyle}>
      <View>
        <Text {...allProps}>{lessText}</Text>
      </View>
      {renderFooter()}

      {renderFullText()}
    </View>
  );
};

ViewMoreText.propTypes = {
  numberOfLines: PropTypes.number,
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

ViewMoreText.defaultProps = {
  children: '',
  containerStyle: {
    flex: 1,
    justifyContent: 'space-between',
  },
  handleShow: () => {},
  textStyle: {},
};

export default ViewMoreText;
