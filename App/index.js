import './Config';
import './Containers/Shared/ErrorHandler';
import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import RootContainer from './Containers/RootContainer';
import {persistCache} from 'apollo3-cache-persist';
import {
  ApolloClient,
  ApolloLink,
  ApolloProvider,
  HttpLink,
} from '@apollo/client';
import {createAuthLink} from 'aws-appsync-auth-link';
import AppSyncConfig from './Config/AppSyncConfig';
import cache, {setupDetailsVar, rehydrateVars} from './Cache';
import {onError} from '@apollo/client/link/error';
import SentryConfig from './Config/SentryConfig';
import {hasConnection, showNoConnectionAlert} from './Utils/network';
import {createUploadLink} from 'apollo-upload-client';
import {RetryLink} from '@apollo/client/link/retry';

// import { CREATE_NOTE } from './GraphQL/NotesTasks'

const url = AppSyncConfig.graphqlEndpoint;
const region = AppSyncConfig.region;
const auth = {
  type: AppSyncConfig.authenticationType,
  apiKey: AppSyncConfig.apiKey,
};

const retryLink = new RetryLink();

const appLink = new ApolloLink(async (operation, forward) => {
  const {appName, apiKey} = setupDetailsVar();
  const mutationKey = operation.variables.__mutationkey;

  if (mutationKey) {
    const mutationVariables = operation.variables[mutationKey];

    operation.variables[mutationKey] = {
      appName,
      apiKey,
      ...mutationVariables,
    };
  } else {
    operation.variables = {appName, apiKey, ...operation.variables};
  }

  return forward(operation);
});

const errorLink = onError(({graphQLErrors, networkError, operation}) => {
  const mutationKey = operation.variables.__mutationkey;

  if (graphQLErrors) {
    graphQLErrors.forEach((err) => {
      const {message, locations, path} = err;
      console.tron.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        `Operation error name: ${operation.operationName}`,
      );

      SentryConfig.captureException(err);
    });
  }

  if (networkError && mutationKey) {
    console.tron.log(`[Network error on mutation]: ${networkError}`);

    // hasConnection().then((isConnected) => {
    //   if (!isConnected) {
    //     showNoConnectionAlert();
    //   }
    // });
  }
});

const link = ApolloLink.from([
  retryLink,
  createAuthLink({url, region, auth}),
  appLink,
  errorLink,
  createUploadLink({uri: url}),
]);

const createClient = () => {
  return new ApolloClient({
    link,
    cache,
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'cache-and-network',
        errorPolicy: 'all',
      },
      query: {
        fetchPolicy: 'network-only',
        errorPolicy: 'all',
      },
      mutate: {
        errorPolicy: 'all',
      },
    },
  });
};

// const variables = {
//   appName: 'staging',
//   apiKey: 1552679244144731,
//   q: '',
//   page: 0,
//   limit: 10,
// };
// client
//   .query({
//     query: LIST_CONTACTS,
//     variables,
//   })
//   .then((result) => console.tron.log('Client result: ', result))
//   .catch((err) => console.tron.log('Error on Client: ' + err));

const App = () => {
  const [client, setClient] = useState(null);

  useEffect(() => {
    async function init() {
      await persistCache({
        cache,
        storage: AsyncStorage,
      });
      const apolloClient = createClient();

      await rehydrateVars();

      console.tron.log('Apollo Client: ', apolloClient);

      setClient(apolloClient);
    }
    init();
  }, []);

  if (!client) {
    return null;
  }

  return (
    <ApolloProvider client={client}>
      {/* <Rehydrated> */}
      <RootContainer />
      {/* </Rehydrated> */}
    </ApolloProvider>
  );
};

// allow reactotron overlay for fast design in dev mode
export default App;
