import {gql} from '@apollo/client';

export default gql`
  mutation completeTask($completeTaskInput: CompleteTaskInput!) {
    completeTask(input: $completeTaskInput) {
      id
    }
  }
`;
