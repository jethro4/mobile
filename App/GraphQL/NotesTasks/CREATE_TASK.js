import {gql} from '@apollo/client';

export default gql`
  mutation createTask($createTaskInput: CreateTaskInput!) {
    createTask(input: $createTaskInput) {
      id
    }
  }
`;
