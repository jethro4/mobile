import {gql} from '@apollo/client';

export default gql`
  mutation updateNote($updateNoteInput: UpdateNoteInput!) {
    updateNote(input: $updateNoteInput) {
      id
    }
  }
`;
