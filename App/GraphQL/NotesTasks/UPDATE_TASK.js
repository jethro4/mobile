import {gql} from '@apollo/client';

export default gql`
  mutation updateTask($updateTaskInput: UpdateTaskInput!) {
    updateTask(input: $updateTaskInput) {
      id
    }
  }
`;
