import {gql} from '@apollo/client';

export default gql`
  mutation createOrUpdateContact(
    $createOrUpdateContactInput: CreateOrUpdateContactInput!
  ) {
    createOrUpdateContact(input: $createOrUpdateContactInput) {
      id
    }
  }
`;
