import {gql} from '@apollo/client';

export default gql`
  query getTaskFilters {
    taskFilters @client {
      filterBy
      terms
      tags
    }
  }
`;
