import {gql} from '@apollo/client';

export default gql`
  mutation createDONote($createDONoteInput: CreateDONoteInput!) {
    createDONote(input: $createDONoteInput) {
      id
    }
  }
`;
