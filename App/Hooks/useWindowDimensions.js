import {useState, useEffect} from 'react';
import {Dimensions} from 'react-native';

const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

export default function useWindowDimensions() {
  const [dimensions, setDimensions] = useState({window, screen});

  const onChange = ({window, screen}) => {
    setDimensions({window, screen});
  };

  useEffect(() => {
    Dimensions.addEventListener('change', onChange);
    return () => {
      Dimensions.removeEventListener('change', onChange);
    };
  }, []);

  return {
    windowWidth: dimensions.window.width,
    windowHeight: dimensions.window.height,
    screenWidth: dimensions.screen.width,
    screenHeight: dimensions.screen.height,
  };
}
