import {useState, useEffect} from 'react';
import NetInfo from '@react-native-community/netinfo';

export default function useConnection() {
  const [loading, setLoading] = useState(true);
  const [connected, setConnected] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    NetInfo.fetch()
      .then(({isConnected}) => {
        setConnected(isConnected);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return {loading, connected, error};
}
