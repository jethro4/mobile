import React, {useEffect, useState} from 'react';
import {StatusBar} from 'react-native';
// import HomeScreen from '../Containers/HomeScreen';
import {ContactsSearchScreen} from '../Containers/ContactsScreens';

import AdminScreen from '../Containers/AdminScreen';
import {Colors, Metrics, ApplicationStyles} from '../Themes';
import {setPrimaryColor} from '../Themes/Colors';
import SvgIcon from '../Components/SvgIcon';
import {moderateScale} from 'react-native-size-matters/extend';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {View} from '../Components/Layout';
import {Text} from '../Components/Text';
import Loader from '../Components/Loader';
import {GET_APP_SETTINGS, GET_ACCESS_PERMISSIONS} from '../GraphQL/Admin';
import {useQuery} from '@apollo/client';
import Const from '../Constants';

const Tab = createBottomTabNavigator();
const TAB_ICON_SIZE = moderateScale(22, 0.4);

const FullScreenLoader = () => {
  return (
    <View alignCenter isFlex justifyCenter>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent
      />
      <View alignCenter row>
        <Loader />
        <Text
          color="darkerGray"
          style={{
            marginLeft: moderateScale(8, 0.3),
          }}>
          Loading app settings
        </Text>
      </View>
    </View>
  );
};

const LoggedInTabNavigation = () => {
  const [initialLoaded, setInitialLoaded] = useState(false);
  const insets = useSafeAreaInsets();
  const bottomInset = insets.bottom;

  const {data: {getAppSettings} = {}, loading: loadingAppSettings} = useQuery(
    GET_APP_SETTINGS,
    {
      fetchPolicy: Const.FETCH_POLICIES.CACHE_AND_NETWORK,
    },
  );

  useEffect(() => {
    if (!loadingAppSettings && !initialLoaded && getAppSettings?.uiColour) {
      setPrimaryColor(getAppSettings?.uiColour);
      setInitialLoaded(true);
    }
  }, [loadingAppSettings, getAppSettings?.uiColour, initialLoaded]);

  if (!initialLoaded) {
    return <FullScreenLoader />;
  }

  return (
    <>
      <StatusBar barStyle="light-content" />
      <Tab.Navigator
        initialRouteName="Contacts"
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            // let color = focused ? Colors.primary : Colors.darkGray

            if (route.name === 'Home') {
              iconName = focused ? 'HomeFilled' : 'HomeOutlined';
            } else if (route.name === 'Contacts') {
              iconName = focused ? 'ContactsFilled' : 'ContactsOutlined';
            }
            // else if (route.name === 'SavedSearches') {
            //   iconName = focused
            //     ? 'SourceFilled'
            //     : 'SourceOutlined'
            // }
            else if (route.name === 'Admin') {
              iconName = focused ? 'SettingsFilled' : 'SettingsOutlined';
            }

            // You can return any component that you like here!
            return (
              <SvgIcon
                fill={color}
                height={TAB_ICON_SIZE}
                name={iconName}
                width={TAB_ICON_SIZE}
              />
            );
          },
        })}
        tabBarOptions={{
          style: {
            height:
              moderateScale(Metrics.isTablet ? 50 : 60, 0.4) + bottomInset,
            backgroundColor: Colors.white,
          },
          activeTintColor: Colors.yellow,
          inactiveTintColor: Colors.darkerGray,
          labelStyle: {
            fontFamily: ApplicationStyles.getFontByWeight('regular'),
            fontSize: moderateScale(11, 0.4),
          },
          tabStyle: {
            paddingTop: Metrics.isTablet ? 0 : moderateScale(6, 0.4),
            paddingBottom: Metrics.isTablet
              ? 0
              : moderateScale(6, 0.4) + moderateScale(4, 0.4),
          },
        }}>
        {/* <Tab.Screen component={HomeScreen} name="Home" /> */}
        <Tab.Screen component={ContactsSearchScreen} name="Contacts" />
        {/* <Tab.Screen name="SavedSearches" options={{ title: 'Saved' }} component={SavedSearchesScreen} />       */}
        <Tab.Screen component={AdminScreen} name="Admin" />
      </Tab.Navigator>
    </>
  );
};

export default LoggedInTabNavigation;
