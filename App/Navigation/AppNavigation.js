import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  ContactAddScreen,
  ContactDetailsScreen,
} from '../Containers/ContactsScreens';
import LoginScreen from '../Containers/LoginScreen';
import LoggedInTabNavigation from './LoggedInTabNavigation';
import {NoteTaskAddScreen} from '../Containers/NoteTaskScreens';
import {
  // DataObjectAddScreen,
  DataObjectItemsScreen,
} from '../Containers/DataObjectScreens';
import {Colors} from '../Themes';
import {GET_SESSION} from '../GraphQL/Cache';
import {useQuery} from '@apollo/client';
import DataObjectTabNavigation from './DataObjectTabNavigation';

const Stack = createStackNavigator();

const AppNavigation = () => {
  const {
    loading: loadingSessionId = true,
    data: {session = {}} = {},
  } = useQuery(GET_SESSION);

  console.tron.log('sessionId: ', session, session.sessionId, loadingSessionId);

  if (loadingSessionId) {
    return null;
  }

  return (
    <Stack.Navigator
      initialRouteName={!session.sessionId ? 'Login' : 'Home'}
      screenOptions={({route}) => ({
        headerShown: false,
        cardStyle: {
          backgroundColor: Colors.background,
        },
      })}>
      <Stack.Screen component={LoginScreen} name="Login" />
      <Stack.Screen component={LoggedInTabNavigation} name="Home" />
      <Stack.Screen component={ContactAddScreen} name="ContactAdd" />
      <Stack.Screen component={ContactDetailsScreen} name="ContactDetails" />
      <Stack.Screen component={NoteTaskAddScreen} name="NoteTaskAdd" />
      <Stack.Screen component={DataObjectItemsScreen} name="DataObjectItems" />
      <Stack.Screen component={DataObjectTabNavigation} name="DataObjectTabs" />
    </Stack.Navigator>
  );
};

export default AppNavigation;
