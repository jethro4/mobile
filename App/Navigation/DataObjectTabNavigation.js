import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Alert, TouchableOpacity} from 'react-native';
import Details from '../Containers/DataObjectScreens/DataObjectItemsScreen/Details';
import Relationships from '../Containers/DataObjectScreens/DataObjectItemsScreen/Relationships';
import Notes from '../Containers/DataObjectScreens/DataObjectItemsScreen/Notes';
import Attachments from '../Containers/DataObjectScreens/DataObjectItemsScreen/Attachments';
import {Colors, ApplicationStyles, Metrics} from '../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ScreenContainer from '../Containers/Shared/ScreenContainer';
import {useNavigation, useRoute} from '@react-navigation/native';
import R from 'ramda';
import {assignOrConcat, sortArrayByObjectKey} from '../Utils/array';
import {setupDetailsVar} from '../Cache';

import {
  CONNECT_OR_DISCONNECT_RELATIONSHIP,
  CREATE_DO_NOTE,
  UPLOAD_DO_ATTACHMENTS,
  LIST_RELATIONSHIPS,
  CREATE_OR_UPDATE_DO_ITEMS,
} from '../GraphQL/DataObjects';
import {Text} from '../Components/Text';
import Loader from '../Components/Loader';
import {useQuery, useMutation} from '@apollo/client';

const Tab = createMaterialTopTabNavigator();

const uploadFile = async (file, doItemId) => {
  const {appName, apiKey} = setupDetailsVar();
  const url = `https://${appName}.macantacrm.com/rest/v1/data_object/attache/file/item/${doItemId}`;

  console.tron.log('Uploading file', file);
  const data = new FormData();
  data.append('access_key', apiKey);

  data.append('CDFileAttachments', {
    uri: file.uri,
    type: file.type,
    name: file.name,
  });

  return await fetch(url, {
    method: 'POST',
    body: data,
    headers: {
      'Content-Type': 'multipart/form-data',
      Accept: 'application/json',
    },
  });
};

const DataObjectTabNavigation = (props) => {
  const {
    contactId,
    doId: groupId,
    doItemId,
    fields,
    items,
    connectedContacts,
    attachments,
    notes,
    isCreate = false,
  } = props.route.params;
  const {
    loading: loadingRelationships,
    data: {listRelationships = []} = {},
  } = useQuery(LIST_RELATIONSHIPS, {
    variables: {
      groupId,
    },
  });

  const [callDOMutation] = useMutation(CREATE_OR_UPDATE_DO_ITEMS, {
    onCompleted(data = {}) {
      console.tron.log('onCompleted CREATE_OR_UPDATE_ITEMS', data);
      if (data.createOrUpdateDataObjectItem?.id) {
        doItemIdRef.current = data.createOrUpdateDataObjectItem.id;
      }
    },
    onError() {},
  });

  const [callRelationshipMutation] = useMutation(
    CONNECT_OR_DISCONNECT_RELATIONSHIP,
    {
      onError() {},
    },
  );
  const [callDONoteMutation] = useMutation(CREATE_DO_NOTE, {
    onError() {},
  });

  // useEffect(() => {
  //   if (props.route.params?.doNoteValues) {
  //     console.tron.log('Hoy DO Tab notes', props.route.params?.doNoteValues);
  //     handleData('doNoteValues')(props.route.params?.doNoteValues);
  //   }
  // }, [props.route.params]);

  const navigation = useNavigation();
  const route = useRoute();

  const doItemIdRef = useRef(doItemId);
  const hasChangesRef = useRef(false);

  const handleData = useCallback(
    (doKey) => async (doValue) => {
      console.tron.log('handleData: ' + doKey + ' ' + doValue);
      hasChangesRef.current = true;

      if (!doItemIdRef.current) {
        await callDOMutation({
          variables: {
            createOrUpdateDataObjectItemInput: {
              groupId,
              contactId,
              relationship: listRelationships[0].role,
            },
            __mutationkey: 'createOrUpdateDataObjectItemInput',
          },
        });
      }

      if (doKey === 'detailValues') {
        const contactRelationship = connectedContacts.find(
          (c) => c.contactId === contactId,
        );

        const relationship =
          (contactRelationship &&
            contactRelationship.relationships.map((r) => r.role).join(',')) ||
          listRelationships[0].role;

        await callDOMutation({
          variables: {
            createOrUpdateDataObjectItemInput: {
              groupId,
              doItemId: doItemIdRef.current,
              contactId,
              relationship,
              fields: Object.entries(doValue).map(([key, value]) => ({
                key,
                value,
              })),
            },
            __mutationkey: 'createOrUpdateDataObjectItemInput',
          },
        });
      } else if (doKey === 'relationshipValues') {
        await Promise.all(
          Object.entries(doValue).map(
            async ([connectedContactId, relationships]) => {
              console.tron.log(
                'connectedContactId',
                connectedContactId,
                relationships,
              );
              await callRelationshipMutation({
                variables: {
                  connectOrDisconnectRelationshipInput: {
                    doItemId: doItemIdRef.current,
                    contactId: connectedContactId,
                    groupId,
                    role: relationships.join(','),
                  },
                  __mutationkey: 'connectOrDisconnectRelationshipInput',
                },
              });
            },
          ),
        );
      } else if (doKey === 'doNoteValues') {
        await callDONoteMutation({
          variables: {
            createDONoteInput: {...doValue, doItemId: doItemIdRef.current},
            __mutationkey: 'createDONoteInput',
          },
        });
      } else if (doKey === 'doAttachmentValues') {
        const responseFile = await uploadFile(doValue, doItemIdRef.current);
        const {data} = await responseFile.json();
        const dataFiles = data[doItemIdRef.current];

        return dataFiles;
      }
    },
    [
      callDOMutation,
      callRelationshipMutation,
      callDONoteMutation,
      contactId,
      connectedContacts,
      groupId,
      listRelationships,
    ],
  );

  const renderDetails = useCallback(
    (props) => {
      return (
        <Details
          {...props}
          fields={fields}
          handleData={handleData('detailValues')}
          isCreate={isCreate}
          items={items}
        />
      );
    },
    [items, fields, handleData],
  );

  const renderRelationships = useCallback(
    (props) => {
      console.tron.log('renderRelationships', listRelationships);
      return (
        <Relationships
          {...props}
          connectedContacts={connectedContacts}
          contactId={contactId}
          handleData={handleData('relationshipValues')}
          navigation={navigation}
          relationships={listRelationships}
        />
      );
    },
    [connectedContacts, contactId, navigation, listRelationships, handleData],
  );

  const renderNotes = useCallback(
    (props) => {
      return (
        <Notes
          {...props}
          contactId={contactId}
          doNoteValues={route.params?.doNoteValues}
          handleData={handleData('doNoteValues')}
          navigation={navigation}
          notes={notes}
        />
      );
    },
    [contactId, navigation, notes, route.params?.doNoteValues, handleData],
  );

  const renderAttachments = useCallback(
    (props) => {
      return (
        <Attachments
          {...props}
          attachments={attachments}
          handleData={handleData('doAttachmentValues')}
        />
      );
    },
    [attachments, handleData],
  );

  const handleBack = useCallback(() => {
    if (hasChangesRef.current) {
      props.navigation.navigate('DataObjectItems', {hasItemSaved: true});
    } else {
      props.navigation.goBack();
    }
  }, [hasChangesRef.current, props.navigation]);

  return (
    <ScreenContainer headerTitle={doItemId} onBack={handleBack}>
      {loadingRelationships ? (
        <Loader
          size="large"
          style={{
            marginTop: moderateScale(20, 0.3),
          }}
        />
      ) : (
        <Tab.Navigator
          initialRouteName="Details"
          sceneContainerStyle={{
            backgroundColor: Colors.white,
          }}
          tabBarOptions={{
            // style: {
            //   marginBottom: moderateScale(8, 0.3),
            // },
            tabStyle: {
              height: moderateScale(46, 0.3),
            },
            indicatorStyle: {
              backgroundColor: Colors.primary,
            },
            activeTintColor: Colors.primary,
            inactiveTintColor: Colors.lightGray,
            labelStyle: {
              fontFamily: ApplicationStyles.getFontByWeight('medium'),
              fontSize: moderateScale(12, 0.3),
              margin: 0,
            },
          }}>
          <Tab.Screen
            component={renderDetails}
            name="Details"
            options={{title: 'Details'}}
          />
          <Tab.Screen
            component={renderRelationships}
            name="Relationships"
            options={{title: "R'ships"}}
          />
          <Tab.Screen
            component={renderNotes}
            name="Notes"
            options={{title: 'Notes'}}
          />
          <Tab.Screen
            component={renderAttachments}
            name="Files"
            options={{title: 'Files'}}
          />
        </Tab.Navigator>
      )}
    </ScreenContainer>
  );
};

export default DataObjectTabNavigation;
