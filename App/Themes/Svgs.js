import React from 'react';
import {
  G,
  Path,
  Circle,
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

const svgs = {
  Plus: {
    svg: (
      <G>
        <Path d="M192 149.4V0h-42.6v149.4H0V192h149.4v149.4H192V192h149.4v-42.6z" />
      </G>
    ),
    viewBox: '0 0 341.4 341.4',
  },
  Subtract: {
    svg: (
      <G>
        <Path d="M7.308 68.211h107.188a7.309 7.309 0 007.309-7.31 7.308 7.308 0 00-7.309-7.309H7.308a7.31 7.31 0 000 14.619z" />
      </G>
    ),
    viewBox: '0 0 121.805 121.804',
  },
  Flag: {
    svg: (
      <G transform="translate(0.2 0.2)">
        <G transform="translate(0)">
          <Path
            class="a"
            d="M52.842,4.907l-.617-1.542.617-1.542a.245.245,0,0,0-.227-.336H49.756V.915A.245.245,0,0,0,49.511.67H46.49V.245a.245.245,0,0,0-.49,0V7.677a.265.265,0,0,0,.245.267c.135,0,.245.135.245,0V4.426h1.96V5a.246.246,0,0,0,.245.245h3.92a.245.245,0,0,0,.227-.336Zm-3.575-.971H46.49V1.16h2.776Zm.02.817.4-.4a.248.248,0,0,0,.072-.173v-2.2h2.5l-.519,1.3a.245.245,0,0,0,0,.182l.519,1.3Z"
            transform="translate(-46)"
          />
        </G>
      </G>
    ),
    viewBox: '0 0 7.263 8.404',
  },
  Close: {
    svg: (
      <G>
        <Path d="M14 1.41L12.59 0 7 5.59 1.41 0 0 1.41 5.59 7 0 12.59 1.41 14 7 8.41 12.59 14 14 12.59 8.41 7z" />
      </G>
    ),
    viewBox: '0 0 14 14',
  },
  Edit: {
    svg: (
      <G>
        <Path d="M370.59 230.965c-5.524 0-10 4.476-10 10v88.793c-.02 16.558-13.438 29.98-30 30H50c-16.563-.02-29.98-13.442-30-30V69.168c.02-16.563 13.438-29.98 30-30h88.79c5.523 0 10-4.477 10-10s-4.477-10-10-10H50c-27.602.031-49.969 22.398-50 50v260.59c.031 27.601 22.398 49.969 50 50h280.59c27.601-.031 49.969-22.399 50-50v-88.79c0-5.523-4.477-10.003-10-10.003zm0 0" />
        <Path d="M156.367 178.344L302.38 32.328l47.09 47.09-146.012 146.016zm0 0M132.543 249.258l52.039-14.414-37.625-37.625zm0 0M362.488 7.578c-9.77-9.746-25.586-9.746-35.355 0l-10.606 10.606 47.09 47.09 10.606-10.606c9.75-9.77 9.75-25.586 0-35.355zm0 0" />
      </G>
    ),
    viewBox: '0 -1 381.534 381',
  },
  EditPencil: {
    svg: (
      <G>
        <Path d="M0 303.947v80h80l236.053-236.054-80-80zM377.707 56.053L327.893 6.24c-8.32-8.32-21.867-8.32-30.187 0l-39.04 39.04 80 80 39.04-39.04c8.321-8.32 8.321-21.867.001-30.187z" />
      </G>
    ),
    viewBox: '0 0 383.947 383.947',
  },
  DownCaret: {
    svg: (
      <G>
        <Path
          d="M3.015 3.018L.195.608a.47.47 0 01-.2-.317c0-.18.247-.292.661-.292h6.099c.413 0 .66.111.66.291a.468.468 0 01-.2.315l-2.82 2.411a1.106 1.106 0 01-1.377 0z"
          fill="#3d4d60"
        />
      </G>
    ),
    viewBox: '0 0 7.415 3.258',
  },
  DownChevron: {
    svg: (
      <Path
        d="M92.672,144.373c-2.752,0-5.493-1.044-7.593-3.138L3.145,59.301c-4.194-4.199-4.194-10.992,0-15.18
            c4.194-4.199,10.987-4.199,15.18,0l74.347,74.341l74.347-74.341c4.194-4.199,10.987-4.199,15.18,0
            c4.194,4.194,4.194,10.981,0,15.18l-81.939,81.934C98.166,143.329,95.419,144.373,92.672,144.373z"
      />
    ),
    viewBox: '0 0 185.344 185.344',
  },
  LeftChevron: {
    svg: (
      <G>
        <Path d="M6.533 11.986l1.41-1.41-4.58-4.59 4.58-4.59-1.41-1.41-6 6z" />
      </G>
    ),
    viewBox: '0 0 8 12',
  },
  RightChevron: {
    svg: (
      <G>
        <Path
          d="M181.776,107.719L78.705,4.648c-6.198-6.198-16.273-6.198-22.47,0s-6.198,16.273,0,22.47
            l91.883,91.883l-91.883,91.883c-6.198,6.198-6.198,16.273,0,22.47s16.273,6.198,22.47,0l103.071-103.039
            c3.146-3.146,4.672-7.246,4.64-11.283C186.416,114.902,184.89,110.833,181.776,107.719z"
        />
      </G>
    ),
    viewBox: '0 0 238.003 238.003',
  },
  Check: {
    svg: (
      <G>
        <Defs>
          <LinearGradient
            gradientUnits="objectBoundingBox"
            id="a"
            x1=".5"
            x2=".5"
            y2="1">
            <Stop offset="0" stopColor="#ffd300" />
            <Stop offset="1" stopColor="#ff9f00" />
          </LinearGradient>
        </Defs>
        <Path
          d="M0 92.761l23.832 23.878 45.762-45.717-6.57-6.479-39.192 39.147L6.479 86.237z"
          fill="url(#a)"
          transform="translate(0 -64.443)"
        />
      </G>
    ),
    viewBox: '0 0 69.594 52.196',
  },
  Lock: {
    svg: (
      <G>
        <Path
          d="M405.333,170.667v-21.333C405.333,67.008,338.347,0,256,0S106.667,67.008,106.667,149.333v21.333
        c-35.285,0-64,28.715-64,64V448c0,35.285,28.715,64,64,64h298.667c35.285,0,64-28.715,64-64V234.667
        C469.333,199.381,440.619,170.667,405.333,170.667z M149.333,149.333c0-58.816,47.851-106.667,106.667-106.667
        s106.667,47.851,106.667,106.667v21.333H149.333V149.333z M426.667,448c0,11.776-9.579,21.333-21.333,21.333H106.667
        c-11.755,0-21.333-9.557-21.333-21.333V234.667c0-11.776,9.579-21.333,21.333-21.333h298.667c11.755,0,21.333,9.557,21.333,21.333
        V448z"
        />
        <Path
          d="M256,245.333c-35.285,0-64,28.715-64,64c0,27.776,17.899,51.243,42.667,60.075v35.925
        c0,11.797,9.557,21.333,21.333,21.333c11.776,0,21.333-9.536,21.333-21.333v-35.925C302.101,360.576,320,337.109,320,309.333
        C320,274.048,291.285,245.333,256,245.333z M256,330.667c-11.755,0-21.333-9.557-21.333-21.333S244.245,288,256,288
        c11.755,0,21.333,9.557,21.333,21.333S267.755,330.667,256,330.667z"
        />
      </G>
    ),
    viewBox: '0 0 512 512',
  },
  AppIcon: {
    svg: (
      <G>
        <Defs>
          <LinearGradient id="a" x1="-18.2%" x2="112.5%" y1="33.2%" y2="76.8%">
            <Stop offset="0%" stopColor="#FFEE98" />
            <Stop offset="100%" stopColor="#FF9F00" />
          </LinearGradient>
          <LinearGradient id="b" x1="50%" x2="50%" y1="0%" y2="100%">
            <Stop offset="0%" stopColor="#FFD600" />
            <Stop offset="100%" stopColor="#FF9F00" />
          </LinearGradient>
        </Defs>
        <G fill="none" fillRule="evenodd" transform="translate(1 1)">
          <Rect
            fill="url(#a)"
            fillRule="nonzero"
            height="98"
            rx="24"
            width="98"
          />
          <Rect
            height="97.355"
            rx="23.5"
            stroke="#707070"
            width="97.355"
            x=".322"
            y=".322"
          />
          <Rect
            fill="url(#b)"
            fillRule="nonzero"
            height="92.842"
            rx="20"
            width="92.842"
            x="2.579"
            y="2.579"
          />
        </G>
      </G>
    ),
    viewBox: '0 0 100 100',
  },
  HomeBg: {
    svg: (
      <G>
        <Path
          d="M193.68 536.272C160.352 685.53.96 700.166.96 700.166h23.114c-2.703.405-5.155.734-7.317.999H1.025v-.43H.586V.81h183.768C129.082 22.753 80.65 62.528 71.404 134.058 47.965 315.43 168.61 333.753 191.895 512.37c1.086 8.326 1.666 16.288 1.786 23.902z"
          fill="#F5F6F9"
          fillRule="nonzero"
        />
      </G>
    ),
    viewBox: '0 0 194 702',
  },
  Check2: {
    svg: (
      <Path d="m.3,14c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.4-0.4 1-0.4 1.4,0l.1,.1 5.5,5.9c0.2,0.2 0.5,0.2 0.7,0l13.4-13.9h0.1v-8.88178e-16c0.4-0.4 1-0.4 1.4,0l1.4,1.4c0.4,0.4 0.4,1 0,1.4l0,0-16,16.6c-0.2,0.2-0.4,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.8-8.4-.2-.3z" />
    ),
    viewBox: '0 0 26 26',
  },
  Menu: {
    svg: (
      <Path d="M18.233 5.822H.767a.767.767 0 100 1.535h17.466a.767.767 0 100-1.535zM18.233 0H.767a.767.767 0 000 1.535h17.466a.767.767 0 000-1.535zM18.233 11.646H.767a.768.768 0 000 1.535h17.466a.768.768 0 000-1.535z" />
    ),
    viewBox: '0 0 19 13.178',
  },
  VerticalMenu: {
    svg: (
      <G>
        <Circle cx="256" cy="256" r="64" />
        <Circle cx="256" cy="448" r="64" />
        <Circle cx="256" cy="64" r="64" />
      </G>
    ),
    viewBox: '0 0 512 512',
  },
  HorizontalMenu: {
    svg: (
      <G fill="none" fill-rule="evenodd" stroke="none" strokeWidth="1">
        <G transform="translate(-324.000000, -144.000000)">
          <G id="Group-2" transform="translate(0.000000, 124.000000)">
            <G id="Mask">
              <Path
                d="M326,20 C324.9,20 324,20.9 324,22 C324,23.1 324.9,24 326,24 C327.1,24 328,23.1 328,22 C328,20.9 327.1,20 326,20 L326,20 Z M338,20 C336.9,20 336,20.9 336,22 C336,23.1 336.9,24 338,24 C339.1,24 340,23.1 340,22 C340,20.9 339.1,20 338,20 L338,20 Z M332,20 C330.9,20 330,20.9 330,22 C330,23.1 330.9,24 332,24 C333.1,24 334,23.1 334,22 C334,20.9 333.1,20 332,20 L332,20 Z"
                id="Path-1"
              />
            </G>
          </G>
        </G>
      </G>
    ),
    viewBox: '0 0 16 4',
  },
  Notification: {
    svg: (
      <Path
        d="M11.895 11.07l-.718-.478V6.438a5.084 5.084 0 00-4.615-5.056v-.82a.462.462 0 10-.923 0v.821a5.084 5.084 0 00-4.615 5.056v4.153l-.718.478a.461.461 0 00-.206.384V13.3a.462.462 0 00.462.462H3.6a2.535 2.535 0 004.992 0h3.042a.462.462 0 00.466-.462v-1.846a.461.461 0 00-.205-.384zM6.1 14.931a1.621 1.621 0 01-1.55-1.169h3.1a1.621 1.621 0 01-1.55 1.169zm5.077-2.092H1.023V11.7l.718-.478a.461.461 0 00.206-.384v-4.4a4.154 4.154 0 118.308 0v4.4a.461.461 0 00.206.384l.718.478z"
        fill="#717c84"
        stroke="#717c84"
        strokeWidth=".2"
      />
    ),
    viewBox: '0 0 12.2 15.954',
  },
  Profile: {
    svg: (
      <G>
        <Circle cx="129.375" cy="60" fill="#BABFC3" r="60" />
        <Path
          d="M129.375,150c-60.061,0-108.75,48.689-108.75,108.75h217.5C238.125,198.689,189.436,150,129.375,150z"
          fill="#BABFC3"
        />
      </G>
    ),
    viewBox: '0 0 258.75 258.75',
  },
  Camera: {
    svg: (
      <G>
        <G transform="translate(.1 .1)">
          <Path
            d="M12.283 1.558a1.366 1.366 0 00-1-.423h-2v-.024a1.084 1.084 0 00-.326-.785A1.1 1.1 0 008.172 0H4.541a1.113 1.113 0 00-1.123 1.111v.024H1.425a1.366 1.366 0 00-1 .423 1.436 1.436 0 00-.423 1v6.014a1.366 1.366 0 00.423 1 1.436 1.436 0 001 .423h9.855a1.366 1.366 0 001-.423 1.436 1.436 0 00.423-1V2.56a1.366 1.366 0 00-.42-1.002zm-.205 7.017h-.012a.784.784 0 01-.785.785H1.425a.784.784 0 01-.785-.785V2.56a.784.784 0 01.785-.785h2.331a.325.325 0 00.326-.326V1.1a.455.455 0 01.471-.471h3.611a.455.455 0 01.471.471v.35a.325.325 0 00.326.326h2.331a.783.783 0 01.785.785z"
            fill="#3d4d60"
            stroke="#3d4d60"
            strokeWidth=".2"
          />
          <Path
            d="M6.353 2.597a2.969 2.969 0 102.1.869 2.976 2.976 0 00-2.1-.869zm1.643 4.626a2.333 2.333 0 01-3.285 0 2.31 2.31 0 01-.676-1.643 2.359 2.359 0 01.676-1.642 2.309 2.309 0 011.643-.676 2.361 2.361 0 011.643.676 2.308 2.308 0 01.676 1.642 2.261 2.261 0 01-.677 1.643z"
            fill="#3d4d60"
            stroke="#3d4d60"
            strokeWidth=".2"
          />
          <Circle
            cx=".592"
            cy=".592"
            fill="#3d4d60"
            r=".592"
            stroke="#3d4d60"
            strokeWidth=".2"
            transform="translate(10.06 2.585)"
          />
        </G>
      </G>
    ),
    viewBox: '0 0 12.905 10.2',
  },
  Album: {
    svg: (
      <G>
        <Path
          d="M8.75 0h-7.5A1.251 1.251 0 000 1.25v7.5A1.251 1.251 0 001.25 10h7.5A1.251 1.251 0 0010 8.75v-7.5A1.251 1.251 0 008.75 0zm-7.5.833h7.5a.417.417 0 01.417.417V4.2L7.795 2.83a.417.417 0 00-.589 0L4.271 5.765l-1.06-1.06a.417.417 0 00-.589 0L.833 6.494V1.25A.417.417 0 011.25.833zm7.5 8.333h-7.5a.417.417 0 01-.417-.416V7.673l2.084-2.084L5.33 8a.417.417 0 10.589-.589L4.86 6.354l2.64-2.64 1.667 1.667V8.75a.417.417 0 01-.417.417zm0 0"
          fill="#3d4d60"
        />
        <Path
          d="M3.334 2.5a.833.833 0 11-.834-.833.833.833 0 01.834.833zm0 0"
          fill="#3d4d60"
        />
      </G>
    ),
    viewBox: '0 0 10 10',
  },
  Video: {
    svg: (
      <G>
        <Path
          d="M15.295 1.01a.325.325 0 00-.321-.006l-3.511 1.918V1.771A1.623 1.623 0 009.842.15H1.771A1.623 1.623 0 00.15 1.771v6.4a1.623 1.623 0 001.621 1.623h8.07a1.623 1.623 0 001.621-1.621v-1.13l3.512 1.917a.324.324 0 00.48-.285V1.289a.324.324 0 00-.159-.279zm-4.481 7.162a.974.974 0 01-.973.978h-8.07a.974.974 0 01-.973-.973v-6.4a.974.974 0 01.973-.979h8.07a.974.974 0 01.973.973zm3.991-.043l-3.342-1.825V3.66l3.343-1.825zm0 0"
          fill="#3d4d60"
          stroke="#3d4d60"
          strokeWidth=".3"
        />
      </G>
    ),
    viewBox: '0 0 15.604 9.944',
  },
  InitBackground: {
    svg: (
      <G>
        <Defs>
          <LinearGradient id="a" x1="0%" x2="100%" y1="0%" y2="100%">
            <Stop offset="0%" stopColor="#9331F7" />
            <Stop offset="100%" stopColor="#521AD6" />
          </LinearGradient>
          <LinearGradient id="b" x1="50%" x2="100%" y1="0%" y2="100.9%">
            <Stop offset="0%" stopColor="#955BFA" />
            <Stop offset="100%" stopColor="#6916E2" />
          </LinearGradient>
          <LinearGradient id="c" x1="50%" x2="100%" y1="0%" y2="100.9%">
            <Stop offset="0%" stopColor="#A94DFB" />
            <Stop offset="100%" stopColor="#7239E9" />
          </LinearGradient>
        </Defs>
        <G fill="none" fillRule="evenodd">
          <Path d="M0 0h375v812H0z" fill="url(#a)" fillRule="nonzero" />
          <Path d="M.5.5h374v811H.5z" stroke="#707070" />
          <Path
            d="M375.011-101.841S62.023-130.575 35.382 98.144C8.741 326.863 145.855 349.968 172.317 575.21 198.779 800.452-38.822 814.592-38.822 814.592L-48 900.925l423.011-40.643v-962.123z"
            fill="url(#b)"
            fillRule="nonzero"
            opacity=".7"
            transform="translate(-.357 -.515)"
          />
          <Path
            d="M369.718-43.152s-216.523 18.374-234.461 187.93c-17.938 169.558 102.988 211.826 90.908 429.942C214.085 792.836.781 812.854.781 812.854h376.576l-7.639-856.006z"
            fill="url(#c)"
            fillRule="nonzero"
            opacity=".7"
            transform="translate(-.357 -.515)"
          />
        </G>
      </G>
    ),
    viewBox: '0 0 375 812',
  },
  Search: {
    svg: (
      <G>
        <Path
          d="M244.186,214.604l-54.379-54.378c-0.289-0.289-0.628-0.491-0.93-0.76
            c10.7-16.231,16.945-35.66,16.945-56.554C205.822,46.075,159.747,0,102.911,0S0,46.075,0,102.911
            c0,56.835,46.074,102.911,102.91,102.911c20.895,0,40.323-6.245,56.554-16.945c0.269,0.301,0.47,0.64,0.759,0.929l54.38,54.38
            c8.169,8.168,21.413,8.168,29.583,0C252.354,236.017,252.354,222.773,244.186,214.604z M102.911,170.146
            c-37.134,0-67.236-30.102-67.236-67.235c0-37.134,30.103-67.236,67.236-67.236c37.132,0,67.235,30.103,67.235,67.236
            C170.146,140.044,140.043,170.146,102.911,170.146z"
        />
      </G>
    ),
    viewBox: '0 0 250.313 250.313',
  },
  AddContact: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M15 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm-9-2V7H4v3H1v2h3v3h2v-3h3v-2H6zm9 4c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  Security: {
    svg: (
      <G>
        <Path d="M8.182 0L0 3.636v5.455A11.313 11.313 0 008.182 20a11.313 11.313 0 008.182-10.909V3.636zm0 9.991h6.364a9.732 9.732 0 01-6.364 8.127V10H1.818V4.818l6.364-2.827v8z" />
      </G>
    ),
    viewBox: '0 0 16.364 20',
  },
  Help: {
    svg: (
      <G>
        <Path d="M7.2 12.8h1.6v-1.6H7.2zM8 0a8 8 0 108 8 8 8 0 00-8-8zm0 14.4A6.4 6.4 0 1114.4 8 6.408 6.408 0 018 14.4zM8 3.2a3.2 3.2 0 00-3.2 3.2h1.6a1.6 1.6 0 013.2 0c0 1.6-2.4 1.4-2.4 4h1.6c0-1.8 2.4-2 2.4-4A3.2 3.2 0 008 3.2z" />
      </G>
    ),
    viewBox: '0 0 16 16',
  },
  FingerPrint: {
    svg: (
      <G>
        <Path d="M14.813 2.47a.465.465 0 01-.23-.06A11.442 11.442 0 009.013 1a11.441 11.441 0 00-5.57 1.41.501.501 0 01-.48-.88 12.724 12.724 0 0112.08-.01.506.506 0 01-.23.95zM.503 7.72a.521.521 0 01-.29-.09.5.5 0 01-.12-.7 10.339 10.339 0 013.75-3.27 11.543 11.543 0 0110.31-.01 10.378 10.378 0 013.75 3.25.502.502 0 01-.82.58 9.388 9.388 0 00-3.39-2.94 10.537 10.537 0 00-9.4.01 9.487 9.487 0 00-3.4 2.96.436.436 0 01-.39.21zm6.25 12.07a.469.469 0 01-.35-.15A10.109 10.109 0 014.393 17a8.877 8.877 0 01-1.05-4.34 5.538 5.538 0 015.66-5.39 5.538 5.538 0 015.66 5.39.5.5 0 11-1 0 4.536 4.536 0 00-4.66-4.39 4.536 4.536 0 00-4.66 4.39 7.834 7.834 0 00.93 3.85 9.465 9.465 0 001.85 2.42.513.513 0 010 .71.548.548 0 01-.37.15zm7.17-1.85a5.385 5.385 0 01-3.1-.89 5.324 5.324 0 01-2.38-4.39.5.5 0 011 0 4.3 4.3 0 001.94 3.56 4.385 4.385 0 002.54.71 6.609 6.609 0 001.04-.1.502.502 0 01.17.99 6.829 6.829 0 01-1.21.12zM11.913 20a.585.585 0 01-.13-.02 7.8 7.8 0 01-3.72-2.1 7.3 7.3 0 01-2.17-5.22 3.016 3.016 0 013.08-2.94 3.016 3.016 0 013.08 2.94 2.085 2.085 0 004.16 0 7.053 7.053 0 00-7.25-6.83 7.3 7.3 0 00-6.61 4.03 6.433 6.433 0 00-.59 2.8 9.937 9.937 0 00.67 3.61.488.488 0 01-.29.64.5.5 0 01-.64-.29 11.141 11.141 0 01-.73-3.96 7.513 7.513 0 01.68-3.24 8.3 8.3 0 017.51-4.6 8.055 8.055 0 018.25 7.83 3.083 3.083 0 01-6.16 0 2.014 2.014 0 00-2.08-1.94 2.014 2.014 0 00-2.08 1.94 6.3 6.3 0 001.87 4.51 6.8 6.8 0 003.27 1.85.5.5 0 01.35.61.488.488 0 01-.47.38z" />
      </G>
    ),
    viewBox: '0 0 17.996 20',
  },
  ArrowUp: {
    svg: (
      <G>
        <Path d="M15.5 0L31 8H0z" fill="#fff" />
      </G>
    ),
    viewBox: '0 0 31 8',
  },
  Filter: {
    svg: (
      <G>
        <Path d="M26.252 6a8.003 8.003 0 0115.496 0h4.25a2 2 0 110 4h-4.25a8.003 8.003 0 01-15.496 0H2.002a2 2 0 110-4h24.25zm-20 20a8.003 8.003 0 0115.496 0h24.25a2 2 0 110 4h-24.25a8.003 8.003 0 01-15.496 0h-4.25a2 2 0 110-4h4.25zM14 32a4 4 0 100-8 4 4 0 000 8zm20-20a4 4 0 100-8 4 4 0 000 8z" />
      </G>
    ),
    viewBox: '0 0 48 36',
  },
  HomeFilled: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  HomeOutlined: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0V0z" fill="none" />
        <Path d="M12 5.69l5 4.5V18h-2v-6H9v6H7v-7.81l5-4.5M12 3L2 12h3v8h6v-6h2v6h6v-8h3L12 3z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  SourceFilled: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M20 6h-8l-2-2H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm-6 10H6v-2h8v2zm4-4H6v-2h12v2z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  SourceOutlined: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M20 6h-8l-2-2H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm0 12H4V6h5.17l2 2H20v10zm-2-6H6v-2h12v2zm-4 4H6v-2h8v2z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  ContactsFilled: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M19 3h-1V1h-2v2H8V1H6v2H5a2 2 0 00-2 2v14a2 2 0 002 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm6 12H6v-1c0-2 4-3.1 6-3.1s6 1.1 6 3.1v1z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  ContactsOutlined: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0V0z" fill="none" />
        <Path d="M20.84 4.22c-.05-.12-.11-.23-.18-.34-.14-.21-.33-.4-.54-.54-.11-.07-.22-.13-.34-.18-.24-.1-.5-.16-.78-.16h-1V1h-2v2H8V1H6v2H5c-.42 0-.8.13-1.12.34-.21.14-.4.33-.54.54-.07.11-.13.22-.18.34-.1.24-.16.5-.16.78v14a2 2 0 002 2h14c.28 0 .54-.06.78-.16.12-.05.23-.11.34-.18.21-.14.4-.33.54-.54.21-.32.34-.71.34-1.12V5c0-.28-.06-.54-.16-.78zM5 19V5h14v14H5zm7-6.12c-2.03 0-6 1.08-6 3.58V18h12v-1.53c0-2.51-3.97-3.59-6-3.59zM8.31 16c.69-.56 2.38-1.12 3.69-1.12s3.01.56 3.69 1.12H8.31zM12 12c1.65 0 3-1.35 3-3s-1.35-3-3-3-3 1.35-3 3 1.35 3 3 3zm0-4c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  SettingsFilled: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0V0z" fill="none" />
        <Path d="M19.14 12.94c.04-.3.06-.61.06-.94 0-.32-.02-.64-.07-.94l2.03-1.58a.49.49 0 00.12-.61l-1.92-3.32a.488.488 0 00-.59-.22l-2.39.96c-.5-.38-1.03-.7-1.62-.94l-.36-2.54a.484.484 0 00-.48-.41h-3.84c-.24 0-.43.17-.47.41l-.36 2.54c-.59.24-1.13.57-1.62.94l-2.39-.96c-.22-.08-.47 0-.59.22L2.74 8.87c-.12.21-.08.47.12.61l2.03 1.58c-.05.3-.09.63-.09.94s.02.64.07.94l-2.03 1.58a.49.49 0 00-.12.61l1.92 3.32c.12.22.37.29.59.22l2.39-.96c.5.38 1.03.7 1.62.94l.36 2.54c.05.24.24.41.48.41h3.84c.24 0 .44-.17.47-.41l.36-2.54c.59-.24 1.13-.56 1.62-.94l2.39.96c.22.08.47 0 .59-.22l1.92-3.32c.12-.22.07-.47-.12-.61l-2.01-1.58zM12 15.6c-1.98 0-3.6-1.62-3.6-3.6s1.62-3.6 3.6-3.6 3.6 1.62 3.6 3.6-1.62 3.6-3.6 3.6z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  SettingsOutlined: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0V0z" fill="none" />
        <Path d="M19.43 12.98c.04-.32.07-.64.07-.98 0-.34-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46a.5.5 0 00-.61-.22l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65A.488.488 0 0014 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1a.566.566 0 00-.18-.03c-.17 0-.34.09-.43.25l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98 0 .33.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46a.5.5 0 00.61.22l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.06.02.12.03.18.03.17 0 .34-.09.43-.25l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zm-1.98-1.71c.04.31.05.52.05.73 0 .21-.02.43-.05.73l-.14 1.13.89.7 1.08.84-.7 1.21-1.27-.51-1.04-.42-.9.68c-.43.32-.84.56-1.25.73l-1.06.43-.16 1.13-.2 1.35h-1.4l-.19-1.35-.16-1.13-1.06-.43c-.43-.18-.83-.41-1.23-.71l-.91-.7-1.06.43-1.27.51-.7-1.21 1.08-.84.89-.7-.14-1.13c-.03-.31-.05-.54-.05-.74s.02-.43.05-.73l.14-1.13-.89-.7-1.08-.84.7-1.21 1.27.51 1.04.42.9-.68c.43-.32.84-.56 1.25-.73l1.06-.43.16-1.13.2-1.35h1.39l.19 1.35.16 1.13 1.06.43c.43.18.83.41 1.23.71l.91.7 1.06-.43 1.27-.51.7 1.21-1.07.85-.89.7.14 1.13zM12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 6c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  Message: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  Call: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M20.01 15.38c-1.23 0-2.42-.2-3.53-.56a.977.977 0 00-1.01.24l-1.57 1.97c-2.83-1.35-5.48-3.9-6.89-6.83l1.95-1.66c.27-.28.35-.67.24-1.02-.37-1.11-.56-2.3-.56-3.53 0-.54-.45-.99-.99-.99H4.19C3.65 3 3 3.24 3 3.99 3 13.28 10.73 21 20.01 21c.71 0 .99-.63.99-1.18v-3.45c0-.54-.45-.99-.99-.99z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  Email: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  Company: {
    svg: (
      <G>
        <Path d="M0 0h24v24H0z" fill="none" />
        <Path d="M12 7V3H2v18h20V7H12zm-2 12H4v-2h6v2zm0-4H4v-2h6v2zm0-4H4V9h6v2zm0-4H4V5h6v2zm10 12h-8V9h8v10zm-2-8h-4v2h4v-2zm0 4h-4v2h4v-2z" />
      </G>
    ),
    viewBox: '0 0 24 24',
  },
  BillingAddress: {
    svg: (
      <G>
        <Path d="M413 0H33C14.781.02.02 14.781 0 33v254c.02 18.219 14.781 32.98 33 33h380c18.219-.02 32.98-14.781 33-33V33c-.02-18.219-14.781-32.98-33-33zM126.398 55.7c26.872.027 48.63 21.831 48.602 48.698-.027 26.872-21.832 48.63-48.7 48.602-26.87-.027-48.628-21.832-48.6-48.7.09-26.843 21.855-48.57 48.698-48.6zm69.403 208.6H57a6.946 6.946 0 01-4.96-2.038A6.952 6.952 0 0150 257.3c0-21.801 7.898-41.903 22.3-56.602a76.049 76.049 0 0154.098-22.597 76.051 76.051 0 0154.102 22.597c14.398 14.7 22.3 34.801 22.3 56.602a6.952 6.952 0 01-2.038 4.96 6.952 6.952 0 01-4.961 2.04zM388.398 231h-116.5a7.001 7.001 0 010-14h116.5c3.868 0 7 3.133 7 7s-3.132 7-7 7zm0-64h-116.5a7.001 7.001 0 010-14h116.5c3.868 0 7 3.133 7 7s-3.132 7-7 7zm0-64h-116.5a7.001 7.001 0 010-14h116.5c3.868 0 7 3.133 7 7s-3.132 7-7 7zm0 0" />
        <Path d="M161.102 104.398c0 19.165-15.54 34.704-34.704 34.704S91.7 123.562 91.7 104.398 107.234 69.7 126.4 69.7c19.163 0 34.703 15.535 34.703 34.7zm0 0M126.398 192.102A61.047 61.047 0 0082.301 210.5C72 221 65.8 235 64.3 250.3H188.5c-1.5-15.402-7.7-29.3-18-39.8a60.884 60.884 0 00-44.102-18.398zm0 0" />
      </G>
    ),
    viewBox: '0 -63 446 446',
  },
  ShippingAddress: {
    svg: (
      <G>
        <Path d="M413 0H33C14.781.02.02 14.781 0 33v254c.02 18.219 14.781 32.98 33 33h380c18.219-.02 32.98-14.781 33-33V33c-.02-18.219-14.781-32.98-33-33zM126.398 55.7c26.872.027 48.63 21.831 48.602 48.698-.027 26.872-21.832 48.63-48.7 48.602-26.87-.027-48.628-21.832-48.6-48.7.09-26.843 21.855-48.57 48.698-48.6zm69.403 208.6H57a6.946 6.946 0 01-4.96-2.038A6.952 6.952 0 0150 257.3c0-21.801 7.898-41.903 22.3-56.602a76.049 76.049 0 0154.098-22.597 76.051 76.051 0 0154.102 22.597c14.398 14.7 22.3 34.801 22.3 56.602a6.952 6.952 0 01-2.038 4.96 6.952 6.952 0 01-4.961 2.04zM388.398 231h-116.5a7.001 7.001 0 010-14h116.5c3.868 0 7 3.133 7 7s-3.132 7-7 7zm0-64h-116.5a7.001 7.001 0 010-14h116.5c3.868 0 7 3.133 7 7s-3.132 7-7 7zm0-64h-116.5a7.001 7.001 0 010-14h116.5c3.868 0 7 3.133 7 7s-3.132 7-7 7zm0 0" />
        <Path d="M161.102 104.398c0 19.165-15.54 34.704-34.704 34.704S91.7 123.562 91.7 104.398 107.234 69.7 126.4 69.7c19.163 0 34.703 15.535 34.703 34.7zm0 0M126.398 192.102A61.047 61.047 0 0082.301 210.5C72 221 65.8 235 64.3 250.3H188.5c-1.5-15.402-7.7-29.3-18-39.8a60.884 60.884 0 00-44.102-18.398zm0 0" />
      </G>
    ),
    viewBox: '0 -63 446 446',
  },
  Calendar: {
    svg: (
      <G>
        <Circle cx="386" cy="210" r="20" />
        <Path d="M432 40h-26V20c0-11.046-8.954-20-20-20s-20 8.954-20 20v20h-91V20c0-11.046-8.954-20-20-20s-20 8.954-20 20v20h-90V20c0-11.046-8.954-20-20-20s-20 8.954-20 20v20H80C35.888 40 0 75.888 0 120v312c0 44.112 35.888 80 80 80h153c11.046 0 20-8.954 20-20s-8.954-20-20-20H80c-22.056 0-40-17.944-40-40V120c0-22.056 17.944-40 40-40h25v20c0 11.046 8.954 20 20 20s20-8.954 20-20V80h90v20c0 11.046 8.954 20 20 20s20-8.954 20-20V80h91v20c0 11.046 8.954 20 20 20s20-8.954 20-20V80h26c22.056 0 40 17.944 40 40v114c0 11.046 8.954 20 20 20s20-8.954 20-20V120c0-44.112-35.888-80-80-80z" />
        <Path d="M391 270c-66.72 0-121 54.28-121 121s54.28 121 121 121 121-54.28 121-121-54.28-121-121-121zm0 202c-44.663 0-81-36.336-81-81s36.337-81 81-81 81 36.336 81 81-36.337 81-81 81z" />
        <Path d="M420 371h-9v-21c0-11.046-8.954-20-20-20s-20 8.954-20 20v41c0 11.046 8.954 20 20 20h29c11.046 0 20-8.954 20-20s-8.954-20-20-20z" />
        <Circle cx="299" cy="210" r="20" />
        <Circle cx="212" cy="297" r="20" />
        <Circle cx="125" cy="210" r="20" />
        <Circle cx="125" cy="297" r="20" />
        <Circle cx="125" cy="384" r="20" />
        <Circle cx="212" cy="384" r="20" />
        <Circle cx="212" cy="210" r="20" />
      </G>
    ),
    viewBox: '0 0 512 512',
  },
  SortAsc: {
    svg: (
      <G>
        <Path
          d="m25.9969 20h6v33c0 .5522847.4477153 1 1 1h10c.5522847 0 1-.4477153 1-1v-33h6c.3708398.002689.7118642-.2028062.8827573-.5319337s.1427857-.7262873-.0727573-1.0280663l-11.97-17.03c-.1884296-.25921249-.4895366-.41258597-.81-.41258597s-.6215704.15337348-.81.41258597l-3.12 4.42-6.93 9.81-1.98 2.8c-.215543.301779-.2436504.6989388-.0727573 1.0280663s.5119175.5346227.8827573.5319337z"
          fill="#AAAAAA"
        />
        <Path d="m2.1731 44.3575 6.93 9.81 3.12 4.42c.1884555.2591733.4895531.4125159.81.4125159s.6215445-.1533426.81-.4125159l11.97-17.03c.215543-.301779.2436504-.6989388.0727573-1.0280663s-.5119175-.5346227-.8827573-.5319337h-6v-32.9975c0-.55228475-.4477153-1-1-1h-10c-.55228475 0-1 .44771525-1 1v32.9975h-6c-.37083976-.002689-.71186417.2028062-.88275728.5319337-.17089312.3291275-.14278572.7262873.07275728 1.0280663z" />
      </G>
    ),
    viewBox: '0 0 51 58',
  },
  SortDesc: {
    svg: (
      <G>
        <Path d="m25.9969 20h6v33c0 .5522847.4477153 1 1 1h10c.5522847 0 1-.4477153 1-1v-33h6c.3708398.002689.7118642-.2028062.8827573-.5319337s.1427857-.7262873-.0727573-1.0280663l-11.97-17.03c-.1884296-.25921249-.4895366-.41258597-.81-.41258597s-.6215704.15337348-.81.41258597l-3.12 4.42-6.93 9.81-1.98 2.8c-.215543.301779-.2436504.6989388-.0727573 1.0280663s.5119175.5346227.8827573.5319337z" />
        <Path
          d="m2.1731 44.3575 6.93 9.81 3.12 4.42c.1884555.2591733.4895531.4125159.81.4125159s.6215445-.1533426.81-.4125159l11.97-17.03c.215543-.301779.2436504-.6989388.0727573-1.0280663s-.5119175-.5346227-.8827573-.5319337h-6v-32.9975c0-.55228475-.4477153-1-1-1h-10c-.55228475 0-1 .44771525-1 1v32.9975h-6c-.37083976-.002689-.71186417.2028062-.88275728.5319337-.17089312.3291275-.14278572.7262873.07275728 1.0280663z"
          fill="#AAAAAA"
        />
      </G>
    ),
    viewBox: '0 0 51 58',
  },
};

export default svgs;
