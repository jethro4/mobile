const PRIMARY = '#663399';
const SECONDARY = `${PRIMARY}aa`;
const TINT1 = `${PRIMARY}88`;
const TINT2 = `${PRIMARY}66`;
const TINT3 = `${PRIMARY}44`;

const colors = {
  primary: PRIMARY,
  secondary: SECONDARY,
  tint1: TINT1,
  tint2: TINT2,
  tint3: TINT3,
  text: '#353b48',
  mutedText: '#7f8fa6',
  yellow: '#FF8C00',
  lightYellow: '#ff9c24',
  white: '#FFFFFF',
  silver: '#F7F7F7',
  background: '#F4F5F9',
  border: '#dcdde1',
  darkGray: '#AAAAAA',
  darkerGray: '#888888',
  darkestGray: '#666666',
  gray: '#BBBBBB',
  lightGray: '#C1C2C4',
  lighterGray: '#DDDDDD',
  lightestGray: '#f5f6fa',
  superLightGray: '#F2F2F2',
  error: '#FF4444',
  required: '#FF4444',
  black: '#333333',
  success: '#34C75A',
  small: '#718093',
  transparent: 'transparent',
  formBlue: '#0075FF',
};

export const setPrimaryColor = (color) => {
  colors.primary = color;
  colors.secondary = `${color}aa`;
  colors.tint1 = `${color}88`;
  colors.tint2 = `${color}66`;
  colors.tint3 = `${color}44`;
};

export const resetColors = () => {
  colors.primary = PRIMARY;
  colors.secondary = SECONDARY;
  colors.tint1 = TINT1;
  colors.tint2 = TINT2;
  colors.tint3 = TINT3;
};

export default colors;
